part of 'category_watcher_bloc.dart';

@freezed
abstract class CategoryWatcherEvent with _$CategoryWatcherEvent {
  const factory CategoryWatcherEvent.getCategories() = _GetCategories;
  const factory CategoryWatcherEvent.categoriesReceived(
      Either<InfraFailure, List<Category>> categories) = _CategoriesReceived;
}
