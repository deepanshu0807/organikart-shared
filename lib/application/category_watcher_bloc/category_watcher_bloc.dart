import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:organikart_shared/domain/category/category.dart';
import 'package:organikart_shared/domain/category/i_category_repo.dart';
import 'package:organikart_shared/domain/core/infra_failure.dart';
import 'package:organikart_shared/organikart_shared_package.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'category_watcher_event.dart';
part 'category_watcher_state.dart';
part 'category_watcher_bloc.freezed.dart';

@injectable
class CategoryWatcherBloc
    extends Bloc<CategoryWatcherEvent, CategoryWatcherState> {
  final ICategoryRepo _iCategoryRepository;
  CategoryWatcherBloc(this._iCategoryRepository)
      : super(const CategoryWatcherState.initial());

  @override
  Stream<CategoryWatcherState> mapEventToState(
    CategoryWatcherEvent event,
  ) async* {
    yield* event.map(
      getCategories: (e) async* {
        yield const CategoryWatcherState.loadInProgress();
        _iCategoryRepository
            .getAllCategory()
            .listen((c) => add(CategoryWatcherEvent.categoriesReceived(c)));
      },
      categoriesReceived: (e) async* {
        yield e.categories.fold(
          (f) => CategoryWatcherState.loadFailure(f),
          (c) => CategoryWatcherState.loadSuccess(c),
        );
      },
    );
  }
}
