// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'content_watcher_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$ContentWatcherEventTearOff {
  const _$ContentWatcherEventTearOff();

// ignore: unused_element
  _GetContent getContent() {
    return const _GetContent();
  }

// ignore: unused_element
  _ContentReceived contentReceived(
      Either<InfraFailure, List<Content>> content) {
    return _ContentReceived(
      content,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $ContentWatcherEvent = _$ContentWatcherEventTearOff();

/// @nodoc
mixin _$ContentWatcherEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult getContent(),
    @required
        TResult contentReceived(Either<InfraFailure, List<Content>> content),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult getContent(),
    TResult contentReceived(Either<InfraFailure, List<Content>> content),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult getContent(_GetContent value),
    @required TResult contentReceived(_ContentReceived value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult getContent(_GetContent value),
    TResult contentReceived(_ContentReceived value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $ContentWatcherEventCopyWith<$Res> {
  factory $ContentWatcherEventCopyWith(
          ContentWatcherEvent value, $Res Function(ContentWatcherEvent) then) =
      _$ContentWatcherEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$ContentWatcherEventCopyWithImpl<$Res>
    implements $ContentWatcherEventCopyWith<$Res> {
  _$ContentWatcherEventCopyWithImpl(this._value, this._then);

  final ContentWatcherEvent _value;
  // ignore: unused_field
  final $Res Function(ContentWatcherEvent) _then;
}

/// @nodoc
abstract class _$GetContentCopyWith<$Res> {
  factory _$GetContentCopyWith(
          _GetContent value, $Res Function(_GetContent) then) =
      __$GetContentCopyWithImpl<$Res>;
}

/// @nodoc
class __$GetContentCopyWithImpl<$Res>
    extends _$ContentWatcherEventCopyWithImpl<$Res>
    implements _$GetContentCopyWith<$Res> {
  __$GetContentCopyWithImpl(
      _GetContent _value, $Res Function(_GetContent) _then)
      : super(_value, (v) => _then(v as _GetContent));

  @override
  _GetContent get _value => super._value as _GetContent;
}

/// @nodoc
class _$_GetContent implements _GetContent {
  const _$_GetContent();

  @override
  String toString() {
    return 'ContentWatcherEvent.getContent()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _GetContent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult getContent(),
    @required
        TResult contentReceived(Either<InfraFailure, List<Content>> content),
  }) {
    assert(getContent != null);
    assert(contentReceived != null);
    return getContent();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult getContent(),
    TResult contentReceived(Either<InfraFailure, List<Content>> content),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (getContent != null) {
      return getContent();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult getContent(_GetContent value),
    @required TResult contentReceived(_ContentReceived value),
  }) {
    assert(getContent != null);
    assert(contentReceived != null);
    return getContent(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult getContent(_GetContent value),
    TResult contentReceived(_ContentReceived value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (getContent != null) {
      return getContent(this);
    }
    return orElse();
  }
}

abstract class _GetContent implements ContentWatcherEvent {
  const factory _GetContent() = _$_GetContent;
}

/// @nodoc
abstract class _$ContentReceivedCopyWith<$Res> {
  factory _$ContentReceivedCopyWith(
          _ContentReceived value, $Res Function(_ContentReceived) then) =
      __$ContentReceivedCopyWithImpl<$Res>;
  $Res call({Either<InfraFailure, List<Content>> content});
}

/// @nodoc
class __$ContentReceivedCopyWithImpl<$Res>
    extends _$ContentWatcherEventCopyWithImpl<$Res>
    implements _$ContentReceivedCopyWith<$Res> {
  __$ContentReceivedCopyWithImpl(
      _ContentReceived _value, $Res Function(_ContentReceived) _then)
      : super(_value, (v) => _then(v as _ContentReceived));

  @override
  _ContentReceived get _value => super._value as _ContentReceived;

  @override
  $Res call({
    Object content = freezed,
  }) {
    return _then(_ContentReceived(
      content == freezed
          ? _value.content
          : content as Either<InfraFailure, List<Content>>,
    ));
  }
}

/// @nodoc
class _$_ContentReceived implements _ContentReceived {
  const _$_ContentReceived(this.content) : assert(content != null);

  @override
  final Either<InfraFailure, List<Content>> content;

  @override
  String toString() {
    return 'ContentWatcherEvent.contentReceived(content: $content)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ContentReceived &&
            (identical(other.content, content) ||
                const DeepCollectionEquality().equals(other.content, content)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(content);

  @override
  _$ContentReceivedCopyWith<_ContentReceived> get copyWith =>
      __$ContentReceivedCopyWithImpl<_ContentReceived>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult getContent(),
    @required
        TResult contentReceived(Either<InfraFailure, List<Content>> content),
  }) {
    assert(getContent != null);
    assert(contentReceived != null);
    return contentReceived(content);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult getContent(),
    TResult contentReceived(Either<InfraFailure, List<Content>> content),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (contentReceived != null) {
      return contentReceived(content);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult getContent(_GetContent value),
    @required TResult contentReceived(_ContentReceived value),
  }) {
    assert(getContent != null);
    assert(contentReceived != null);
    return contentReceived(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult getContent(_GetContent value),
    TResult contentReceived(_ContentReceived value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (contentReceived != null) {
      return contentReceived(this);
    }
    return orElse();
  }
}

abstract class _ContentReceived implements ContentWatcherEvent {
  const factory _ContentReceived(Either<InfraFailure, List<Content>> content) =
      _$_ContentReceived;

  Either<InfraFailure, List<Content>> get content;
  _$ContentReceivedCopyWith<_ContentReceived> get copyWith;
}

/// @nodoc
class _$ContentWatcherStateTearOff {
  const _$ContentWatcherStateTearOff();

// ignore: unused_element
  _Initial initial() {
    return const _Initial();
  }

// ignore: unused_element
  _DataTransferInProgress loadInProgress() {
    return const _DataTransferInProgress();
  }

// ignore: unused_element
  _LoadSuccess loadSuccess(List<Content> content) {
    return _LoadSuccess(
      content,
    );
  }

// ignore: unused_element
  _LoadFailure loadFailure(InfraFailure<dynamic> failure) {
    return _LoadFailure(
      failure,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $ContentWatcherState = _$ContentWatcherStateTearOff();

/// @nodoc
mixin _$ContentWatcherState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadSuccess(List<Content> content),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadSuccess(List<Content> content),
    TResult loadFailure(InfraFailure<dynamic> failure),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadSuccess(_LoadSuccess value),
    @required TResult loadFailure(_LoadFailure value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadSuccess(_LoadSuccess value),
    TResult loadFailure(_LoadFailure value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $ContentWatcherStateCopyWith<$Res> {
  factory $ContentWatcherStateCopyWith(
          ContentWatcherState value, $Res Function(ContentWatcherState) then) =
      _$ContentWatcherStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$ContentWatcherStateCopyWithImpl<$Res>
    implements $ContentWatcherStateCopyWith<$Res> {
  _$ContentWatcherStateCopyWithImpl(this._value, this._then);

  final ContentWatcherState _value;
  // ignore: unused_field
  final $Res Function(ContentWatcherState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res>
    extends _$ContentWatcherStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc
class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'ContentWatcherState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadSuccess(List<Content> content),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadSuccess != null);
    assert(loadFailure != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadSuccess(List<Content> content),
    TResult loadFailure(InfraFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadSuccess(_LoadSuccess value),
    @required TResult loadFailure(_LoadFailure value),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadSuccess != null);
    assert(loadFailure != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadSuccess(_LoadSuccess value),
    TResult loadFailure(_LoadFailure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements ContentWatcherState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$DataTransferInProgressCopyWith<$Res> {
  factory _$DataTransferInProgressCopyWith(_DataTransferInProgress value,
          $Res Function(_DataTransferInProgress) then) =
      __$DataTransferInProgressCopyWithImpl<$Res>;
}

/// @nodoc
class __$DataTransferInProgressCopyWithImpl<$Res>
    extends _$ContentWatcherStateCopyWithImpl<$Res>
    implements _$DataTransferInProgressCopyWith<$Res> {
  __$DataTransferInProgressCopyWithImpl(_DataTransferInProgress _value,
      $Res Function(_DataTransferInProgress) _then)
      : super(_value, (v) => _then(v as _DataTransferInProgress));

  @override
  _DataTransferInProgress get _value => super._value as _DataTransferInProgress;
}

/// @nodoc
class _$_DataTransferInProgress implements _DataTransferInProgress {
  const _$_DataTransferInProgress();

  @override
  String toString() {
    return 'ContentWatcherState.loadInProgress()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _DataTransferInProgress);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadSuccess(List<Content> content),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadSuccess != null);
    assert(loadFailure != null);
    return loadInProgress();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadSuccess(List<Content> content),
    TResult loadFailure(InfraFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadInProgress != null) {
      return loadInProgress();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadSuccess(_LoadSuccess value),
    @required TResult loadFailure(_LoadFailure value),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadSuccess != null);
    assert(loadFailure != null);
    return loadInProgress(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadSuccess(_LoadSuccess value),
    TResult loadFailure(_LoadFailure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadInProgress != null) {
      return loadInProgress(this);
    }
    return orElse();
  }
}

abstract class _DataTransferInProgress implements ContentWatcherState {
  const factory _DataTransferInProgress() = _$_DataTransferInProgress;
}

/// @nodoc
abstract class _$LoadSuccessCopyWith<$Res> {
  factory _$LoadSuccessCopyWith(
          _LoadSuccess value, $Res Function(_LoadSuccess) then) =
      __$LoadSuccessCopyWithImpl<$Res>;
  $Res call({List<Content> content});
}

/// @nodoc
class __$LoadSuccessCopyWithImpl<$Res>
    extends _$ContentWatcherStateCopyWithImpl<$Res>
    implements _$LoadSuccessCopyWith<$Res> {
  __$LoadSuccessCopyWithImpl(
      _LoadSuccess _value, $Res Function(_LoadSuccess) _then)
      : super(_value, (v) => _then(v as _LoadSuccess));

  @override
  _LoadSuccess get _value => super._value as _LoadSuccess;

  @override
  $Res call({
    Object content = freezed,
  }) {
    return _then(_LoadSuccess(
      content == freezed ? _value.content : content as List<Content>,
    ));
  }
}

/// @nodoc
class _$_LoadSuccess implements _LoadSuccess {
  const _$_LoadSuccess(this.content) : assert(content != null);

  @override
  final List<Content> content;

  @override
  String toString() {
    return 'ContentWatcherState.loadSuccess(content: $content)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LoadSuccess &&
            (identical(other.content, content) ||
                const DeepCollectionEquality().equals(other.content, content)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(content);

  @override
  _$LoadSuccessCopyWith<_LoadSuccess> get copyWith =>
      __$LoadSuccessCopyWithImpl<_LoadSuccess>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadSuccess(List<Content> content),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadSuccess != null);
    assert(loadFailure != null);
    return loadSuccess(content);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadSuccess(List<Content> content),
    TResult loadFailure(InfraFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadSuccess != null) {
      return loadSuccess(content);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadSuccess(_LoadSuccess value),
    @required TResult loadFailure(_LoadFailure value),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadSuccess != null);
    assert(loadFailure != null);
    return loadSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadSuccess(_LoadSuccess value),
    TResult loadFailure(_LoadFailure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadSuccess != null) {
      return loadSuccess(this);
    }
    return orElse();
  }
}

abstract class _LoadSuccess implements ContentWatcherState {
  const factory _LoadSuccess(List<Content> content) = _$_LoadSuccess;

  List<Content> get content;
  _$LoadSuccessCopyWith<_LoadSuccess> get copyWith;
}

/// @nodoc
abstract class _$LoadFailureCopyWith<$Res> {
  factory _$LoadFailureCopyWith(
          _LoadFailure value, $Res Function(_LoadFailure) then) =
      __$LoadFailureCopyWithImpl<$Res>;
  $Res call({InfraFailure<dynamic> failure});

  $InfraFailureCopyWith<dynamic, $Res> get failure;
}

/// @nodoc
class __$LoadFailureCopyWithImpl<$Res>
    extends _$ContentWatcherStateCopyWithImpl<$Res>
    implements _$LoadFailureCopyWith<$Res> {
  __$LoadFailureCopyWithImpl(
      _LoadFailure _value, $Res Function(_LoadFailure) _then)
      : super(_value, (v) => _then(v as _LoadFailure));

  @override
  _LoadFailure get _value => super._value as _LoadFailure;

  @override
  $Res call({
    Object failure = freezed,
  }) {
    return _then(_LoadFailure(
      failure == freezed ? _value.failure : failure as InfraFailure<dynamic>,
    ));
  }

  @override
  $InfraFailureCopyWith<dynamic, $Res> get failure {
    if (_value.failure == null) {
      return null;
    }
    return $InfraFailureCopyWith<dynamic, $Res>(_value.failure, (value) {
      return _then(_value.copyWith(failure: value));
    });
  }
}

/// @nodoc
class _$_LoadFailure implements _LoadFailure {
  const _$_LoadFailure(this.failure) : assert(failure != null);

  @override
  final InfraFailure<dynamic> failure;

  @override
  String toString() {
    return 'ContentWatcherState.loadFailure(failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LoadFailure &&
            (identical(other.failure, failure) ||
                const DeepCollectionEquality().equals(other.failure, failure)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(failure);

  @override
  _$LoadFailureCopyWith<_LoadFailure> get copyWith =>
      __$LoadFailureCopyWithImpl<_LoadFailure>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadSuccess(List<Content> content),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadSuccess != null);
    assert(loadFailure != null);
    return loadFailure(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadSuccess(List<Content> content),
    TResult loadFailure(InfraFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadFailure != null) {
      return loadFailure(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadSuccess(_LoadSuccess value),
    @required TResult loadFailure(_LoadFailure value),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadSuccess != null);
    assert(loadFailure != null);
    return loadFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadSuccess(_LoadSuccess value),
    TResult loadFailure(_LoadFailure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadFailure != null) {
      return loadFailure(this);
    }
    return orElse();
  }
}

abstract class _LoadFailure implements ContentWatcherState {
  const factory _LoadFailure(InfraFailure<dynamic> failure) = _$_LoadFailure;

  InfraFailure<dynamic> get failure;
  _$LoadFailureCopyWith<_LoadFailure> get copyWith;
}
