part of 'content_watcher_bloc.dart';

@freezed
abstract class ContentWatcherState with _$ContentWatcherState {
  const factory ContentWatcherState.initial() = _Initial;
  const factory ContentWatcherState.loadInProgress() = _DataTransferInProgress;
  const factory ContentWatcherState.loadSuccess(List<Content> content) =
      _LoadSuccess;
  const factory ContentWatcherState.loadFailure(InfraFailure failure) =
      _LoadFailure;
}
