import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

part 'content_watcher_event.dart';
part 'content_watcher_state.dart';
part 'content_watcher_bloc.freezed.dart';

@injectable
class ContentWatcherBloc
    extends Bloc<ContentWatcherEvent, ContentWatcherState> {
  final IContentRepo _iContentRepository;
  ContentWatcherBloc(this._iContentRepository)
      : super(const ContentWatcherState.initial());

  @override
  Stream<ContentWatcherState> mapEventToState(
    ContentWatcherEvent event,
  ) async* {
    yield* event.map(
      getContent: (e) async* {
        yield const ContentWatcherState.loadInProgress();
        _iContentRepository
            .getAllContent()
            .listen((c) => add(ContentWatcherEvent.contentReceived(c)));
      },
      contentReceived: (e) async* {
        yield e.content.fold(
          (f) => ContentWatcherState.loadFailure(f),
          (c) => ContentWatcherState.loadSuccess(c),
        );
      },
    );
  }
}
