part of 'content_watcher_bloc.dart';

@freezed
abstract class ContentWatcherEvent with _$ContentWatcherEvent {
  const factory ContentWatcherEvent.getContent() = _GetContent;
  const factory ContentWatcherEvent.contentReceived(
      Either<InfraFailure, List<Content>> content) = _ContentReceived;
}
