import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

part 'enquiry_watcher_event.dart';
part 'enquiry_watcher_state.dart';
part 'enquiry_watcher_bloc.freezed.dart';

@injectable
class EnquiryWatcherBloc
    extends Bloc<EnquiryWatcherEvent, EnquiryWatcherState> {
  final IEnquiryRepo _iEnquiryRepository;
  EnquiryWatcherBloc(this._iEnquiryRepository)
      : super(const EnquiryWatcherState.initial());

  @override
  Stream<EnquiryWatcherState> mapEventToState(
    EnquiryWatcherEvent event,
  ) async* {
    yield* event.map(
      getEnquiry: (e) async* {
        yield const EnquiryWatcherState.loadInProgress();
        _iEnquiryRepository
            .getEnquiry()
            .listen((c) => add(EnquiryWatcherEvent.enquiryReceived(c)));
      },
      enquiryReceived: (e) async* {
        yield e.enquiry.fold(
          (f) => EnquiryWatcherState.loadFailure(f),
          (c) => EnquiryWatcherState.loadSuccess(c),
        );
      },
    );
  }
}
