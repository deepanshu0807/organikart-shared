// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'enquiry_watcher_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$EnquiryWatcherEventTearOff {
  const _$EnquiryWatcherEventTearOff();

// ignore: unused_element
  _GetEnquiry getEnquiry() {
    return const _GetEnquiry();
  }

// ignore: unused_element
  _EnquiryReceived enquiryReceived(
      Either<InfraFailure, List<Enquiry>> enquiry) {
    return _EnquiryReceived(
      enquiry,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $EnquiryWatcherEvent = _$EnquiryWatcherEventTearOff();

/// @nodoc
mixin _$EnquiryWatcherEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult getEnquiry(),
    @required
        TResult enquiryReceived(Either<InfraFailure, List<Enquiry>> enquiry),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult getEnquiry(),
    TResult enquiryReceived(Either<InfraFailure, List<Enquiry>> enquiry),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult getEnquiry(_GetEnquiry value),
    @required TResult enquiryReceived(_EnquiryReceived value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult getEnquiry(_GetEnquiry value),
    TResult enquiryReceived(_EnquiryReceived value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $EnquiryWatcherEventCopyWith<$Res> {
  factory $EnquiryWatcherEventCopyWith(
          EnquiryWatcherEvent value, $Res Function(EnquiryWatcherEvent) then) =
      _$EnquiryWatcherEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$EnquiryWatcherEventCopyWithImpl<$Res>
    implements $EnquiryWatcherEventCopyWith<$Res> {
  _$EnquiryWatcherEventCopyWithImpl(this._value, this._then);

  final EnquiryWatcherEvent _value;
  // ignore: unused_field
  final $Res Function(EnquiryWatcherEvent) _then;
}

/// @nodoc
abstract class _$GetEnquiryCopyWith<$Res> {
  factory _$GetEnquiryCopyWith(
          _GetEnquiry value, $Res Function(_GetEnquiry) then) =
      __$GetEnquiryCopyWithImpl<$Res>;
}

/// @nodoc
class __$GetEnquiryCopyWithImpl<$Res>
    extends _$EnquiryWatcherEventCopyWithImpl<$Res>
    implements _$GetEnquiryCopyWith<$Res> {
  __$GetEnquiryCopyWithImpl(
      _GetEnquiry _value, $Res Function(_GetEnquiry) _then)
      : super(_value, (v) => _then(v as _GetEnquiry));

  @override
  _GetEnquiry get _value => super._value as _GetEnquiry;
}

/// @nodoc
class _$_GetEnquiry implements _GetEnquiry {
  const _$_GetEnquiry();

  @override
  String toString() {
    return 'EnquiryWatcherEvent.getEnquiry()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _GetEnquiry);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult getEnquiry(),
    @required
        TResult enquiryReceived(Either<InfraFailure, List<Enquiry>> enquiry),
  }) {
    assert(getEnquiry != null);
    assert(enquiryReceived != null);
    return getEnquiry();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult getEnquiry(),
    TResult enquiryReceived(Either<InfraFailure, List<Enquiry>> enquiry),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (getEnquiry != null) {
      return getEnquiry();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult getEnquiry(_GetEnquiry value),
    @required TResult enquiryReceived(_EnquiryReceived value),
  }) {
    assert(getEnquiry != null);
    assert(enquiryReceived != null);
    return getEnquiry(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult getEnquiry(_GetEnquiry value),
    TResult enquiryReceived(_EnquiryReceived value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (getEnquiry != null) {
      return getEnquiry(this);
    }
    return orElse();
  }
}

abstract class _GetEnquiry implements EnquiryWatcherEvent {
  const factory _GetEnquiry() = _$_GetEnquiry;
}

/// @nodoc
abstract class _$EnquiryReceivedCopyWith<$Res> {
  factory _$EnquiryReceivedCopyWith(
          _EnquiryReceived value, $Res Function(_EnquiryReceived) then) =
      __$EnquiryReceivedCopyWithImpl<$Res>;
  $Res call({Either<InfraFailure, List<Enquiry>> enquiry});
}

/// @nodoc
class __$EnquiryReceivedCopyWithImpl<$Res>
    extends _$EnquiryWatcherEventCopyWithImpl<$Res>
    implements _$EnquiryReceivedCopyWith<$Res> {
  __$EnquiryReceivedCopyWithImpl(
      _EnquiryReceived _value, $Res Function(_EnquiryReceived) _then)
      : super(_value, (v) => _then(v as _EnquiryReceived));

  @override
  _EnquiryReceived get _value => super._value as _EnquiryReceived;

  @override
  $Res call({
    Object enquiry = freezed,
  }) {
    return _then(_EnquiryReceived(
      enquiry == freezed
          ? _value.enquiry
          : enquiry as Either<InfraFailure, List<Enquiry>>,
    ));
  }
}

/// @nodoc
class _$_EnquiryReceived implements _EnquiryReceived {
  const _$_EnquiryReceived(this.enquiry) : assert(enquiry != null);

  @override
  final Either<InfraFailure, List<Enquiry>> enquiry;

  @override
  String toString() {
    return 'EnquiryWatcherEvent.enquiryReceived(enquiry: $enquiry)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EnquiryReceived &&
            (identical(other.enquiry, enquiry) ||
                const DeepCollectionEquality().equals(other.enquiry, enquiry)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(enquiry);

  @override
  _$EnquiryReceivedCopyWith<_EnquiryReceived> get copyWith =>
      __$EnquiryReceivedCopyWithImpl<_EnquiryReceived>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult getEnquiry(),
    @required
        TResult enquiryReceived(Either<InfraFailure, List<Enquiry>> enquiry),
  }) {
    assert(getEnquiry != null);
    assert(enquiryReceived != null);
    return enquiryReceived(enquiry);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult getEnquiry(),
    TResult enquiryReceived(Either<InfraFailure, List<Enquiry>> enquiry),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (enquiryReceived != null) {
      return enquiryReceived(enquiry);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult getEnquiry(_GetEnquiry value),
    @required TResult enquiryReceived(_EnquiryReceived value),
  }) {
    assert(getEnquiry != null);
    assert(enquiryReceived != null);
    return enquiryReceived(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult getEnquiry(_GetEnquiry value),
    TResult enquiryReceived(_EnquiryReceived value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (enquiryReceived != null) {
      return enquiryReceived(this);
    }
    return orElse();
  }
}

abstract class _EnquiryReceived implements EnquiryWatcherEvent {
  const factory _EnquiryReceived(Either<InfraFailure, List<Enquiry>> enquiry) =
      _$_EnquiryReceived;

  Either<InfraFailure, List<Enquiry>> get enquiry;
  _$EnquiryReceivedCopyWith<_EnquiryReceived> get copyWith;
}

/// @nodoc
class _$EnquiryWatcherStateTearOff {
  const _$EnquiryWatcherStateTearOff();

// ignore: unused_element
  _Initial initial() {
    return const _Initial();
  }

// ignore: unused_element
  _DataTransferInProgress loadInProgress() {
    return const _DataTransferInProgress();
  }

// ignore: unused_element
  _LoadSuccess loadSuccess(List<Enquiry> enquiry) {
    return _LoadSuccess(
      enquiry,
    );
  }

// ignore: unused_element
  _LoadFailure loadFailure(InfraFailure<dynamic> failure) {
    return _LoadFailure(
      failure,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $EnquiryWatcherState = _$EnquiryWatcherStateTearOff();

/// @nodoc
mixin _$EnquiryWatcherState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadSuccess(List<Enquiry> enquiry),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadSuccess(List<Enquiry> enquiry),
    TResult loadFailure(InfraFailure<dynamic> failure),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadSuccess(_LoadSuccess value),
    @required TResult loadFailure(_LoadFailure value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadSuccess(_LoadSuccess value),
    TResult loadFailure(_LoadFailure value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $EnquiryWatcherStateCopyWith<$Res> {
  factory $EnquiryWatcherStateCopyWith(
          EnquiryWatcherState value, $Res Function(EnquiryWatcherState) then) =
      _$EnquiryWatcherStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$EnquiryWatcherStateCopyWithImpl<$Res>
    implements $EnquiryWatcherStateCopyWith<$Res> {
  _$EnquiryWatcherStateCopyWithImpl(this._value, this._then);

  final EnquiryWatcherState _value;
  // ignore: unused_field
  final $Res Function(EnquiryWatcherState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res>
    extends _$EnquiryWatcherStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc
class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'EnquiryWatcherState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadSuccess(List<Enquiry> enquiry),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadSuccess != null);
    assert(loadFailure != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadSuccess(List<Enquiry> enquiry),
    TResult loadFailure(InfraFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadSuccess(_LoadSuccess value),
    @required TResult loadFailure(_LoadFailure value),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadSuccess != null);
    assert(loadFailure != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadSuccess(_LoadSuccess value),
    TResult loadFailure(_LoadFailure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements EnquiryWatcherState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$DataTransferInProgressCopyWith<$Res> {
  factory _$DataTransferInProgressCopyWith(_DataTransferInProgress value,
          $Res Function(_DataTransferInProgress) then) =
      __$DataTransferInProgressCopyWithImpl<$Res>;
}

/// @nodoc
class __$DataTransferInProgressCopyWithImpl<$Res>
    extends _$EnquiryWatcherStateCopyWithImpl<$Res>
    implements _$DataTransferInProgressCopyWith<$Res> {
  __$DataTransferInProgressCopyWithImpl(_DataTransferInProgress _value,
      $Res Function(_DataTransferInProgress) _then)
      : super(_value, (v) => _then(v as _DataTransferInProgress));

  @override
  _DataTransferInProgress get _value => super._value as _DataTransferInProgress;
}

/// @nodoc
class _$_DataTransferInProgress implements _DataTransferInProgress {
  const _$_DataTransferInProgress();

  @override
  String toString() {
    return 'EnquiryWatcherState.loadInProgress()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _DataTransferInProgress);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadSuccess(List<Enquiry> enquiry),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadSuccess != null);
    assert(loadFailure != null);
    return loadInProgress();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadSuccess(List<Enquiry> enquiry),
    TResult loadFailure(InfraFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadInProgress != null) {
      return loadInProgress();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadSuccess(_LoadSuccess value),
    @required TResult loadFailure(_LoadFailure value),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadSuccess != null);
    assert(loadFailure != null);
    return loadInProgress(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadSuccess(_LoadSuccess value),
    TResult loadFailure(_LoadFailure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadInProgress != null) {
      return loadInProgress(this);
    }
    return orElse();
  }
}

abstract class _DataTransferInProgress implements EnquiryWatcherState {
  const factory _DataTransferInProgress() = _$_DataTransferInProgress;
}

/// @nodoc
abstract class _$LoadSuccessCopyWith<$Res> {
  factory _$LoadSuccessCopyWith(
          _LoadSuccess value, $Res Function(_LoadSuccess) then) =
      __$LoadSuccessCopyWithImpl<$Res>;
  $Res call({List<Enquiry> enquiry});
}

/// @nodoc
class __$LoadSuccessCopyWithImpl<$Res>
    extends _$EnquiryWatcherStateCopyWithImpl<$Res>
    implements _$LoadSuccessCopyWith<$Res> {
  __$LoadSuccessCopyWithImpl(
      _LoadSuccess _value, $Res Function(_LoadSuccess) _then)
      : super(_value, (v) => _then(v as _LoadSuccess));

  @override
  _LoadSuccess get _value => super._value as _LoadSuccess;

  @override
  $Res call({
    Object enquiry = freezed,
  }) {
    return _then(_LoadSuccess(
      enquiry == freezed ? _value.enquiry : enquiry as List<Enquiry>,
    ));
  }
}

/// @nodoc
class _$_LoadSuccess implements _LoadSuccess {
  const _$_LoadSuccess(this.enquiry) : assert(enquiry != null);

  @override
  final List<Enquiry> enquiry;

  @override
  String toString() {
    return 'EnquiryWatcherState.loadSuccess(enquiry: $enquiry)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LoadSuccess &&
            (identical(other.enquiry, enquiry) ||
                const DeepCollectionEquality().equals(other.enquiry, enquiry)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(enquiry);

  @override
  _$LoadSuccessCopyWith<_LoadSuccess> get copyWith =>
      __$LoadSuccessCopyWithImpl<_LoadSuccess>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadSuccess(List<Enquiry> enquiry),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadSuccess != null);
    assert(loadFailure != null);
    return loadSuccess(enquiry);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadSuccess(List<Enquiry> enquiry),
    TResult loadFailure(InfraFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadSuccess != null) {
      return loadSuccess(enquiry);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadSuccess(_LoadSuccess value),
    @required TResult loadFailure(_LoadFailure value),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadSuccess != null);
    assert(loadFailure != null);
    return loadSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadSuccess(_LoadSuccess value),
    TResult loadFailure(_LoadFailure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadSuccess != null) {
      return loadSuccess(this);
    }
    return orElse();
  }
}

abstract class _LoadSuccess implements EnquiryWatcherState {
  const factory _LoadSuccess(List<Enquiry> enquiry) = _$_LoadSuccess;

  List<Enquiry> get enquiry;
  _$LoadSuccessCopyWith<_LoadSuccess> get copyWith;
}

/// @nodoc
abstract class _$LoadFailureCopyWith<$Res> {
  factory _$LoadFailureCopyWith(
          _LoadFailure value, $Res Function(_LoadFailure) then) =
      __$LoadFailureCopyWithImpl<$Res>;
  $Res call({InfraFailure<dynamic> failure});

  $InfraFailureCopyWith<dynamic, $Res> get failure;
}

/// @nodoc
class __$LoadFailureCopyWithImpl<$Res>
    extends _$EnquiryWatcherStateCopyWithImpl<$Res>
    implements _$LoadFailureCopyWith<$Res> {
  __$LoadFailureCopyWithImpl(
      _LoadFailure _value, $Res Function(_LoadFailure) _then)
      : super(_value, (v) => _then(v as _LoadFailure));

  @override
  _LoadFailure get _value => super._value as _LoadFailure;

  @override
  $Res call({
    Object failure = freezed,
  }) {
    return _then(_LoadFailure(
      failure == freezed ? _value.failure : failure as InfraFailure<dynamic>,
    ));
  }

  @override
  $InfraFailureCopyWith<dynamic, $Res> get failure {
    if (_value.failure == null) {
      return null;
    }
    return $InfraFailureCopyWith<dynamic, $Res>(_value.failure, (value) {
      return _then(_value.copyWith(failure: value));
    });
  }
}

/// @nodoc
class _$_LoadFailure implements _LoadFailure {
  const _$_LoadFailure(this.failure) : assert(failure != null);

  @override
  final InfraFailure<dynamic> failure;

  @override
  String toString() {
    return 'EnquiryWatcherState.loadFailure(failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LoadFailure &&
            (identical(other.failure, failure) ||
                const DeepCollectionEquality().equals(other.failure, failure)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(failure);

  @override
  _$LoadFailureCopyWith<_LoadFailure> get copyWith =>
      __$LoadFailureCopyWithImpl<_LoadFailure>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadSuccess(List<Enquiry> enquiry),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadSuccess != null);
    assert(loadFailure != null);
    return loadFailure(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadSuccess(List<Enquiry> enquiry),
    TResult loadFailure(InfraFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadFailure != null) {
      return loadFailure(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadSuccess(_LoadSuccess value),
    @required TResult loadFailure(_LoadFailure value),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadSuccess != null);
    assert(loadFailure != null);
    return loadFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadSuccess(_LoadSuccess value),
    TResult loadFailure(_LoadFailure value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadFailure != null) {
      return loadFailure(this);
    }
    return orElse();
  }
}

abstract class _LoadFailure implements EnquiryWatcherState {
  const factory _LoadFailure(InfraFailure<dynamic> failure) = _$_LoadFailure;

  InfraFailure<dynamic> get failure;
  _$LoadFailureCopyWith<_LoadFailure> get copyWith;
}
