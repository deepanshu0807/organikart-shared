part of 'enquiry_watcher_bloc.dart';

@freezed
abstract class EnquiryWatcherState with _$EnquiryWatcherState {
  const factory EnquiryWatcherState.initial() = _Initial;
  const factory EnquiryWatcherState.loadInProgress() = _DataTransferInProgress;
  const factory EnquiryWatcherState.loadSuccess(List<Enquiry> enquiry) =
      _LoadSuccess;
  const factory EnquiryWatcherState.loadFailure(InfraFailure failure) =
      _LoadFailure;
}
