part of 'enquiry_watcher_bloc.dart';

@freezed
abstract class EnquiryWatcherEvent with _$EnquiryWatcherEvent {
  const factory EnquiryWatcherEvent.getEnquiry() = _GetEnquiry;
  const factory EnquiryWatcherEvent.enquiryReceived(
      Either<InfraFailure, List<Enquiry>> enquiry) = _EnquiryReceived;
}
