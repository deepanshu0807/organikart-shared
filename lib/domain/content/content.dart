import 'package:organikart_shared/domain/category/category.dart';
import 'package:organikart_shared/domain/content/qty_unit.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

part 'content.freezed.dart';

@freezed
abstract class Content with _$Content {
  const factory Content({
    @required UniqueId id,
    @required String picUrl,
    @required String name,
    @required Category category,
    @required String description,
    @required Price mrp,
    @required QtyUnit qtyUnit,
    @required Price discountedMrp,
    @required Percentage discountPercentage,
  }) = _Content;
}
