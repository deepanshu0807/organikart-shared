import 'package:freezed_annotation/freezed_annotation.dart';

part 'qty_unit.freezed.dart';

@freezed
abstract class QtyUnit with _$QtyUnit {
  const factory QtyUnit.kg() = _KG;
  const factory QtyUnit.gm() = _Gram;
  const factory QtyUnit.ltr() = _Ltr;
  const factory QtyUnit.piece() = _Piece;
  const factory QtyUnit.ml() = _Ml;
  const factory QtyUnit.dozen() = _Dozen;
}

extension QtyUnitX on String {
  QtyUnit toQtyUnit() {
    switch (this) {
      case "Kg":
        return const QtyUnit.kg();
        break;
      case "gm":
        return const QtyUnit.gm();
        break;
      case "Ltr":
        return const QtyUnit.ltr();
        break;
      case "ml":
        return const QtyUnit.ml();
        break;
      case "Dozen":
        return const QtyUnit.dozen();
        break;
      case "Piece":
        return const QtyUnit.piece();
        break;
      default:
        return const QtyUnit.kg();
    }
  }
}

extension QtyUnitXX on QtyUnit {
  String toValueString() {
    return map(
        kg: (_) => "Kg",
        gm: (_) => "gm",
        ltr: (_) => "Ltr",
        ml: (_) => "ml",
        dozen: (_) => "Dozen",
        piece: (_) => "Piece");
  }
}
