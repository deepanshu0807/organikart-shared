// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'qty_unit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$QtyUnitTearOff {
  const _$QtyUnitTearOff();

// ignore: unused_element
  _KG kg() {
    return const _KG();
  }

// ignore: unused_element
  _Gram gm() {
    return const _Gram();
  }

// ignore: unused_element
  _Ltr ltr() {
    return const _Ltr();
  }

// ignore: unused_element
  _Piece piece() {
    return const _Piece();
  }

// ignore: unused_element
  _Ml ml() {
    return const _Ml();
  }

// ignore: unused_element
  _Dozen dozen() {
    return const _Dozen();
  }
}

/// @nodoc
// ignore: unused_element
const $QtyUnit = _$QtyUnitTearOff();

/// @nodoc
mixin _$QtyUnit {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult kg(),
    @required TResult gm(),
    @required TResult ltr(),
    @required TResult piece(),
    @required TResult ml(),
    @required TResult dozen(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult kg(),
    TResult gm(),
    TResult ltr(),
    TResult piece(),
    TResult ml(),
    TResult dozen(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult kg(_KG value),
    @required TResult gm(_Gram value),
    @required TResult ltr(_Ltr value),
    @required TResult piece(_Piece value),
    @required TResult ml(_Ml value),
    @required TResult dozen(_Dozen value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult kg(_KG value),
    TResult gm(_Gram value),
    TResult ltr(_Ltr value),
    TResult piece(_Piece value),
    TResult ml(_Ml value),
    TResult dozen(_Dozen value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $QtyUnitCopyWith<$Res> {
  factory $QtyUnitCopyWith(QtyUnit value, $Res Function(QtyUnit) then) =
      _$QtyUnitCopyWithImpl<$Res>;
}

/// @nodoc
class _$QtyUnitCopyWithImpl<$Res> implements $QtyUnitCopyWith<$Res> {
  _$QtyUnitCopyWithImpl(this._value, this._then);

  final QtyUnit _value;
  // ignore: unused_field
  final $Res Function(QtyUnit) _then;
}

/// @nodoc
abstract class _$KGCopyWith<$Res> {
  factory _$KGCopyWith(_KG value, $Res Function(_KG) then) =
      __$KGCopyWithImpl<$Res>;
}

/// @nodoc
class __$KGCopyWithImpl<$Res> extends _$QtyUnitCopyWithImpl<$Res>
    implements _$KGCopyWith<$Res> {
  __$KGCopyWithImpl(_KG _value, $Res Function(_KG) _then)
      : super(_value, (v) => _then(v as _KG));

  @override
  _KG get _value => super._value as _KG;
}

/// @nodoc
class _$_KG implements _KG {
  const _$_KG();

  @override
  String toString() {
    return 'QtyUnit.kg()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _KG);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult kg(),
    @required TResult gm(),
    @required TResult ltr(),
    @required TResult piece(),
    @required TResult ml(),
    @required TResult dozen(),
  }) {
    assert(kg != null);
    assert(gm != null);
    assert(ltr != null);
    assert(piece != null);
    assert(ml != null);
    assert(dozen != null);
    return kg();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult kg(),
    TResult gm(),
    TResult ltr(),
    TResult piece(),
    TResult ml(),
    TResult dozen(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (kg != null) {
      return kg();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult kg(_KG value),
    @required TResult gm(_Gram value),
    @required TResult ltr(_Ltr value),
    @required TResult piece(_Piece value),
    @required TResult ml(_Ml value),
    @required TResult dozen(_Dozen value),
  }) {
    assert(kg != null);
    assert(gm != null);
    assert(ltr != null);
    assert(piece != null);
    assert(ml != null);
    assert(dozen != null);
    return kg(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult kg(_KG value),
    TResult gm(_Gram value),
    TResult ltr(_Ltr value),
    TResult piece(_Piece value),
    TResult ml(_Ml value),
    TResult dozen(_Dozen value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (kg != null) {
      return kg(this);
    }
    return orElse();
  }
}

abstract class _KG implements QtyUnit {
  const factory _KG() = _$_KG;
}

/// @nodoc
abstract class _$GramCopyWith<$Res> {
  factory _$GramCopyWith(_Gram value, $Res Function(_Gram) then) =
      __$GramCopyWithImpl<$Res>;
}

/// @nodoc
class __$GramCopyWithImpl<$Res> extends _$QtyUnitCopyWithImpl<$Res>
    implements _$GramCopyWith<$Res> {
  __$GramCopyWithImpl(_Gram _value, $Res Function(_Gram) _then)
      : super(_value, (v) => _then(v as _Gram));

  @override
  _Gram get _value => super._value as _Gram;
}

/// @nodoc
class _$_Gram implements _Gram {
  const _$_Gram();

  @override
  String toString() {
    return 'QtyUnit.gm()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Gram);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult kg(),
    @required TResult gm(),
    @required TResult ltr(),
    @required TResult piece(),
    @required TResult ml(),
    @required TResult dozen(),
  }) {
    assert(kg != null);
    assert(gm != null);
    assert(ltr != null);
    assert(piece != null);
    assert(ml != null);
    assert(dozen != null);
    return gm();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult kg(),
    TResult gm(),
    TResult ltr(),
    TResult piece(),
    TResult ml(),
    TResult dozen(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (gm != null) {
      return gm();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult kg(_KG value),
    @required TResult gm(_Gram value),
    @required TResult ltr(_Ltr value),
    @required TResult piece(_Piece value),
    @required TResult ml(_Ml value),
    @required TResult dozen(_Dozen value),
  }) {
    assert(kg != null);
    assert(gm != null);
    assert(ltr != null);
    assert(piece != null);
    assert(ml != null);
    assert(dozen != null);
    return gm(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult kg(_KG value),
    TResult gm(_Gram value),
    TResult ltr(_Ltr value),
    TResult piece(_Piece value),
    TResult ml(_Ml value),
    TResult dozen(_Dozen value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (gm != null) {
      return gm(this);
    }
    return orElse();
  }
}

abstract class _Gram implements QtyUnit {
  const factory _Gram() = _$_Gram;
}

/// @nodoc
abstract class _$LtrCopyWith<$Res> {
  factory _$LtrCopyWith(_Ltr value, $Res Function(_Ltr) then) =
      __$LtrCopyWithImpl<$Res>;
}

/// @nodoc
class __$LtrCopyWithImpl<$Res> extends _$QtyUnitCopyWithImpl<$Res>
    implements _$LtrCopyWith<$Res> {
  __$LtrCopyWithImpl(_Ltr _value, $Res Function(_Ltr) _then)
      : super(_value, (v) => _then(v as _Ltr));

  @override
  _Ltr get _value => super._value as _Ltr;
}

/// @nodoc
class _$_Ltr implements _Ltr {
  const _$_Ltr();

  @override
  String toString() {
    return 'QtyUnit.ltr()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Ltr);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult kg(),
    @required TResult gm(),
    @required TResult ltr(),
    @required TResult piece(),
    @required TResult ml(),
    @required TResult dozen(),
  }) {
    assert(kg != null);
    assert(gm != null);
    assert(ltr != null);
    assert(piece != null);
    assert(ml != null);
    assert(dozen != null);
    return ltr();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult kg(),
    TResult gm(),
    TResult ltr(),
    TResult piece(),
    TResult ml(),
    TResult dozen(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (ltr != null) {
      return ltr();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult kg(_KG value),
    @required TResult gm(_Gram value),
    @required TResult ltr(_Ltr value),
    @required TResult piece(_Piece value),
    @required TResult ml(_Ml value),
    @required TResult dozen(_Dozen value),
  }) {
    assert(kg != null);
    assert(gm != null);
    assert(ltr != null);
    assert(piece != null);
    assert(ml != null);
    assert(dozen != null);
    return ltr(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult kg(_KG value),
    TResult gm(_Gram value),
    TResult ltr(_Ltr value),
    TResult piece(_Piece value),
    TResult ml(_Ml value),
    TResult dozen(_Dozen value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (ltr != null) {
      return ltr(this);
    }
    return orElse();
  }
}

abstract class _Ltr implements QtyUnit {
  const factory _Ltr() = _$_Ltr;
}

/// @nodoc
abstract class _$PieceCopyWith<$Res> {
  factory _$PieceCopyWith(_Piece value, $Res Function(_Piece) then) =
      __$PieceCopyWithImpl<$Res>;
}

/// @nodoc
class __$PieceCopyWithImpl<$Res> extends _$QtyUnitCopyWithImpl<$Res>
    implements _$PieceCopyWith<$Res> {
  __$PieceCopyWithImpl(_Piece _value, $Res Function(_Piece) _then)
      : super(_value, (v) => _then(v as _Piece));

  @override
  _Piece get _value => super._value as _Piece;
}

/// @nodoc
class _$_Piece implements _Piece {
  const _$_Piece();

  @override
  String toString() {
    return 'QtyUnit.piece()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Piece);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult kg(),
    @required TResult gm(),
    @required TResult ltr(),
    @required TResult piece(),
    @required TResult ml(),
    @required TResult dozen(),
  }) {
    assert(kg != null);
    assert(gm != null);
    assert(ltr != null);
    assert(piece != null);
    assert(ml != null);
    assert(dozen != null);
    return piece();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult kg(),
    TResult gm(),
    TResult ltr(),
    TResult piece(),
    TResult ml(),
    TResult dozen(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (piece != null) {
      return piece();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult kg(_KG value),
    @required TResult gm(_Gram value),
    @required TResult ltr(_Ltr value),
    @required TResult piece(_Piece value),
    @required TResult ml(_Ml value),
    @required TResult dozen(_Dozen value),
  }) {
    assert(kg != null);
    assert(gm != null);
    assert(ltr != null);
    assert(piece != null);
    assert(ml != null);
    assert(dozen != null);
    return piece(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult kg(_KG value),
    TResult gm(_Gram value),
    TResult ltr(_Ltr value),
    TResult piece(_Piece value),
    TResult ml(_Ml value),
    TResult dozen(_Dozen value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (piece != null) {
      return piece(this);
    }
    return orElse();
  }
}

abstract class _Piece implements QtyUnit {
  const factory _Piece() = _$_Piece;
}

/// @nodoc
abstract class _$MlCopyWith<$Res> {
  factory _$MlCopyWith(_Ml value, $Res Function(_Ml) then) =
      __$MlCopyWithImpl<$Res>;
}

/// @nodoc
class __$MlCopyWithImpl<$Res> extends _$QtyUnitCopyWithImpl<$Res>
    implements _$MlCopyWith<$Res> {
  __$MlCopyWithImpl(_Ml _value, $Res Function(_Ml) _then)
      : super(_value, (v) => _then(v as _Ml));

  @override
  _Ml get _value => super._value as _Ml;
}

/// @nodoc
class _$_Ml implements _Ml {
  const _$_Ml();

  @override
  String toString() {
    return 'QtyUnit.ml()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Ml);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult kg(),
    @required TResult gm(),
    @required TResult ltr(),
    @required TResult piece(),
    @required TResult ml(),
    @required TResult dozen(),
  }) {
    assert(kg != null);
    assert(gm != null);
    assert(ltr != null);
    assert(piece != null);
    assert(ml != null);
    assert(dozen != null);
    return ml();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult kg(),
    TResult gm(),
    TResult ltr(),
    TResult piece(),
    TResult ml(),
    TResult dozen(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (ml != null) {
      return ml();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult kg(_KG value),
    @required TResult gm(_Gram value),
    @required TResult ltr(_Ltr value),
    @required TResult piece(_Piece value),
    @required TResult ml(_Ml value),
    @required TResult dozen(_Dozen value),
  }) {
    assert(kg != null);
    assert(gm != null);
    assert(ltr != null);
    assert(piece != null);
    assert(ml != null);
    assert(dozen != null);
    return ml(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult kg(_KG value),
    TResult gm(_Gram value),
    TResult ltr(_Ltr value),
    TResult piece(_Piece value),
    TResult ml(_Ml value),
    TResult dozen(_Dozen value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (ml != null) {
      return ml(this);
    }
    return orElse();
  }
}

abstract class _Ml implements QtyUnit {
  const factory _Ml() = _$_Ml;
}

/// @nodoc
abstract class _$DozenCopyWith<$Res> {
  factory _$DozenCopyWith(_Dozen value, $Res Function(_Dozen) then) =
      __$DozenCopyWithImpl<$Res>;
}

/// @nodoc
class __$DozenCopyWithImpl<$Res> extends _$QtyUnitCopyWithImpl<$Res>
    implements _$DozenCopyWith<$Res> {
  __$DozenCopyWithImpl(_Dozen _value, $Res Function(_Dozen) _then)
      : super(_value, (v) => _then(v as _Dozen));

  @override
  _Dozen get _value => super._value as _Dozen;
}

/// @nodoc
class _$_Dozen implements _Dozen {
  const _$_Dozen();

  @override
  String toString() {
    return 'QtyUnit.dozen()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Dozen);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult kg(),
    @required TResult gm(),
    @required TResult ltr(),
    @required TResult piece(),
    @required TResult ml(),
    @required TResult dozen(),
  }) {
    assert(kg != null);
    assert(gm != null);
    assert(ltr != null);
    assert(piece != null);
    assert(ml != null);
    assert(dozen != null);
    return dozen();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult kg(),
    TResult gm(),
    TResult ltr(),
    TResult piece(),
    TResult ml(),
    TResult dozen(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (dozen != null) {
      return dozen();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult kg(_KG value),
    @required TResult gm(_Gram value),
    @required TResult ltr(_Ltr value),
    @required TResult piece(_Piece value),
    @required TResult ml(_Ml value),
    @required TResult dozen(_Dozen value),
  }) {
    assert(kg != null);
    assert(gm != null);
    assert(ltr != null);
    assert(piece != null);
    assert(ml != null);
    assert(dozen != null);
    return dozen(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult kg(_KG value),
    TResult gm(_Gram value),
    TResult ltr(_Ltr value),
    TResult piece(_Piece value),
    TResult ml(_Ml value),
    TResult dozen(_Dozen value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (dozen != null) {
      return dozen(this);
    }
    return orElse();
  }
}

abstract class _Dozen implements QtyUnit {
  const factory _Dozen() = _$_Dozen;
}
