import 'package:dartz/dartz.dart';
import 'package:organikart_shared/domain/content/content.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

abstract class IContentRepo {
  Stream<Either<InfraFailure, List<Content>>> getAllContent();
  Future<Either<InfraFailure, Unit>> create(Content content);
  Future<Either<InfraFailure, Unit>> delete(Content content);

  Future<Either<InfraFailure, UploadResult>> uploadMainContentImage(
      Content content);
}
