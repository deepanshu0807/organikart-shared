// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'content.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$ContentTearOff {
  const _$ContentTearOff();

// ignore: unused_element
  _Content call(
      {@required UniqueId id,
      @required String picUrl,
      @required String name,
      @required Category category,
      @required String description,
      @required Price mrp,
      @required QtyUnit qtyUnit,
      @required Price discountedMrp,
      @required Percentage discountPercentage}) {
    return _Content(
      id: id,
      picUrl: picUrl,
      name: name,
      category: category,
      description: description,
      mrp: mrp,
      qtyUnit: qtyUnit,
      discountedMrp: discountedMrp,
      discountPercentage: discountPercentage,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $Content = _$ContentTearOff();

/// @nodoc
mixin _$Content {
  UniqueId get id;
  String get picUrl;
  String get name;
  Category get category;
  String get description;
  Price get mrp;
  QtyUnit get qtyUnit;
  Price get discountedMrp;
  Percentage get discountPercentage;

  $ContentCopyWith<Content> get copyWith;
}

/// @nodoc
abstract class $ContentCopyWith<$Res> {
  factory $ContentCopyWith(Content value, $Res Function(Content) then) =
      _$ContentCopyWithImpl<$Res>;
  $Res call(
      {UniqueId id,
      String picUrl,
      String name,
      Category category,
      String description,
      Price mrp,
      QtyUnit qtyUnit,
      Price discountedMrp,
      Percentage discountPercentage});

  $CategoryCopyWith<$Res> get category;
  $QtyUnitCopyWith<$Res> get qtyUnit;
}

/// @nodoc
class _$ContentCopyWithImpl<$Res> implements $ContentCopyWith<$Res> {
  _$ContentCopyWithImpl(this._value, this._then);

  final Content _value;
  // ignore: unused_field
  final $Res Function(Content) _then;

  @override
  $Res call({
    Object id = freezed,
    Object picUrl = freezed,
    Object name = freezed,
    Object category = freezed,
    Object description = freezed,
    Object mrp = freezed,
    Object qtyUnit = freezed,
    Object discountedMrp = freezed,
    Object discountPercentage = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as UniqueId,
      picUrl: picUrl == freezed ? _value.picUrl : picUrl as String,
      name: name == freezed ? _value.name : name as String,
      category: category == freezed ? _value.category : category as Category,
      description:
          description == freezed ? _value.description : description as String,
      mrp: mrp == freezed ? _value.mrp : mrp as Price,
      qtyUnit: qtyUnit == freezed ? _value.qtyUnit : qtyUnit as QtyUnit,
      discountedMrp: discountedMrp == freezed
          ? _value.discountedMrp
          : discountedMrp as Price,
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage as Percentage,
    ));
  }

  @override
  $CategoryCopyWith<$Res> get category {
    if (_value.category == null) {
      return null;
    }
    return $CategoryCopyWith<$Res>(_value.category, (value) {
      return _then(_value.copyWith(category: value));
    });
  }

  @override
  $QtyUnitCopyWith<$Res> get qtyUnit {
    if (_value.qtyUnit == null) {
      return null;
    }
    return $QtyUnitCopyWith<$Res>(_value.qtyUnit, (value) {
      return _then(_value.copyWith(qtyUnit: value));
    });
  }
}

/// @nodoc
abstract class _$ContentCopyWith<$Res> implements $ContentCopyWith<$Res> {
  factory _$ContentCopyWith(_Content value, $Res Function(_Content) then) =
      __$ContentCopyWithImpl<$Res>;
  @override
  $Res call(
      {UniqueId id,
      String picUrl,
      String name,
      Category category,
      String description,
      Price mrp,
      QtyUnit qtyUnit,
      Price discountedMrp,
      Percentage discountPercentage});

  @override
  $CategoryCopyWith<$Res> get category;
  @override
  $QtyUnitCopyWith<$Res> get qtyUnit;
}

/// @nodoc
class __$ContentCopyWithImpl<$Res> extends _$ContentCopyWithImpl<$Res>
    implements _$ContentCopyWith<$Res> {
  __$ContentCopyWithImpl(_Content _value, $Res Function(_Content) _then)
      : super(_value, (v) => _then(v as _Content));

  @override
  _Content get _value => super._value as _Content;

  @override
  $Res call({
    Object id = freezed,
    Object picUrl = freezed,
    Object name = freezed,
    Object category = freezed,
    Object description = freezed,
    Object mrp = freezed,
    Object qtyUnit = freezed,
    Object discountedMrp = freezed,
    Object discountPercentage = freezed,
  }) {
    return _then(_Content(
      id: id == freezed ? _value.id : id as UniqueId,
      picUrl: picUrl == freezed ? _value.picUrl : picUrl as String,
      name: name == freezed ? _value.name : name as String,
      category: category == freezed ? _value.category : category as Category,
      description:
          description == freezed ? _value.description : description as String,
      mrp: mrp == freezed ? _value.mrp : mrp as Price,
      qtyUnit: qtyUnit == freezed ? _value.qtyUnit : qtyUnit as QtyUnit,
      discountedMrp: discountedMrp == freezed
          ? _value.discountedMrp
          : discountedMrp as Price,
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage as Percentage,
    ));
  }
}

/// @nodoc
class _$_Content implements _Content {
  const _$_Content(
      {@required this.id,
      @required this.picUrl,
      @required this.name,
      @required this.category,
      @required this.description,
      @required this.mrp,
      @required this.qtyUnit,
      @required this.discountedMrp,
      @required this.discountPercentage})
      : assert(id != null),
        assert(picUrl != null),
        assert(name != null),
        assert(category != null),
        assert(description != null),
        assert(mrp != null),
        assert(qtyUnit != null),
        assert(discountedMrp != null),
        assert(discountPercentage != null);

  @override
  final UniqueId id;
  @override
  final String picUrl;
  @override
  final String name;
  @override
  final Category category;
  @override
  final String description;
  @override
  final Price mrp;
  @override
  final QtyUnit qtyUnit;
  @override
  final Price discountedMrp;
  @override
  final Percentage discountPercentage;

  @override
  String toString() {
    return 'Content(id: $id, picUrl: $picUrl, name: $name, category: $category, description: $description, mrp: $mrp, qtyUnit: $qtyUnit, discountedMrp: $discountedMrp, discountPercentage: $discountPercentage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Content &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.picUrl, picUrl) ||
                const DeepCollectionEquality().equals(other.picUrl, picUrl)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.category, category) ||
                const DeepCollectionEquality()
                    .equals(other.category, category)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.mrp, mrp) ||
                const DeepCollectionEquality().equals(other.mrp, mrp)) &&
            (identical(other.qtyUnit, qtyUnit) ||
                const DeepCollectionEquality()
                    .equals(other.qtyUnit, qtyUnit)) &&
            (identical(other.discountedMrp, discountedMrp) ||
                const DeepCollectionEquality()
                    .equals(other.discountedMrp, discountedMrp)) &&
            (identical(other.discountPercentage, discountPercentage) ||
                const DeepCollectionEquality()
                    .equals(other.discountPercentage, discountPercentage)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(picUrl) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(category) ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(mrp) ^
      const DeepCollectionEquality().hash(qtyUnit) ^
      const DeepCollectionEquality().hash(discountedMrp) ^
      const DeepCollectionEquality().hash(discountPercentage);

  @override
  _$ContentCopyWith<_Content> get copyWith =>
      __$ContentCopyWithImpl<_Content>(this, _$identity);
}

abstract class _Content implements Content {
  const factory _Content(
      {@required UniqueId id,
      @required String picUrl,
      @required String name,
      @required Category category,
      @required String description,
      @required Price mrp,
      @required QtyUnit qtyUnit,
      @required Price discountedMrp,
      @required Percentage discountPercentage}) = _$_Content;

  @override
  UniqueId get id;
  @override
  String get picUrl;
  @override
  String get name;
  @override
  Category get category;
  @override
  String get description;
  @override
  Price get mrp;
  @override
  QtyUnit get qtyUnit;
  @override
  Price get discountedMrp;
  @override
  Percentage get discountPercentage;
  @override
  _$ContentCopyWith<_Content> get copyWith;
}
