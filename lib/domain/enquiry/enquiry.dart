import 'package:organikart_shared/organikart_shared_package.dart';

part 'enquiry.freezed.dart';

@freezed
abstract class Enquiry with _$Enquiry {
  const factory Enquiry({
    @required UniqueId id,
    @required EmailAddress emailAddress,
    @required String message,
    @required Name name,
    @required PhoneNumber phoneNumber,
  }) = _Enquiry;
}
