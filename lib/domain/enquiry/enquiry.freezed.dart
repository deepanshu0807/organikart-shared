// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'enquiry.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$EnquiryTearOff {
  const _$EnquiryTearOff();

// ignore: unused_element
  _Enquiry call(
      {@required UniqueId id,
      @required EmailAddress emailAddress,
      @required String message,
      @required Name name,
      @required PhoneNumber phoneNumber}) {
    return _Enquiry(
      id: id,
      emailAddress: emailAddress,
      message: message,
      name: name,
      phoneNumber: phoneNumber,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $Enquiry = _$EnquiryTearOff();

/// @nodoc
mixin _$Enquiry {
  UniqueId get id;
  EmailAddress get emailAddress;
  String get message;
  Name get name;
  PhoneNumber get phoneNumber;

  $EnquiryCopyWith<Enquiry> get copyWith;
}

/// @nodoc
abstract class $EnquiryCopyWith<$Res> {
  factory $EnquiryCopyWith(Enquiry value, $Res Function(Enquiry) then) =
      _$EnquiryCopyWithImpl<$Res>;
  $Res call(
      {UniqueId id,
      EmailAddress emailAddress,
      String message,
      Name name,
      PhoneNumber phoneNumber});
}

/// @nodoc
class _$EnquiryCopyWithImpl<$Res> implements $EnquiryCopyWith<$Res> {
  _$EnquiryCopyWithImpl(this._value, this._then);

  final Enquiry _value;
  // ignore: unused_field
  final $Res Function(Enquiry) _then;

  @override
  $Res call({
    Object id = freezed,
    Object emailAddress = freezed,
    Object message = freezed,
    Object name = freezed,
    Object phoneNumber = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as UniqueId,
      emailAddress: emailAddress == freezed
          ? _value.emailAddress
          : emailAddress as EmailAddress,
      message: message == freezed ? _value.message : message as String,
      name: name == freezed ? _value.name : name as Name,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber as PhoneNumber,
    ));
  }
}

/// @nodoc
abstract class _$EnquiryCopyWith<$Res> implements $EnquiryCopyWith<$Res> {
  factory _$EnquiryCopyWith(_Enquiry value, $Res Function(_Enquiry) then) =
      __$EnquiryCopyWithImpl<$Res>;
  @override
  $Res call(
      {UniqueId id,
      EmailAddress emailAddress,
      String message,
      Name name,
      PhoneNumber phoneNumber});
}

/// @nodoc
class __$EnquiryCopyWithImpl<$Res> extends _$EnquiryCopyWithImpl<$Res>
    implements _$EnquiryCopyWith<$Res> {
  __$EnquiryCopyWithImpl(_Enquiry _value, $Res Function(_Enquiry) _then)
      : super(_value, (v) => _then(v as _Enquiry));

  @override
  _Enquiry get _value => super._value as _Enquiry;

  @override
  $Res call({
    Object id = freezed,
    Object emailAddress = freezed,
    Object message = freezed,
    Object name = freezed,
    Object phoneNumber = freezed,
  }) {
    return _then(_Enquiry(
      id: id == freezed ? _value.id : id as UniqueId,
      emailAddress: emailAddress == freezed
          ? _value.emailAddress
          : emailAddress as EmailAddress,
      message: message == freezed ? _value.message : message as String,
      name: name == freezed ? _value.name : name as Name,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber as PhoneNumber,
    ));
  }
}

/// @nodoc
class _$_Enquiry implements _Enquiry {
  const _$_Enquiry(
      {@required this.id,
      @required this.emailAddress,
      @required this.message,
      @required this.name,
      @required this.phoneNumber})
      : assert(id != null),
        assert(emailAddress != null),
        assert(message != null),
        assert(name != null),
        assert(phoneNumber != null);

  @override
  final UniqueId id;
  @override
  final EmailAddress emailAddress;
  @override
  final String message;
  @override
  final Name name;
  @override
  final PhoneNumber phoneNumber;

  @override
  String toString() {
    return 'Enquiry(id: $id, emailAddress: $emailAddress, message: $message, name: $name, phoneNumber: $phoneNumber)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Enquiry &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.emailAddress, emailAddress) ||
                const DeepCollectionEquality()
                    .equals(other.emailAddress, emailAddress)) &&
            (identical(other.message, message) ||
                const DeepCollectionEquality()
                    .equals(other.message, message)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.phoneNumber, phoneNumber) ||
                const DeepCollectionEquality()
                    .equals(other.phoneNumber, phoneNumber)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(emailAddress) ^
      const DeepCollectionEquality().hash(message) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(phoneNumber);

  @override
  _$EnquiryCopyWith<_Enquiry> get copyWith =>
      __$EnquiryCopyWithImpl<_Enquiry>(this, _$identity);
}

abstract class _Enquiry implements Enquiry {
  const factory _Enquiry(
      {@required UniqueId id,
      @required EmailAddress emailAddress,
      @required String message,
      @required Name name,
      @required PhoneNumber phoneNumber}) = _$_Enquiry;

  @override
  UniqueId get id;
  @override
  EmailAddress get emailAddress;
  @override
  String get message;
  @override
  Name get name;
  @override
  PhoneNumber get phoneNumber;
  @override
  _$EnquiryCopyWith<_Enquiry> get copyWith;
}
