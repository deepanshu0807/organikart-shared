import 'package:dartz/dartz.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

import 'enquiry.dart';

abstract class IEnquiryRepo {
  Stream<Either<InfraFailure, List<Enquiry>>> getEnquiry();
  Future<Either<InfraFailure, Unit>> create(Enquiry enquiry);
  Future<Either<InfraFailure, Unit>> delete(Enquiry enquiry);
}
