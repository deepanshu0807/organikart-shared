// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'saved_products.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$SavedProductsTearOff {
  const _$SavedProductsTearOff();

// ignore: unused_element
  _SavedProducts call({List<Content> allProducts}) {
    return _SavedProducts(
      allProducts: allProducts,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $SavedProducts = _$SavedProductsTearOff();

/// @nodoc
mixin _$SavedProducts {
  List<Content> get allProducts;

  $SavedProductsCopyWith<SavedProducts> get copyWith;
}

/// @nodoc
abstract class $SavedProductsCopyWith<$Res> {
  factory $SavedProductsCopyWith(
          SavedProducts value, $Res Function(SavedProducts) then) =
      _$SavedProductsCopyWithImpl<$Res>;
  $Res call({List<Content> allProducts});
}

/// @nodoc
class _$SavedProductsCopyWithImpl<$Res>
    implements $SavedProductsCopyWith<$Res> {
  _$SavedProductsCopyWithImpl(this._value, this._then);

  final SavedProducts _value;
  // ignore: unused_field
  final $Res Function(SavedProducts) _then;

  @override
  $Res call({
    Object allProducts = freezed,
  }) {
    return _then(_value.copyWith(
      allProducts: allProducts == freezed
          ? _value.allProducts
          : allProducts as List<Content>,
    ));
  }
}

/// @nodoc
abstract class _$SavedProductsCopyWith<$Res>
    implements $SavedProductsCopyWith<$Res> {
  factory _$SavedProductsCopyWith(
          _SavedProducts value, $Res Function(_SavedProducts) then) =
      __$SavedProductsCopyWithImpl<$Res>;
  @override
  $Res call({List<Content> allProducts});
}

/// @nodoc
class __$SavedProductsCopyWithImpl<$Res>
    extends _$SavedProductsCopyWithImpl<$Res>
    implements _$SavedProductsCopyWith<$Res> {
  __$SavedProductsCopyWithImpl(
      _SavedProducts _value, $Res Function(_SavedProducts) _then)
      : super(_value, (v) => _then(v as _SavedProducts));

  @override
  _SavedProducts get _value => super._value as _SavedProducts;

  @override
  $Res call({
    Object allProducts = freezed,
  }) {
    return _then(_SavedProducts(
      allProducts: allProducts == freezed
          ? _value.allProducts
          : allProducts as List<Content>,
    ));
  }
}

/// @nodoc
class _$_SavedProducts implements _SavedProducts {
  const _$_SavedProducts({this.allProducts});

  @override
  final List<Content> allProducts;

  @override
  String toString() {
    return 'SavedProducts(allProducts: $allProducts)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SavedProducts &&
            (identical(other.allProducts, allProducts) ||
                const DeepCollectionEquality()
                    .equals(other.allProducts, allProducts)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(allProducts);

  @override
  _$SavedProductsCopyWith<_SavedProducts> get copyWith =>
      __$SavedProductsCopyWithImpl<_SavedProducts>(this, _$identity);
}

abstract class _SavedProducts implements SavedProducts {
  const factory _SavedProducts({List<Content> allProducts}) = _$_SavedProducts;

  @override
  List<Content> get allProducts;
  @override
  _$SavedProductsCopyWith<_SavedProducts> get copyWith;
}
