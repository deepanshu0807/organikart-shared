import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:organikart_shared/domain/content/content.dart';

part 'saved_products.freezed.dart';

@freezed
abstract class SavedProducts with _$SavedProducts {
  const factory SavedProducts({
    List<Content> allProducts,
  }) = _SavedProducts;
}

extension SavedProductsX on SavedProducts {
  bool isValid() {
    return allProducts.isNotEmpty;
  }
}
