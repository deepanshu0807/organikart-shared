import 'package:dartz/dartz.dart';
import 'package:organikart_shared/domain/auth/auth_domain.dart';
import 'package:organikart_shared/domain/features/saved_products.dart';

import '../../organikart_shared_package.dart';

abstract class ICustomerRepo {
  Stream<Either<InfraFailure, OrganikartUser>> getStoreUser(
      OrganikartUser user);
  Stream<Either<InfraFailure, List<OrganikartUser>>> getAllUsers();

  Stream<Either<InfraFailure, SavedProducts>> getMySavedProducts(
      OrganikartUser user);

  Future<Either<InfraFailure, Unit>> create(OrganikartUser user);
  Future<Either<InfraFailure, Unit>> update(OrganikartUser user);
  Future<Either<InfraFailure, Unit>> delete(OrganikartUser user);

  Future<Either<InfraFailure, Unit>> saveOrRemoveProductToMySavedList(
      OrganikartUser user, Content allProducts);
}
