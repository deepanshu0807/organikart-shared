import 'package:organikart_shared/organikart_shared_package.dart';

part 'category.freezed.dart';

@freezed
abstract class Category with _$Category {
  const factory Category({
    @required UniqueId id,
    @required String title,
    @required String titlePicUrl,
    String shortDescription,
  }) = _Category;
}
