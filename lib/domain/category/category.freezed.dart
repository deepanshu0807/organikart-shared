// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'category.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$CategoryTearOff {
  const _$CategoryTearOff();

// ignore: unused_element
  _Category call(
      {@required UniqueId id,
      @required String title,
      @required String titlePicUrl,
      String shortDescription}) {
    return _Category(
      id: id,
      title: title,
      titlePicUrl: titlePicUrl,
      shortDescription: shortDescription,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $Category = _$CategoryTearOff();

/// @nodoc
mixin _$Category {
  UniqueId get id;
  String get title;
  String get titlePicUrl;
  String get shortDescription;

  $CategoryCopyWith<Category> get copyWith;
}

/// @nodoc
abstract class $CategoryCopyWith<$Res> {
  factory $CategoryCopyWith(Category value, $Res Function(Category) then) =
      _$CategoryCopyWithImpl<$Res>;
  $Res call(
      {UniqueId id, String title, String titlePicUrl, String shortDescription});
}

/// @nodoc
class _$CategoryCopyWithImpl<$Res> implements $CategoryCopyWith<$Res> {
  _$CategoryCopyWithImpl(this._value, this._then);

  final Category _value;
  // ignore: unused_field
  final $Res Function(Category) _then;

  @override
  $Res call({
    Object id = freezed,
    Object title = freezed,
    Object titlePicUrl = freezed,
    Object shortDescription = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as UniqueId,
      title: title == freezed ? _value.title : title as String,
      titlePicUrl:
          titlePicUrl == freezed ? _value.titlePicUrl : titlePicUrl as String,
      shortDescription: shortDescription == freezed
          ? _value.shortDescription
          : shortDescription as String,
    ));
  }
}

/// @nodoc
abstract class _$CategoryCopyWith<$Res> implements $CategoryCopyWith<$Res> {
  factory _$CategoryCopyWith(_Category value, $Res Function(_Category) then) =
      __$CategoryCopyWithImpl<$Res>;
  @override
  $Res call(
      {UniqueId id, String title, String titlePicUrl, String shortDescription});
}

/// @nodoc
class __$CategoryCopyWithImpl<$Res> extends _$CategoryCopyWithImpl<$Res>
    implements _$CategoryCopyWith<$Res> {
  __$CategoryCopyWithImpl(_Category _value, $Res Function(_Category) _then)
      : super(_value, (v) => _then(v as _Category));

  @override
  _Category get _value => super._value as _Category;

  @override
  $Res call({
    Object id = freezed,
    Object title = freezed,
    Object titlePicUrl = freezed,
    Object shortDescription = freezed,
  }) {
    return _then(_Category(
      id: id == freezed ? _value.id : id as UniqueId,
      title: title == freezed ? _value.title : title as String,
      titlePicUrl:
          titlePicUrl == freezed ? _value.titlePicUrl : titlePicUrl as String,
      shortDescription: shortDescription == freezed
          ? _value.shortDescription
          : shortDescription as String,
    ));
  }
}

/// @nodoc
class _$_Category implements _Category {
  const _$_Category(
      {@required this.id,
      @required this.title,
      @required this.titlePicUrl,
      this.shortDescription})
      : assert(id != null),
        assert(title != null),
        assert(titlePicUrl != null);

  @override
  final UniqueId id;
  @override
  final String title;
  @override
  final String titlePicUrl;
  @override
  final String shortDescription;

  @override
  String toString() {
    return 'Category(id: $id, title: $title, titlePicUrl: $titlePicUrl, shortDescription: $shortDescription)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Category &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.titlePicUrl, titlePicUrl) ||
                const DeepCollectionEquality()
                    .equals(other.titlePicUrl, titlePicUrl)) &&
            (identical(other.shortDescription, shortDescription) ||
                const DeepCollectionEquality()
                    .equals(other.shortDescription, shortDescription)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(titlePicUrl) ^
      const DeepCollectionEquality().hash(shortDescription);

  @override
  _$CategoryCopyWith<_Category> get copyWith =>
      __$CategoryCopyWithImpl<_Category>(this, _$identity);
}

abstract class _Category implements Category {
  const factory _Category(
      {@required UniqueId id,
      @required String title,
      @required String titlePicUrl,
      String shortDescription}) = _$_Category;

  @override
  UniqueId get id;
  @override
  String get title;
  @override
  String get titlePicUrl;
  @override
  String get shortDescription;
  @override
  _$CategoryCopyWith<_Category> get copyWith;
}
