import 'package:dartz/dartz.dart';
import 'package:organikart_shared/domain/category/category.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

abstract class ICategoryRepo {
  Stream<Either<InfraFailure, List<Category>>> getAllCategory();
  Future<Either<InfraFailure, Unit>> create(Category category);
  Future<Either<InfraFailure, Unit>> delete(Category category);
  Future<Either<InfraFailure, UploadResult>> uploadTitleImage(
      Category category);
}
