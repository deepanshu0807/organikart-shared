import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:organikart_shared/domain/orders/orders.dart';
import 'package:organikart_shared/domain/orders/payment_method.dart';

abstract class IPaymentRepo {
  Future<Unit> onlinePaymentForOrder({
    @required OrderEntity order,
    @required
        Function(OrderEntity order, PaymentMethod method) onPaymentSuccess,
    @required Function(OrderEntity order, PaymentMethod method) onPaymentFailed,
  });
}
