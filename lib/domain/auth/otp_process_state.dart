import 'package:freezed_annotation/freezed_annotation.dart';

import 'auth_failures.dart';
part 'otp_process_state.freezed.dart';

@freezed
abstract class OTPProcessState<T> with _$OTPProcessState<T> {
  const factory OTPProcessState.initial() = _Initial<T>;
  const factory OTPProcessState.phoneNumberSubmiting() =
      _PhoneNumberSubmiting<T>;
  const factory OTPProcessState.phoneNumberSubmitSuccess() =
      _PhoneNumberSubmitSuccess<T>;

  const factory OTPProcessState.phoneNumberSubmitFailed(AuthFailure failure) =
      _PhoneNumberSubmitFailed<T>;
  const factory OTPProcessState.waitingForOTP() = _WaitingForOTP<T>;
  const factory OTPProcessState.waitingForOTPExpired() =
      _WaitingForOTPExpired<T>;
  const factory OTPProcessState.submitingOTP() = _SubmitingOTP<T>;
  const factory OTPProcessState.submitOTPSuccess() = _SubmitOTPSuccess<T>;
  const factory OTPProcessState.submitOTPFailed(AuthFailure failure) =
      _SubmitOTPFailed<T>;
  const factory OTPProcessState.otpVerified() = _OtpVerified<T>;
  const factory OTPProcessState.otpVerificationFailed(AuthFailure failure) =
      _OtpVerificationFailed<T>;
}
