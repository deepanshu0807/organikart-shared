// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'user_role.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$UserRoleTearOff {
  const _$UserRoleTearOff();

// ignore: unused_element
  SuperAdmin superAdmin() {
    return const SuperAdmin();
  }

// ignore: unused_element
  Customer customer() {
    return const Customer();
  }
}

/// @nodoc
// ignore: unused_element
const $UserRole = _$UserRoleTearOff();

/// @nodoc
mixin _$UserRole {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult superAdmin(),
    @required TResult customer(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult superAdmin(),
    TResult customer(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult superAdmin(SuperAdmin value),
    @required TResult customer(Customer value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult superAdmin(SuperAdmin value),
    TResult customer(Customer value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $UserRoleCopyWith<$Res> {
  factory $UserRoleCopyWith(UserRole value, $Res Function(UserRole) then) =
      _$UserRoleCopyWithImpl<$Res>;
}

/// @nodoc
class _$UserRoleCopyWithImpl<$Res> implements $UserRoleCopyWith<$Res> {
  _$UserRoleCopyWithImpl(this._value, this._then);

  final UserRole _value;
  // ignore: unused_field
  final $Res Function(UserRole) _then;
}

/// @nodoc
abstract class $SuperAdminCopyWith<$Res> {
  factory $SuperAdminCopyWith(
          SuperAdmin value, $Res Function(SuperAdmin) then) =
      _$SuperAdminCopyWithImpl<$Res>;
}

/// @nodoc
class _$SuperAdminCopyWithImpl<$Res> extends _$UserRoleCopyWithImpl<$Res>
    implements $SuperAdminCopyWith<$Res> {
  _$SuperAdminCopyWithImpl(SuperAdmin _value, $Res Function(SuperAdmin) _then)
      : super(_value, (v) => _then(v as SuperAdmin));

  @override
  SuperAdmin get _value => super._value as SuperAdmin;
}

/// @nodoc
class _$SuperAdmin implements SuperAdmin {
  const _$SuperAdmin();

  @override
  String toString() {
    return 'UserRole.superAdmin()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is SuperAdmin);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult superAdmin(),
    @required TResult customer(),
  }) {
    assert(superAdmin != null);
    assert(customer != null);
    return superAdmin();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult superAdmin(),
    TResult customer(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (superAdmin != null) {
      return superAdmin();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult superAdmin(SuperAdmin value),
    @required TResult customer(Customer value),
  }) {
    assert(superAdmin != null);
    assert(customer != null);
    return superAdmin(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult superAdmin(SuperAdmin value),
    TResult customer(Customer value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (superAdmin != null) {
      return superAdmin(this);
    }
    return orElse();
  }
}

abstract class SuperAdmin implements UserRole {
  const factory SuperAdmin() = _$SuperAdmin;
}

/// @nodoc
abstract class $CustomerCopyWith<$Res> {
  factory $CustomerCopyWith(Customer value, $Res Function(Customer) then) =
      _$CustomerCopyWithImpl<$Res>;
}

/// @nodoc
class _$CustomerCopyWithImpl<$Res> extends _$UserRoleCopyWithImpl<$Res>
    implements $CustomerCopyWith<$Res> {
  _$CustomerCopyWithImpl(Customer _value, $Res Function(Customer) _then)
      : super(_value, (v) => _then(v as Customer));

  @override
  Customer get _value => super._value as Customer;
}

/// @nodoc
class _$Customer implements Customer {
  const _$Customer();

  @override
  String toString() {
    return 'UserRole.customer()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is Customer);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult superAdmin(),
    @required TResult customer(),
  }) {
    assert(superAdmin != null);
    assert(customer != null);
    return customer();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult superAdmin(),
    TResult customer(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (customer != null) {
      return customer();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult superAdmin(SuperAdmin value),
    @required TResult customer(Customer value),
  }) {
    assert(superAdmin != null);
    assert(customer != null);
    return customer(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult superAdmin(SuperAdmin value),
    TResult customer(Customer value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (customer != null) {
      return customer(this);
    }
    return orElse();
  }
}

abstract class Customer implements UserRole {
  const factory Customer() = _$Customer;
}
