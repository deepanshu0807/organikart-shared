// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'user.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$OrganikartUserTearOff {
  const _$OrganikartUserTearOff();

// ignore: unused_element
  _OrganikartUser call(
      {@required UniqueId uId,
      @required Name name,
      @required EmailAddress emailAddress,
      String picUrl,
      @required PhoneNumber phoneNumber,
      @required UserRole role,
      @required DateTime lastSignInDateTime}) {
    return _OrganikartUser(
      uId: uId,
      name: name,
      emailAddress: emailAddress,
      picUrl: picUrl,
      phoneNumber: phoneNumber,
      role: role,
      lastSignInDateTime: lastSignInDateTime,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $OrganikartUser = _$OrganikartUserTearOff();

/// @nodoc
mixin _$OrganikartUser {
  UniqueId get uId;
  Name get name;
  EmailAddress get emailAddress;
  String get picUrl;
  PhoneNumber get phoneNumber;
  UserRole get role;
  DateTime get lastSignInDateTime;

  $OrganikartUserCopyWith<OrganikartUser> get copyWith;
}

/// @nodoc
abstract class $OrganikartUserCopyWith<$Res> {
  factory $OrganikartUserCopyWith(
          OrganikartUser value, $Res Function(OrganikartUser) then) =
      _$OrganikartUserCopyWithImpl<$Res>;
  $Res call(
      {UniqueId uId,
      Name name,
      EmailAddress emailAddress,
      String picUrl,
      PhoneNumber phoneNumber,
      UserRole role,
      DateTime lastSignInDateTime});

  $UserRoleCopyWith<$Res> get role;
}

/// @nodoc
class _$OrganikartUserCopyWithImpl<$Res>
    implements $OrganikartUserCopyWith<$Res> {
  _$OrganikartUserCopyWithImpl(this._value, this._then);

  final OrganikartUser _value;
  // ignore: unused_field
  final $Res Function(OrganikartUser) _then;

  @override
  $Res call({
    Object uId = freezed,
    Object name = freezed,
    Object emailAddress = freezed,
    Object picUrl = freezed,
    Object phoneNumber = freezed,
    Object role = freezed,
    Object lastSignInDateTime = freezed,
  }) {
    return _then(_value.copyWith(
      uId: uId == freezed ? _value.uId : uId as UniqueId,
      name: name == freezed ? _value.name : name as Name,
      emailAddress: emailAddress == freezed
          ? _value.emailAddress
          : emailAddress as EmailAddress,
      picUrl: picUrl == freezed ? _value.picUrl : picUrl as String,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber as PhoneNumber,
      role: role == freezed ? _value.role : role as UserRole,
      lastSignInDateTime: lastSignInDateTime == freezed
          ? _value.lastSignInDateTime
          : lastSignInDateTime as DateTime,
    ));
  }

  @override
  $UserRoleCopyWith<$Res> get role {
    if (_value.role == null) {
      return null;
    }
    return $UserRoleCopyWith<$Res>(_value.role, (value) {
      return _then(_value.copyWith(role: value));
    });
  }
}

/// @nodoc
abstract class _$OrganikartUserCopyWith<$Res>
    implements $OrganikartUserCopyWith<$Res> {
  factory _$OrganikartUserCopyWith(
          _OrganikartUser value, $Res Function(_OrganikartUser) then) =
      __$OrganikartUserCopyWithImpl<$Res>;
  @override
  $Res call(
      {UniqueId uId,
      Name name,
      EmailAddress emailAddress,
      String picUrl,
      PhoneNumber phoneNumber,
      UserRole role,
      DateTime lastSignInDateTime});

  @override
  $UserRoleCopyWith<$Res> get role;
}

/// @nodoc
class __$OrganikartUserCopyWithImpl<$Res>
    extends _$OrganikartUserCopyWithImpl<$Res>
    implements _$OrganikartUserCopyWith<$Res> {
  __$OrganikartUserCopyWithImpl(
      _OrganikartUser _value, $Res Function(_OrganikartUser) _then)
      : super(_value, (v) => _then(v as _OrganikartUser));

  @override
  _OrganikartUser get _value => super._value as _OrganikartUser;

  @override
  $Res call({
    Object uId = freezed,
    Object name = freezed,
    Object emailAddress = freezed,
    Object picUrl = freezed,
    Object phoneNumber = freezed,
    Object role = freezed,
    Object lastSignInDateTime = freezed,
  }) {
    return _then(_OrganikartUser(
      uId: uId == freezed ? _value.uId : uId as UniqueId,
      name: name == freezed ? _value.name : name as Name,
      emailAddress: emailAddress == freezed
          ? _value.emailAddress
          : emailAddress as EmailAddress,
      picUrl: picUrl == freezed ? _value.picUrl : picUrl as String,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber as PhoneNumber,
      role: role == freezed ? _value.role : role as UserRole,
      lastSignInDateTime: lastSignInDateTime == freezed
          ? _value.lastSignInDateTime
          : lastSignInDateTime as DateTime,
    ));
  }
}

/// @nodoc
class _$_OrganikartUser
    with DiagnosticableTreeMixin
    implements _OrganikartUser {
  const _$_OrganikartUser(
      {@required this.uId,
      @required this.name,
      @required this.emailAddress,
      this.picUrl,
      @required this.phoneNumber,
      @required this.role,
      @required this.lastSignInDateTime})
      : assert(uId != null),
        assert(name != null),
        assert(emailAddress != null),
        assert(phoneNumber != null),
        assert(role != null),
        assert(lastSignInDateTime != null);

  @override
  final UniqueId uId;
  @override
  final Name name;
  @override
  final EmailAddress emailAddress;
  @override
  final String picUrl;
  @override
  final PhoneNumber phoneNumber;
  @override
  final UserRole role;
  @override
  final DateTime lastSignInDateTime;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'OrganikartUser(uId: $uId, name: $name, emailAddress: $emailAddress, picUrl: $picUrl, phoneNumber: $phoneNumber, role: $role, lastSignInDateTime: $lastSignInDateTime)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'OrganikartUser'))
      ..add(DiagnosticsProperty('uId', uId))
      ..add(DiagnosticsProperty('name', name))
      ..add(DiagnosticsProperty('emailAddress', emailAddress))
      ..add(DiagnosticsProperty('picUrl', picUrl))
      ..add(DiagnosticsProperty('phoneNumber', phoneNumber))
      ..add(DiagnosticsProperty('role', role))
      ..add(DiagnosticsProperty('lastSignInDateTime', lastSignInDateTime));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _OrganikartUser &&
            (identical(other.uId, uId) ||
                const DeepCollectionEquality().equals(other.uId, uId)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.emailAddress, emailAddress) ||
                const DeepCollectionEquality()
                    .equals(other.emailAddress, emailAddress)) &&
            (identical(other.picUrl, picUrl) ||
                const DeepCollectionEquality().equals(other.picUrl, picUrl)) &&
            (identical(other.phoneNumber, phoneNumber) ||
                const DeepCollectionEquality()
                    .equals(other.phoneNumber, phoneNumber)) &&
            (identical(other.role, role) ||
                const DeepCollectionEquality().equals(other.role, role)) &&
            (identical(other.lastSignInDateTime, lastSignInDateTime) ||
                const DeepCollectionEquality()
                    .equals(other.lastSignInDateTime, lastSignInDateTime)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(uId) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(emailAddress) ^
      const DeepCollectionEquality().hash(picUrl) ^
      const DeepCollectionEquality().hash(phoneNumber) ^
      const DeepCollectionEquality().hash(role) ^
      const DeepCollectionEquality().hash(lastSignInDateTime);

  @override
  _$OrganikartUserCopyWith<_OrganikartUser> get copyWith =>
      __$OrganikartUserCopyWithImpl<_OrganikartUser>(this, _$identity);
}

abstract class _OrganikartUser implements OrganikartUser {
  const factory _OrganikartUser(
      {@required UniqueId uId,
      @required Name name,
      @required EmailAddress emailAddress,
      String picUrl,
      @required PhoneNumber phoneNumber,
      @required UserRole role,
      @required DateTime lastSignInDateTime}) = _$_OrganikartUser;

  @override
  UniqueId get uId;
  @override
  Name get name;
  @override
  EmailAddress get emailAddress;
  @override
  String get picUrl;
  @override
  PhoneNumber get phoneNumber;
  @override
  UserRole get role;
  @override
  DateTime get lastSignInDateTime;
  @override
  _$OrganikartUserCopyWith<_OrganikartUser> get copyWith;
}
