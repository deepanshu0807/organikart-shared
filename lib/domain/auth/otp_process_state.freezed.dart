// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'otp_process_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$OTPProcessStateTearOff {
  const _$OTPProcessStateTearOff();

// ignore: unused_element
  _Initial<T> initial<T>() {
    return _Initial<T>();
  }

// ignore: unused_element
  _PhoneNumberSubmiting<T> phoneNumberSubmiting<T>() {
    return _PhoneNumberSubmiting<T>();
  }

// ignore: unused_element
  _PhoneNumberSubmitSuccess<T> phoneNumberSubmitSuccess<T>() {
    return _PhoneNumberSubmitSuccess<T>();
  }

// ignore: unused_element
  _PhoneNumberSubmitFailed<T> phoneNumberSubmitFailed<T>(
      AuthFailure<dynamic> failure) {
    return _PhoneNumberSubmitFailed<T>(
      failure,
    );
  }

// ignore: unused_element
  _WaitingForOTP<T> waitingForOTP<T>() {
    return _WaitingForOTP<T>();
  }

// ignore: unused_element
  _WaitingForOTPExpired<T> waitingForOTPExpired<T>() {
    return _WaitingForOTPExpired<T>();
  }

// ignore: unused_element
  _SubmitingOTP<T> submitingOTP<T>() {
    return _SubmitingOTP<T>();
  }

// ignore: unused_element
  _SubmitOTPSuccess<T> submitOTPSuccess<T>() {
    return _SubmitOTPSuccess<T>();
  }

// ignore: unused_element
  _SubmitOTPFailed<T> submitOTPFailed<T>(AuthFailure<dynamic> failure) {
    return _SubmitOTPFailed<T>(
      failure,
    );
  }

// ignore: unused_element
  _OtpVerified<T> otpVerified<T>() {
    return _OtpVerified<T>();
  }

// ignore: unused_element
  _OtpVerificationFailed<T> otpVerificationFailed<T>(
      AuthFailure<dynamic> failure) {
    return _OtpVerificationFailed<T>(
      failure,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $OTPProcessState = _$OTPProcessStateTearOff();

/// @nodoc
mixin _$OTPProcessState<T> {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult phoneNumberSubmiting(),
    @required TResult phoneNumberSubmitSuccess(),
    @required TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    @required TResult waitingForOTP(),
    @required TResult waitingForOTPExpired(),
    @required TResult submitingOTP(),
    @required TResult submitOTPSuccess(),
    @required TResult submitOTPFailed(AuthFailure<dynamic> failure),
    @required TResult otpVerified(),
    @required TResult otpVerificationFailed(AuthFailure<dynamic> failure),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult phoneNumberSubmiting(),
    TResult phoneNumberSubmitSuccess(),
    TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    TResult waitingForOTP(),
    TResult waitingForOTPExpired(),
    TResult submitingOTP(),
    TResult submitOTPSuccess(),
    TResult submitOTPFailed(AuthFailure<dynamic> failure),
    TResult otpVerified(),
    TResult otpVerificationFailed(AuthFailure<dynamic> failure),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial<T> value),
    @required TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    @required
        TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    @required
        TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    @required TResult waitingForOTP(_WaitingForOTP<T> value),
    @required TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    @required TResult submitingOTP(_SubmitingOTP<T> value),
    @required TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    @required TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    @required TResult otpVerified(_OtpVerified<T> value),
    @required TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial<T> value),
    TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    TResult waitingForOTP(_WaitingForOTP<T> value),
    TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    TResult submitingOTP(_SubmitingOTP<T> value),
    TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    TResult otpVerified(_OtpVerified<T> value),
    TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $OTPProcessStateCopyWith<T, $Res> {
  factory $OTPProcessStateCopyWith(
          OTPProcessState<T> value, $Res Function(OTPProcessState<T>) then) =
      _$OTPProcessStateCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$OTPProcessStateCopyWithImpl<T, $Res>
    implements $OTPProcessStateCopyWith<T, $Res> {
  _$OTPProcessStateCopyWithImpl(this._value, this._then);

  final OTPProcessState<T> _value;
  // ignore: unused_field
  final $Res Function(OTPProcessState<T>) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<T, $Res> {
  factory _$InitialCopyWith(
          _Initial<T> value, $Res Function(_Initial<T>) then) =
      __$InitialCopyWithImpl<T, $Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<T, $Res>
    extends _$OTPProcessStateCopyWithImpl<T, $Res>
    implements _$InitialCopyWith<T, $Res> {
  __$InitialCopyWithImpl(_Initial<T> _value, $Res Function(_Initial<T>) _then)
      : super(_value, (v) => _then(v as _Initial<T>));

  @override
  _Initial<T> get _value => super._value as _Initial<T>;
}

/// @nodoc
class _$_Initial<T> implements _Initial<T> {
  const _$_Initial();

  @override
  String toString() {
    return 'OTPProcessState<$T>.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult phoneNumberSubmiting(),
    @required TResult phoneNumberSubmitSuccess(),
    @required TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    @required TResult waitingForOTP(),
    @required TResult waitingForOTPExpired(),
    @required TResult submitingOTP(),
    @required TResult submitOTPSuccess(),
    @required TResult submitOTPFailed(AuthFailure<dynamic> failure),
    @required TResult otpVerified(),
    @required TResult otpVerificationFailed(AuthFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult phoneNumberSubmiting(),
    TResult phoneNumberSubmitSuccess(),
    TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    TResult waitingForOTP(),
    TResult waitingForOTPExpired(),
    TResult submitingOTP(),
    TResult submitOTPSuccess(),
    TResult submitOTPFailed(AuthFailure<dynamic> failure),
    TResult otpVerified(),
    TResult otpVerificationFailed(AuthFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial<T> value),
    @required TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    @required
        TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    @required
        TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    @required TResult waitingForOTP(_WaitingForOTP<T> value),
    @required TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    @required TResult submitingOTP(_SubmitingOTP<T> value),
    @required TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    @required TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    @required TResult otpVerified(_OtpVerified<T> value),
    @required TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial<T> value),
    TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    TResult waitingForOTP(_WaitingForOTP<T> value),
    TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    TResult submitingOTP(_SubmitingOTP<T> value),
    TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    TResult otpVerified(_OtpVerified<T> value),
    TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial<T> implements OTPProcessState<T> {
  const factory _Initial() = _$_Initial<T>;
}

/// @nodoc
abstract class _$PhoneNumberSubmitingCopyWith<T, $Res> {
  factory _$PhoneNumberSubmitingCopyWith(_PhoneNumberSubmiting<T> value,
          $Res Function(_PhoneNumberSubmiting<T>) then) =
      __$PhoneNumberSubmitingCopyWithImpl<T, $Res>;
}

/// @nodoc
class __$PhoneNumberSubmitingCopyWithImpl<T, $Res>
    extends _$OTPProcessStateCopyWithImpl<T, $Res>
    implements _$PhoneNumberSubmitingCopyWith<T, $Res> {
  __$PhoneNumberSubmitingCopyWithImpl(_PhoneNumberSubmiting<T> _value,
      $Res Function(_PhoneNumberSubmiting<T>) _then)
      : super(_value, (v) => _then(v as _PhoneNumberSubmiting<T>));

  @override
  _PhoneNumberSubmiting<T> get _value =>
      super._value as _PhoneNumberSubmiting<T>;
}

/// @nodoc
class _$_PhoneNumberSubmiting<T> implements _PhoneNumberSubmiting<T> {
  const _$_PhoneNumberSubmiting();

  @override
  String toString() {
    return 'OTPProcessState<$T>.phoneNumberSubmiting()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _PhoneNumberSubmiting<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult phoneNumberSubmiting(),
    @required TResult phoneNumberSubmitSuccess(),
    @required TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    @required TResult waitingForOTP(),
    @required TResult waitingForOTPExpired(),
    @required TResult submitingOTP(),
    @required TResult submitOTPSuccess(),
    @required TResult submitOTPFailed(AuthFailure<dynamic> failure),
    @required TResult otpVerified(),
    @required TResult otpVerificationFailed(AuthFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return phoneNumberSubmiting();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult phoneNumberSubmiting(),
    TResult phoneNumberSubmitSuccess(),
    TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    TResult waitingForOTP(),
    TResult waitingForOTPExpired(),
    TResult submitingOTP(),
    TResult submitOTPSuccess(),
    TResult submitOTPFailed(AuthFailure<dynamic> failure),
    TResult otpVerified(),
    TResult otpVerificationFailed(AuthFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (phoneNumberSubmiting != null) {
      return phoneNumberSubmiting();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial<T> value),
    @required TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    @required
        TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    @required
        TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    @required TResult waitingForOTP(_WaitingForOTP<T> value),
    @required TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    @required TResult submitingOTP(_SubmitingOTP<T> value),
    @required TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    @required TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    @required TResult otpVerified(_OtpVerified<T> value),
    @required TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return phoneNumberSubmiting(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial<T> value),
    TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    TResult waitingForOTP(_WaitingForOTP<T> value),
    TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    TResult submitingOTP(_SubmitingOTP<T> value),
    TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    TResult otpVerified(_OtpVerified<T> value),
    TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (phoneNumberSubmiting != null) {
      return phoneNumberSubmiting(this);
    }
    return orElse();
  }
}

abstract class _PhoneNumberSubmiting<T> implements OTPProcessState<T> {
  const factory _PhoneNumberSubmiting() = _$_PhoneNumberSubmiting<T>;
}

/// @nodoc
abstract class _$PhoneNumberSubmitSuccessCopyWith<T, $Res> {
  factory _$PhoneNumberSubmitSuccessCopyWith(_PhoneNumberSubmitSuccess<T> value,
          $Res Function(_PhoneNumberSubmitSuccess<T>) then) =
      __$PhoneNumberSubmitSuccessCopyWithImpl<T, $Res>;
}

/// @nodoc
class __$PhoneNumberSubmitSuccessCopyWithImpl<T, $Res>
    extends _$OTPProcessStateCopyWithImpl<T, $Res>
    implements _$PhoneNumberSubmitSuccessCopyWith<T, $Res> {
  __$PhoneNumberSubmitSuccessCopyWithImpl(_PhoneNumberSubmitSuccess<T> _value,
      $Res Function(_PhoneNumberSubmitSuccess<T>) _then)
      : super(_value, (v) => _then(v as _PhoneNumberSubmitSuccess<T>));

  @override
  _PhoneNumberSubmitSuccess<T> get _value =>
      super._value as _PhoneNumberSubmitSuccess<T>;
}

/// @nodoc
class _$_PhoneNumberSubmitSuccess<T> implements _PhoneNumberSubmitSuccess<T> {
  const _$_PhoneNumberSubmitSuccess();

  @override
  String toString() {
    return 'OTPProcessState<$T>.phoneNumberSubmitSuccess()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _PhoneNumberSubmitSuccess<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult phoneNumberSubmiting(),
    @required TResult phoneNumberSubmitSuccess(),
    @required TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    @required TResult waitingForOTP(),
    @required TResult waitingForOTPExpired(),
    @required TResult submitingOTP(),
    @required TResult submitOTPSuccess(),
    @required TResult submitOTPFailed(AuthFailure<dynamic> failure),
    @required TResult otpVerified(),
    @required TResult otpVerificationFailed(AuthFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return phoneNumberSubmitSuccess();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult phoneNumberSubmiting(),
    TResult phoneNumberSubmitSuccess(),
    TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    TResult waitingForOTP(),
    TResult waitingForOTPExpired(),
    TResult submitingOTP(),
    TResult submitOTPSuccess(),
    TResult submitOTPFailed(AuthFailure<dynamic> failure),
    TResult otpVerified(),
    TResult otpVerificationFailed(AuthFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (phoneNumberSubmitSuccess != null) {
      return phoneNumberSubmitSuccess();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial<T> value),
    @required TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    @required
        TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    @required
        TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    @required TResult waitingForOTP(_WaitingForOTP<T> value),
    @required TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    @required TResult submitingOTP(_SubmitingOTP<T> value),
    @required TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    @required TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    @required TResult otpVerified(_OtpVerified<T> value),
    @required TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return phoneNumberSubmitSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial<T> value),
    TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    TResult waitingForOTP(_WaitingForOTP<T> value),
    TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    TResult submitingOTP(_SubmitingOTP<T> value),
    TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    TResult otpVerified(_OtpVerified<T> value),
    TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (phoneNumberSubmitSuccess != null) {
      return phoneNumberSubmitSuccess(this);
    }
    return orElse();
  }
}

abstract class _PhoneNumberSubmitSuccess<T> implements OTPProcessState<T> {
  const factory _PhoneNumberSubmitSuccess() = _$_PhoneNumberSubmitSuccess<T>;
}

/// @nodoc
abstract class _$PhoneNumberSubmitFailedCopyWith<T, $Res> {
  factory _$PhoneNumberSubmitFailedCopyWith(_PhoneNumberSubmitFailed<T> value,
          $Res Function(_PhoneNumberSubmitFailed<T>) then) =
      __$PhoneNumberSubmitFailedCopyWithImpl<T, $Res>;
  $Res call({AuthFailure<dynamic> failure});

  $AuthFailureCopyWith<dynamic, $Res> get failure;
}

/// @nodoc
class __$PhoneNumberSubmitFailedCopyWithImpl<T, $Res>
    extends _$OTPProcessStateCopyWithImpl<T, $Res>
    implements _$PhoneNumberSubmitFailedCopyWith<T, $Res> {
  __$PhoneNumberSubmitFailedCopyWithImpl(_PhoneNumberSubmitFailed<T> _value,
      $Res Function(_PhoneNumberSubmitFailed<T>) _then)
      : super(_value, (v) => _then(v as _PhoneNumberSubmitFailed<T>));

  @override
  _PhoneNumberSubmitFailed<T> get _value =>
      super._value as _PhoneNumberSubmitFailed<T>;

  @override
  $Res call({
    Object failure = freezed,
  }) {
    return _then(_PhoneNumberSubmitFailed<T>(
      failure == freezed ? _value.failure : failure as AuthFailure<dynamic>,
    ));
  }

  @override
  $AuthFailureCopyWith<dynamic, $Res> get failure {
    if (_value.failure == null) {
      return null;
    }
    return $AuthFailureCopyWith<dynamic, $Res>(_value.failure, (value) {
      return _then(_value.copyWith(failure: value));
    });
  }
}

/// @nodoc
class _$_PhoneNumberSubmitFailed<T> implements _PhoneNumberSubmitFailed<T> {
  const _$_PhoneNumberSubmitFailed(this.failure) : assert(failure != null);

  @override
  final AuthFailure<dynamic> failure;

  @override
  String toString() {
    return 'OTPProcessState<$T>.phoneNumberSubmitFailed(failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PhoneNumberSubmitFailed<T> &&
            (identical(other.failure, failure) ||
                const DeepCollectionEquality().equals(other.failure, failure)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(failure);

  @override
  _$PhoneNumberSubmitFailedCopyWith<T, _PhoneNumberSubmitFailed<T>>
      get copyWith => __$PhoneNumberSubmitFailedCopyWithImpl<T,
          _PhoneNumberSubmitFailed<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult phoneNumberSubmiting(),
    @required TResult phoneNumberSubmitSuccess(),
    @required TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    @required TResult waitingForOTP(),
    @required TResult waitingForOTPExpired(),
    @required TResult submitingOTP(),
    @required TResult submitOTPSuccess(),
    @required TResult submitOTPFailed(AuthFailure<dynamic> failure),
    @required TResult otpVerified(),
    @required TResult otpVerificationFailed(AuthFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return phoneNumberSubmitFailed(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult phoneNumberSubmiting(),
    TResult phoneNumberSubmitSuccess(),
    TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    TResult waitingForOTP(),
    TResult waitingForOTPExpired(),
    TResult submitingOTP(),
    TResult submitOTPSuccess(),
    TResult submitOTPFailed(AuthFailure<dynamic> failure),
    TResult otpVerified(),
    TResult otpVerificationFailed(AuthFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (phoneNumberSubmitFailed != null) {
      return phoneNumberSubmitFailed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial<T> value),
    @required TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    @required
        TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    @required
        TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    @required TResult waitingForOTP(_WaitingForOTP<T> value),
    @required TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    @required TResult submitingOTP(_SubmitingOTP<T> value),
    @required TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    @required TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    @required TResult otpVerified(_OtpVerified<T> value),
    @required TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return phoneNumberSubmitFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial<T> value),
    TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    TResult waitingForOTP(_WaitingForOTP<T> value),
    TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    TResult submitingOTP(_SubmitingOTP<T> value),
    TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    TResult otpVerified(_OtpVerified<T> value),
    TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (phoneNumberSubmitFailed != null) {
      return phoneNumberSubmitFailed(this);
    }
    return orElse();
  }
}

abstract class _PhoneNumberSubmitFailed<T> implements OTPProcessState<T> {
  const factory _PhoneNumberSubmitFailed(AuthFailure<dynamic> failure) =
      _$_PhoneNumberSubmitFailed<T>;

  AuthFailure<dynamic> get failure;
  _$PhoneNumberSubmitFailedCopyWith<T, _PhoneNumberSubmitFailed<T>>
      get copyWith;
}

/// @nodoc
abstract class _$WaitingForOTPCopyWith<T, $Res> {
  factory _$WaitingForOTPCopyWith(
          _WaitingForOTP<T> value, $Res Function(_WaitingForOTP<T>) then) =
      __$WaitingForOTPCopyWithImpl<T, $Res>;
}

/// @nodoc
class __$WaitingForOTPCopyWithImpl<T, $Res>
    extends _$OTPProcessStateCopyWithImpl<T, $Res>
    implements _$WaitingForOTPCopyWith<T, $Res> {
  __$WaitingForOTPCopyWithImpl(
      _WaitingForOTP<T> _value, $Res Function(_WaitingForOTP<T>) _then)
      : super(_value, (v) => _then(v as _WaitingForOTP<T>));

  @override
  _WaitingForOTP<T> get _value => super._value as _WaitingForOTP<T>;
}

/// @nodoc
class _$_WaitingForOTP<T> implements _WaitingForOTP<T> {
  const _$_WaitingForOTP();

  @override
  String toString() {
    return 'OTPProcessState<$T>.waitingForOTP()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _WaitingForOTP<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult phoneNumberSubmiting(),
    @required TResult phoneNumberSubmitSuccess(),
    @required TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    @required TResult waitingForOTP(),
    @required TResult waitingForOTPExpired(),
    @required TResult submitingOTP(),
    @required TResult submitOTPSuccess(),
    @required TResult submitOTPFailed(AuthFailure<dynamic> failure),
    @required TResult otpVerified(),
    @required TResult otpVerificationFailed(AuthFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return waitingForOTP();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult phoneNumberSubmiting(),
    TResult phoneNumberSubmitSuccess(),
    TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    TResult waitingForOTP(),
    TResult waitingForOTPExpired(),
    TResult submitingOTP(),
    TResult submitOTPSuccess(),
    TResult submitOTPFailed(AuthFailure<dynamic> failure),
    TResult otpVerified(),
    TResult otpVerificationFailed(AuthFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (waitingForOTP != null) {
      return waitingForOTP();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial<T> value),
    @required TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    @required
        TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    @required
        TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    @required TResult waitingForOTP(_WaitingForOTP<T> value),
    @required TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    @required TResult submitingOTP(_SubmitingOTP<T> value),
    @required TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    @required TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    @required TResult otpVerified(_OtpVerified<T> value),
    @required TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return waitingForOTP(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial<T> value),
    TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    TResult waitingForOTP(_WaitingForOTP<T> value),
    TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    TResult submitingOTP(_SubmitingOTP<T> value),
    TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    TResult otpVerified(_OtpVerified<T> value),
    TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (waitingForOTP != null) {
      return waitingForOTP(this);
    }
    return orElse();
  }
}

abstract class _WaitingForOTP<T> implements OTPProcessState<T> {
  const factory _WaitingForOTP() = _$_WaitingForOTP<T>;
}

/// @nodoc
abstract class _$WaitingForOTPExpiredCopyWith<T, $Res> {
  factory _$WaitingForOTPExpiredCopyWith(_WaitingForOTPExpired<T> value,
          $Res Function(_WaitingForOTPExpired<T>) then) =
      __$WaitingForOTPExpiredCopyWithImpl<T, $Res>;
}

/// @nodoc
class __$WaitingForOTPExpiredCopyWithImpl<T, $Res>
    extends _$OTPProcessStateCopyWithImpl<T, $Res>
    implements _$WaitingForOTPExpiredCopyWith<T, $Res> {
  __$WaitingForOTPExpiredCopyWithImpl(_WaitingForOTPExpired<T> _value,
      $Res Function(_WaitingForOTPExpired<T>) _then)
      : super(_value, (v) => _then(v as _WaitingForOTPExpired<T>));

  @override
  _WaitingForOTPExpired<T> get _value =>
      super._value as _WaitingForOTPExpired<T>;
}

/// @nodoc
class _$_WaitingForOTPExpired<T> implements _WaitingForOTPExpired<T> {
  const _$_WaitingForOTPExpired();

  @override
  String toString() {
    return 'OTPProcessState<$T>.waitingForOTPExpired()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _WaitingForOTPExpired<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult phoneNumberSubmiting(),
    @required TResult phoneNumberSubmitSuccess(),
    @required TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    @required TResult waitingForOTP(),
    @required TResult waitingForOTPExpired(),
    @required TResult submitingOTP(),
    @required TResult submitOTPSuccess(),
    @required TResult submitOTPFailed(AuthFailure<dynamic> failure),
    @required TResult otpVerified(),
    @required TResult otpVerificationFailed(AuthFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return waitingForOTPExpired();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult phoneNumberSubmiting(),
    TResult phoneNumberSubmitSuccess(),
    TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    TResult waitingForOTP(),
    TResult waitingForOTPExpired(),
    TResult submitingOTP(),
    TResult submitOTPSuccess(),
    TResult submitOTPFailed(AuthFailure<dynamic> failure),
    TResult otpVerified(),
    TResult otpVerificationFailed(AuthFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (waitingForOTPExpired != null) {
      return waitingForOTPExpired();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial<T> value),
    @required TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    @required
        TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    @required
        TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    @required TResult waitingForOTP(_WaitingForOTP<T> value),
    @required TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    @required TResult submitingOTP(_SubmitingOTP<T> value),
    @required TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    @required TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    @required TResult otpVerified(_OtpVerified<T> value),
    @required TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return waitingForOTPExpired(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial<T> value),
    TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    TResult waitingForOTP(_WaitingForOTP<T> value),
    TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    TResult submitingOTP(_SubmitingOTP<T> value),
    TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    TResult otpVerified(_OtpVerified<T> value),
    TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (waitingForOTPExpired != null) {
      return waitingForOTPExpired(this);
    }
    return orElse();
  }
}

abstract class _WaitingForOTPExpired<T> implements OTPProcessState<T> {
  const factory _WaitingForOTPExpired() = _$_WaitingForOTPExpired<T>;
}

/// @nodoc
abstract class _$SubmitingOTPCopyWith<T, $Res> {
  factory _$SubmitingOTPCopyWith(
          _SubmitingOTP<T> value, $Res Function(_SubmitingOTP<T>) then) =
      __$SubmitingOTPCopyWithImpl<T, $Res>;
}

/// @nodoc
class __$SubmitingOTPCopyWithImpl<T, $Res>
    extends _$OTPProcessStateCopyWithImpl<T, $Res>
    implements _$SubmitingOTPCopyWith<T, $Res> {
  __$SubmitingOTPCopyWithImpl(
      _SubmitingOTP<T> _value, $Res Function(_SubmitingOTP<T>) _then)
      : super(_value, (v) => _then(v as _SubmitingOTP<T>));

  @override
  _SubmitingOTP<T> get _value => super._value as _SubmitingOTP<T>;
}

/// @nodoc
class _$_SubmitingOTP<T> implements _SubmitingOTP<T> {
  const _$_SubmitingOTP();

  @override
  String toString() {
    return 'OTPProcessState<$T>.submitingOTP()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _SubmitingOTP<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult phoneNumberSubmiting(),
    @required TResult phoneNumberSubmitSuccess(),
    @required TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    @required TResult waitingForOTP(),
    @required TResult waitingForOTPExpired(),
    @required TResult submitingOTP(),
    @required TResult submitOTPSuccess(),
    @required TResult submitOTPFailed(AuthFailure<dynamic> failure),
    @required TResult otpVerified(),
    @required TResult otpVerificationFailed(AuthFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return submitingOTP();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult phoneNumberSubmiting(),
    TResult phoneNumberSubmitSuccess(),
    TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    TResult waitingForOTP(),
    TResult waitingForOTPExpired(),
    TResult submitingOTP(),
    TResult submitOTPSuccess(),
    TResult submitOTPFailed(AuthFailure<dynamic> failure),
    TResult otpVerified(),
    TResult otpVerificationFailed(AuthFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (submitingOTP != null) {
      return submitingOTP();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial<T> value),
    @required TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    @required
        TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    @required
        TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    @required TResult waitingForOTP(_WaitingForOTP<T> value),
    @required TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    @required TResult submitingOTP(_SubmitingOTP<T> value),
    @required TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    @required TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    @required TResult otpVerified(_OtpVerified<T> value),
    @required TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return submitingOTP(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial<T> value),
    TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    TResult waitingForOTP(_WaitingForOTP<T> value),
    TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    TResult submitingOTP(_SubmitingOTP<T> value),
    TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    TResult otpVerified(_OtpVerified<T> value),
    TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (submitingOTP != null) {
      return submitingOTP(this);
    }
    return orElse();
  }
}

abstract class _SubmitingOTP<T> implements OTPProcessState<T> {
  const factory _SubmitingOTP() = _$_SubmitingOTP<T>;
}

/// @nodoc
abstract class _$SubmitOTPSuccessCopyWith<T, $Res> {
  factory _$SubmitOTPSuccessCopyWith(_SubmitOTPSuccess<T> value,
          $Res Function(_SubmitOTPSuccess<T>) then) =
      __$SubmitOTPSuccessCopyWithImpl<T, $Res>;
}

/// @nodoc
class __$SubmitOTPSuccessCopyWithImpl<T, $Res>
    extends _$OTPProcessStateCopyWithImpl<T, $Res>
    implements _$SubmitOTPSuccessCopyWith<T, $Res> {
  __$SubmitOTPSuccessCopyWithImpl(
      _SubmitOTPSuccess<T> _value, $Res Function(_SubmitOTPSuccess<T>) _then)
      : super(_value, (v) => _then(v as _SubmitOTPSuccess<T>));

  @override
  _SubmitOTPSuccess<T> get _value => super._value as _SubmitOTPSuccess<T>;
}

/// @nodoc
class _$_SubmitOTPSuccess<T> implements _SubmitOTPSuccess<T> {
  const _$_SubmitOTPSuccess();

  @override
  String toString() {
    return 'OTPProcessState<$T>.submitOTPSuccess()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _SubmitOTPSuccess<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult phoneNumberSubmiting(),
    @required TResult phoneNumberSubmitSuccess(),
    @required TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    @required TResult waitingForOTP(),
    @required TResult waitingForOTPExpired(),
    @required TResult submitingOTP(),
    @required TResult submitOTPSuccess(),
    @required TResult submitOTPFailed(AuthFailure<dynamic> failure),
    @required TResult otpVerified(),
    @required TResult otpVerificationFailed(AuthFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return submitOTPSuccess();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult phoneNumberSubmiting(),
    TResult phoneNumberSubmitSuccess(),
    TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    TResult waitingForOTP(),
    TResult waitingForOTPExpired(),
    TResult submitingOTP(),
    TResult submitOTPSuccess(),
    TResult submitOTPFailed(AuthFailure<dynamic> failure),
    TResult otpVerified(),
    TResult otpVerificationFailed(AuthFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (submitOTPSuccess != null) {
      return submitOTPSuccess();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial<T> value),
    @required TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    @required
        TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    @required
        TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    @required TResult waitingForOTP(_WaitingForOTP<T> value),
    @required TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    @required TResult submitingOTP(_SubmitingOTP<T> value),
    @required TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    @required TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    @required TResult otpVerified(_OtpVerified<T> value),
    @required TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return submitOTPSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial<T> value),
    TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    TResult waitingForOTP(_WaitingForOTP<T> value),
    TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    TResult submitingOTP(_SubmitingOTP<T> value),
    TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    TResult otpVerified(_OtpVerified<T> value),
    TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (submitOTPSuccess != null) {
      return submitOTPSuccess(this);
    }
    return orElse();
  }
}

abstract class _SubmitOTPSuccess<T> implements OTPProcessState<T> {
  const factory _SubmitOTPSuccess() = _$_SubmitOTPSuccess<T>;
}

/// @nodoc
abstract class _$SubmitOTPFailedCopyWith<T, $Res> {
  factory _$SubmitOTPFailedCopyWith(
          _SubmitOTPFailed<T> value, $Res Function(_SubmitOTPFailed<T>) then) =
      __$SubmitOTPFailedCopyWithImpl<T, $Res>;
  $Res call({AuthFailure<dynamic> failure});

  $AuthFailureCopyWith<dynamic, $Res> get failure;
}

/// @nodoc
class __$SubmitOTPFailedCopyWithImpl<T, $Res>
    extends _$OTPProcessStateCopyWithImpl<T, $Res>
    implements _$SubmitOTPFailedCopyWith<T, $Res> {
  __$SubmitOTPFailedCopyWithImpl(
      _SubmitOTPFailed<T> _value, $Res Function(_SubmitOTPFailed<T>) _then)
      : super(_value, (v) => _then(v as _SubmitOTPFailed<T>));

  @override
  _SubmitOTPFailed<T> get _value => super._value as _SubmitOTPFailed<T>;

  @override
  $Res call({
    Object failure = freezed,
  }) {
    return _then(_SubmitOTPFailed<T>(
      failure == freezed ? _value.failure : failure as AuthFailure<dynamic>,
    ));
  }

  @override
  $AuthFailureCopyWith<dynamic, $Res> get failure {
    if (_value.failure == null) {
      return null;
    }
    return $AuthFailureCopyWith<dynamic, $Res>(_value.failure, (value) {
      return _then(_value.copyWith(failure: value));
    });
  }
}

/// @nodoc
class _$_SubmitOTPFailed<T> implements _SubmitOTPFailed<T> {
  const _$_SubmitOTPFailed(this.failure) : assert(failure != null);

  @override
  final AuthFailure<dynamic> failure;

  @override
  String toString() {
    return 'OTPProcessState<$T>.submitOTPFailed(failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SubmitOTPFailed<T> &&
            (identical(other.failure, failure) ||
                const DeepCollectionEquality().equals(other.failure, failure)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(failure);

  @override
  _$SubmitOTPFailedCopyWith<T, _SubmitOTPFailed<T>> get copyWith =>
      __$SubmitOTPFailedCopyWithImpl<T, _SubmitOTPFailed<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult phoneNumberSubmiting(),
    @required TResult phoneNumberSubmitSuccess(),
    @required TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    @required TResult waitingForOTP(),
    @required TResult waitingForOTPExpired(),
    @required TResult submitingOTP(),
    @required TResult submitOTPSuccess(),
    @required TResult submitOTPFailed(AuthFailure<dynamic> failure),
    @required TResult otpVerified(),
    @required TResult otpVerificationFailed(AuthFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return submitOTPFailed(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult phoneNumberSubmiting(),
    TResult phoneNumberSubmitSuccess(),
    TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    TResult waitingForOTP(),
    TResult waitingForOTPExpired(),
    TResult submitingOTP(),
    TResult submitOTPSuccess(),
    TResult submitOTPFailed(AuthFailure<dynamic> failure),
    TResult otpVerified(),
    TResult otpVerificationFailed(AuthFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (submitOTPFailed != null) {
      return submitOTPFailed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial<T> value),
    @required TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    @required
        TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    @required
        TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    @required TResult waitingForOTP(_WaitingForOTP<T> value),
    @required TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    @required TResult submitingOTP(_SubmitingOTP<T> value),
    @required TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    @required TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    @required TResult otpVerified(_OtpVerified<T> value),
    @required TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return submitOTPFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial<T> value),
    TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    TResult waitingForOTP(_WaitingForOTP<T> value),
    TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    TResult submitingOTP(_SubmitingOTP<T> value),
    TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    TResult otpVerified(_OtpVerified<T> value),
    TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (submitOTPFailed != null) {
      return submitOTPFailed(this);
    }
    return orElse();
  }
}

abstract class _SubmitOTPFailed<T> implements OTPProcessState<T> {
  const factory _SubmitOTPFailed(AuthFailure<dynamic> failure) =
      _$_SubmitOTPFailed<T>;

  AuthFailure<dynamic> get failure;
  _$SubmitOTPFailedCopyWith<T, _SubmitOTPFailed<T>> get copyWith;
}

/// @nodoc
abstract class _$OtpVerifiedCopyWith<T, $Res> {
  factory _$OtpVerifiedCopyWith(
          _OtpVerified<T> value, $Res Function(_OtpVerified<T>) then) =
      __$OtpVerifiedCopyWithImpl<T, $Res>;
}

/// @nodoc
class __$OtpVerifiedCopyWithImpl<T, $Res>
    extends _$OTPProcessStateCopyWithImpl<T, $Res>
    implements _$OtpVerifiedCopyWith<T, $Res> {
  __$OtpVerifiedCopyWithImpl(
      _OtpVerified<T> _value, $Res Function(_OtpVerified<T>) _then)
      : super(_value, (v) => _then(v as _OtpVerified<T>));

  @override
  _OtpVerified<T> get _value => super._value as _OtpVerified<T>;
}

/// @nodoc
class _$_OtpVerified<T> implements _OtpVerified<T> {
  const _$_OtpVerified();

  @override
  String toString() {
    return 'OTPProcessState<$T>.otpVerified()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _OtpVerified<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult phoneNumberSubmiting(),
    @required TResult phoneNumberSubmitSuccess(),
    @required TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    @required TResult waitingForOTP(),
    @required TResult waitingForOTPExpired(),
    @required TResult submitingOTP(),
    @required TResult submitOTPSuccess(),
    @required TResult submitOTPFailed(AuthFailure<dynamic> failure),
    @required TResult otpVerified(),
    @required TResult otpVerificationFailed(AuthFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return otpVerified();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult phoneNumberSubmiting(),
    TResult phoneNumberSubmitSuccess(),
    TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    TResult waitingForOTP(),
    TResult waitingForOTPExpired(),
    TResult submitingOTP(),
    TResult submitOTPSuccess(),
    TResult submitOTPFailed(AuthFailure<dynamic> failure),
    TResult otpVerified(),
    TResult otpVerificationFailed(AuthFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (otpVerified != null) {
      return otpVerified();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial<T> value),
    @required TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    @required
        TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    @required
        TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    @required TResult waitingForOTP(_WaitingForOTP<T> value),
    @required TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    @required TResult submitingOTP(_SubmitingOTP<T> value),
    @required TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    @required TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    @required TResult otpVerified(_OtpVerified<T> value),
    @required TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return otpVerified(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial<T> value),
    TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    TResult waitingForOTP(_WaitingForOTP<T> value),
    TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    TResult submitingOTP(_SubmitingOTP<T> value),
    TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    TResult otpVerified(_OtpVerified<T> value),
    TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (otpVerified != null) {
      return otpVerified(this);
    }
    return orElse();
  }
}

abstract class _OtpVerified<T> implements OTPProcessState<T> {
  const factory _OtpVerified() = _$_OtpVerified<T>;
}

/// @nodoc
abstract class _$OtpVerificationFailedCopyWith<T, $Res> {
  factory _$OtpVerificationFailedCopyWith(_OtpVerificationFailed<T> value,
          $Res Function(_OtpVerificationFailed<T>) then) =
      __$OtpVerificationFailedCopyWithImpl<T, $Res>;
  $Res call({AuthFailure<dynamic> failure});

  $AuthFailureCopyWith<dynamic, $Res> get failure;
}

/// @nodoc
class __$OtpVerificationFailedCopyWithImpl<T, $Res>
    extends _$OTPProcessStateCopyWithImpl<T, $Res>
    implements _$OtpVerificationFailedCopyWith<T, $Res> {
  __$OtpVerificationFailedCopyWithImpl(_OtpVerificationFailed<T> _value,
      $Res Function(_OtpVerificationFailed<T>) _then)
      : super(_value, (v) => _then(v as _OtpVerificationFailed<T>));

  @override
  _OtpVerificationFailed<T> get _value =>
      super._value as _OtpVerificationFailed<T>;

  @override
  $Res call({
    Object failure = freezed,
  }) {
    return _then(_OtpVerificationFailed<T>(
      failure == freezed ? _value.failure : failure as AuthFailure<dynamic>,
    ));
  }

  @override
  $AuthFailureCopyWith<dynamic, $Res> get failure {
    if (_value.failure == null) {
      return null;
    }
    return $AuthFailureCopyWith<dynamic, $Res>(_value.failure, (value) {
      return _then(_value.copyWith(failure: value));
    });
  }
}

/// @nodoc
class _$_OtpVerificationFailed<T> implements _OtpVerificationFailed<T> {
  const _$_OtpVerificationFailed(this.failure) : assert(failure != null);

  @override
  final AuthFailure<dynamic> failure;

  @override
  String toString() {
    return 'OTPProcessState<$T>.otpVerificationFailed(failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _OtpVerificationFailed<T> &&
            (identical(other.failure, failure) ||
                const DeepCollectionEquality().equals(other.failure, failure)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(failure);

  @override
  _$OtpVerificationFailedCopyWith<T, _OtpVerificationFailed<T>> get copyWith =>
      __$OtpVerificationFailedCopyWithImpl<T, _OtpVerificationFailed<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult phoneNumberSubmiting(),
    @required TResult phoneNumberSubmitSuccess(),
    @required TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    @required TResult waitingForOTP(),
    @required TResult waitingForOTPExpired(),
    @required TResult submitingOTP(),
    @required TResult submitOTPSuccess(),
    @required TResult submitOTPFailed(AuthFailure<dynamic> failure),
    @required TResult otpVerified(),
    @required TResult otpVerificationFailed(AuthFailure<dynamic> failure),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return otpVerificationFailed(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult phoneNumberSubmiting(),
    TResult phoneNumberSubmitSuccess(),
    TResult phoneNumberSubmitFailed(AuthFailure<dynamic> failure),
    TResult waitingForOTP(),
    TResult waitingForOTPExpired(),
    TResult submitingOTP(),
    TResult submitOTPSuccess(),
    TResult submitOTPFailed(AuthFailure<dynamic> failure),
    TResult otpVerified(),
    TResult otpVerificationFailed(AuthFailure<dynamic> failure),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (otpVerificationFailed != null) {
      return otpVerificationFailed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial<T> value),
    @required TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    @required
        TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    @required
        TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    @required TResult waitingForOTP(_WaitingForOTP<T> value),
    @required TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    @required TResult submitingOTP(_SubmitingOTP<T> value),
    @required TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    @required TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    @required TResult otpVerified(_OtpVerified<T> value),
    @required TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
  }) {
    assert(initial != null);
    assert(phoneNumberSubmiting != null);
    assert(phoneNumberSubmitSuccess != null);
    assert(phoneNumberSubmitFailed != null);
    assert(waitingForOTP != null);
    assert(waitingForOTPExpired != null);
    assert(submitingOTP != null);
    assert(submitOTPSuccess != null);
    assert(submitOTPFailed != null);
    assert(otpVerified != null);
    assert(otpVerificationFailed != null);
    return otpVerificationFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial<T> value),
    TResult phoneNumberSubmiting(_PhoneNumberSubmiting<T> value),
    TResult phoneNumberSubmitSuccess(_PhoneNumberSubmitSuccess<T> value),
    TResult phoneNumberSubmitFailed(_PhoneNumberSubmitFailed<T> value),
    TResult waitingForOTP(_WaitingForOTP<T> value),
    TResult waitingForOTPExpired(_WaitingForOTPExpired<T> value),
    TResult submitingOTP(_SubmitingOTP<T> value),
    TResult submitOTPSuccess(_SubmitOTPSuccess<T> value),
    TResult submitOTPFailed(_SubmitOTPFailed<T> value),
    TResult otpVerified(_OtpVerified<T> value),
    TResult otpVerificationFailed(_OtpVerificationFailed<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (otpVerificationFailed != null) {
      return otpVerificationFailed(this);
    }
    return orElse();
  }
}

abstract class _OtpVerificationFailed<T> implements OTPProcessState<T> {
  const factory _OtpVerificationFailed(AuthFailure<dynamic> failure) =
      _$_OtpVerificationFailed<T>;

  AuthFailure<dynamic> get failure;
  _$OtpVerificationFailedCopyWith<T, _OtpVerificationFailed<T>> get copyWith;
}
