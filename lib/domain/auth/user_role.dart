import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_role.freezed.dart';

@freezed
abstract class UserRole with _$UserRole {
  const factory UserRole.superAdmin() = SuperAdmin;
  const factory UserRole.customer() = Customer;
}

extension UserRoleSX on UserRole {
  String toValueString() {
    return map(
      superAdmin: (_) => "SUPER_ADMIN",
      customer: (_) => "CUSTOMER",
    );
  }
}

extension UserRoleX on String {
  UserRole toUserRole() {
    switch (this) {
      case "SUPER_ADMIN":
        return const UserRole.superAdmin();
        break;
      case "CUSTOMER":
        return const UserRole.customer();
        break;
      default:
        return const UserRole.superAdmin();
    }
  }
}
