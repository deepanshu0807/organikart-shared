import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';

import 'auth_failures.dart';
import 'user.dart';
import '../core/value_objects.dart';

abstract class IAuth {
  Stream<Future<Option<OrganikartUser>>> getSignedInUser();

  Future<Either<AuthFailure, Unit>> updateUserName({
    @required Name name,
  });

  Future<Either<AuthFailure, Unit>> loginEmailAndPassword({
    @required EmailAddress email,
    @required Password password,
  });

  Future<Either<AuthFailure, OrganikartUser>> verifyOTP({
    @required String verId,
    @required String otp,
  });

  Future<Either<AuthFailure, Unit>> loginOnlyAdminsUsingEmailAndPassword({
    @required EmailAddress email,
    @required Password password,
  });

  Future<Either<AuthFailure, Unit>> forgotPassword({
    @required EmailAddress email,
  });

  Future<Either<AuthFailure, Unit>> facebookSignIn();

  Future<Either<AuthFailure, Unit>> registerUser({
    @required Name name,
    @required EmailAddress email,
    @required Password password,
  });

  Future<void> signOut();
}
