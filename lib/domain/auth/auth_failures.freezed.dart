// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'auth_failures.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$AuthFailureTearOff {
  const _$AuthFailureTearOff();

// ignore: unused_element
  FCanceledByUser<T> canceledByUser<T>() {
    return FCanceledByUser<T>();
  }

// ignore: unused_element
  FServerError<T> serverError<T>() {
    return FServerError<T>();
  }

// ignore: unused_element
  FNotAllowed<T> notAllowed<T>() {
    return FNotAllowed<T>();
  }

// ignore: unused_element
  _FAccountExistWithDifferentCredential<T>
      accountExistWithDifferentCredential<T>() {
    return _FAccountExistWithDifferentCredential<T>();
  }

// ignore: unused_element
  _InvalidCredential<T> invalidCredential<T>() {
    return _InvalidCredential<T>();
  }

// ignore: unused_element
  FInvalidEmailPasswordCombination<T> invalidEmailPasswordCombination<T>() {
    return FInvalidEmailPasswordCombination<T>();
  }

// ignore: unused_element
  _InvalidOTP<T> invalidOTP<T>() {
    return _InvalidOTP<T>();
  }

// ignore: unused_element
  _InvalidOTPVerId<T> invalidOTPVerId<T>() {
    return _InvalidOTPVerId<T>();
  }

// ignore: unused_element
  _InvalidPhone<T> invalidPhone<T>() {
    return _InvalidPhone<T>();
  }

// ignore: unused_element
  FNotAnAdmin<T> notAnAdmin<T>() {
    return FNotAnAdmin<T>();
  }

// ignore: unused_element
  FUserNotFound<T> userNotFound<T>() {
    return FUserNotFound<T>();
  }

// ignore: unused_element
  FInvalidEmail<T> invalidEmail<T>() {
    return FInvalidEmail<T>();
  }

// ignore: unused_element
  FInvalidEmailOrPasswordValue<T> invalidEmailOrPasswordValue<T>() {
    return FInvalidEmailOrPasswordValue<T>();
  }

// ignore: unused_element
  FemailAlreadyExist<T> emailAlreadyExist<T>() {
    return FemailAlreadyExist<T>();
  }

// ignore: unused_element
  FphoneAlreadyExist<T> phoneAlreadyExist<T>() {
    return FphoneAlreadyExist<T>();
  }

// ignore: unused_element
  FNotACartManager<T> notACartManager<T>() {
    return FNotACartManager<T>();
  }
}

/// @nodoc
// ignore: unused_element
const $AuthFailure = _$AuthFailureTearOff();

/// @nodoc
mixin _$AuthFailure<T> {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $AuthFailureCopyWith<T, $Res> {
  factory $AuthFailureCopyWith(
          AuthFailure<T> value, $Res Function(AuthFailure<T>) then) =
      _$AuthFailureCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$AuthFailureCopyWithImpl<T, $Res>
    implements $AuthFailureCopyWith<T, $Res> {
  _$AuthFailureCopyWithImpl(this._value, this._then);

  final AuthFailure<T> _value;
  // ignore: unused_field
  final $Res Function(AuthFailure<T>) _then;
}

/// @nodoc
abstract class $FCanceledByUserCopyWith<T, $Res> {
  factory $FCanceledByUserCopyWith(
          FCanceledByUser<T> value, $Res Function(FCanceledByUser<T>) then) =
      _$FCanceledByUserCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$FCanceledByUserCopyWithImpl<T, $Res>
    extends _$AuthFailureCopyWithImpl<T, $Res>
    implements $FCanceledByUserCopyWith<T, $Res> {
  _$FCanceledByUserCopyWithImpl(
      FCanceledByUser<T> _value, $Res Function(FCanceledByUser<T>) _then)
      : super(_value, (v) => _then(v as FCanceledByUser<T>));

  @override
  FCanceledByUser<T> get _value => super._value as FCanceledByUser<T>;
}

/// @nodoc
class _$FCanceledByUser<T> implements FCanceledByUser<T> {
  const _$FCanceledByUser();

  @override
  String toString() {
    return 'AuthFailure<$T>.canceledByUser()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is FCanceledByUser<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return canceledByUser();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (canceledByUser != null) {
      return canceledByUser();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return canceledByUser(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (canceledByUser != null) {
      return canceledByUser(this);
    }
    return orElse();
  }
}

abstract class FCanceledByUser<T> implements AuthFailure<T> {
  const factory FCanceledByUser() = _$FCanceledByUser<T>;
}

/// @nodoc
abstract class $FServerErrorCopyWith<T, $Res> {
  factory $FServerErrorCopyWith(
          FServerError<T> value, $Res Function(FServerError<T>) then) =
      _$FServerErrorCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$FServerErrorCopyWithImpl<T, $Res>
    extends _$AuthFailureCopyWithImpl<T, $Res>
    implements $FServerErrorCopyWith<T, $Res> {
  _$FServerErrorCopyWithImpl(
      FServerError<T> _value, $Res Function(FServerError<T>) _then)
      : super(_value, (v) => _then(v as FServerError<T>));

  @override
  FServerError<T> get _value => super._value as FServerError<T>;
}

/// @nodoc
class _$FServerError<T> implements FServerError<T> {
  const _$FServerError();

  @override
  String toString() {
    return 'AuthFailure<$T>.serverError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is FServerError<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return serverError();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (serverError != null) {
      return serverError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return serverError(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (serverError != null) {
      return serverError(this);
    }
    return orElse();
  }
}

abstract class FServerError<T> implements AuthFailure<T> {
  const factory FServerError() = _$FServerError<T>;
}

/// @nodoc
abstract class $FNotAllowedCopyWith<T, $Res> {
  factory $FNotAllowedCopyWith(
          FNotAllowed<T> value, $Res Function(FNotAllowed<T>) then) =
      _$FNotAllowedCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$FNotAllowedCopyWithImpl<T, $Res>
    extends _$AuthFailureCopyWithImpl<T, $Res>
    implements $FNotAllowedCopyWith<T, $Res> {
  _$FNotAllowedCopyWithImpl(
      FNotAllowed<T> _value, $Res Function(FNotAllowed<T>) _then)
      : super(_value, (v) => _then(v as FNotAllowed<T>));

  @override
  FNotAllowed<T> get _value => super._value as FNotAllowed<T>;
}

/// @nodoc
class _$FNotAllowed<T> implements FNotAllowed<T> {
  const _$FNotAllowed();

  @override
  String toString() {
    return 'AuthFailure<$T>.notAllowed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is FNotAllowed<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return notAllowed();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (notAllowed != null) {
      return notAllowed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return notAllowed(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (notAllowed != null) {
      return notAllowed(this);
    }
    return orElse();
  }
}

abstract class FNotAllowed<T> implements AuthFailure<T> {
  const factory FNotAllowed() = _$FNotAllowed<T>;
}

/// @nodoc
abstract class _$FAccountExistWithDifferentCredentialCopyWith<T, $Res> {
  factory _$FAccountExistWithDifferentCredentialCopyWith(
          _FAccountExistWithDifferentCredential<T> value,
          $Res Function(_FAccountExistWithDifferentCredential<T>) then) =
      __$FAccountExistWithDifferentCredentialCopyWithImpl<T, $Res>;
}

/// @nodoc
class __$FAccountExistWithDifferentCredentialCopyWithImpl<T, $Res>
    extends _$AuthFailureCopyWithImpl<T, $Res>
    implements _$FAccountExistWithDifferentCredentialCopyWith<T, $Res> {
  __$FAccountExistWithDifferentCredentialCopyWithImpl(
      _FAccountExistWithDifferentCredential<T> _value,
      $Res Function(_FAccountExistWithDifferentCredential<T>) _then)
      : super(_value,
            (v) => _then(v as _FAccountExistWithDifferentCredential<T>));

  @override
  _FAccountExistWithDifferentCredential<T> get _value =>
      super._value as _FAccountExistWithDifferentCredential<T>;
}

/// @nodoc
class _$_FAccountExistWithDifferentCredential<T>
    implements _FAccountExistWithDifferentCredential<T> {
  const _$_FAccountExistWithDifferentCredential();

  @override
  String toString() {
    return 'AuthFailure<$T>.accountExistWithDifferentCredential()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _FAccountExistWithDifferentCredential<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return accountExistWithDifferentCredential();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (accountExistWithDifferentCredential != null) {
      return accountExistWithDifferentCredential();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return accountExistWithDifferentCredential(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (accountExistWithDifferentCredential != null) {
      return accountExistWithDifferentCredential(this);
    }
    return orElse();
  }
}

abstract class _FAccountExistWithDifferentCredential<T>
    implements AuthFailure<T> {
  const factory _FAccountExistWithDifferentCredential() =
      _$_FAccountExistWithDifferentCredential<T>;
}

/// @nodoc
abstract class _$InvalidCredentialCopyWith<T, $Res> {
  factory _$InvalidCredentialCopyWith(_InvalidCredential<T> value,
          $Res Function(_InvalidCredential<T>) then) =
      __$InvalidCredentialCopyWithImpl<T, $Res>;
}

/// @nodoc
class __$InvalidCredentialCopyWithImpl<T, $Res>
    extends _$AuthFailureCopyWithImpl<T, $Res>
    implements _$InvalidCredentialCopyWith<T, $Res> {
  __$InvalidCredentialCopyWithImpl(
      _InvalidCredential<T> _value, $Res Function(_InvalidCredential<T>) _then)
      : super(_value, (v) => _then(v as _InvalidCredential<T>));

  @override
  _InvalidCredential<T> get _value => super._value as _InvalidCredential<T>;
}

/// @nodoc
class _$_InvalidCredential<T> implements _InvalidCredential<T> {
  const _$_InvalidCredential();

  @override
  String toString() {
    return 'AuthFailure<$T>.invalidCredential()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _InvalidCredential<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return invalidCredential();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (invalidCredential != null) {
      return invalidCredential();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return invalidCredential(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (invalidCredential != null) {
      return invalidCredential(this);
    }
    return orElse();
  }
}

abstract class _InvalidCredential<T> implements AuthFailure<T> {
  const factory _InvalidCredential() = _$_InvalidCredential<T>;
}

/// @nodoc
abstract class $FInvalidEmailPasswordCombinationCopyWith<T, $Res> {
  factory $FInvalidEmailPasswordCombinationCopyWith(
          FInvalidEmailPasswordCombination<T> value,
          $Res Function(FInvalidEmailPasswordCombination<T>) then) =
      _$FInvalidEmailPasswordCombinationCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$FInvalidEmailPasswordCombinationCopyWithImpl<T, $Res>
    extends _$AuthFailureCopyWithImpl<T, $Res>
    implements $FInvalidEmailPasswordCombinationCopyWith<T, $Res> {
  _$FInvalidEmailPasswordCombinationCopyWithImpl(
      FInvalidEmailPasswordCombination<T> _value,
      $Res Function(FInvalidEmailPasswordCombination<T>) _then)
      : super(_value, (v) => _then(v as FInvalidEmailPasswordCombination<T>));

  @override
  FInvalidEmailPasswordCombination<T> get _value =>
      super._value as FInvalidEmailPasswordCombination<T>;
}

/// @nodoc
class _$FInvalidEmailPasswordCombination<T>
    implements FInvalidEmailPasswordCombination<T> {
  const _$FInvalidEmailPasswordCombination();

  @override
  String toString() {
    return 'AuthFailure<$T>.invalidEmailPasswordCombination()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is FInvalidEmailPasswordCombination<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return invalidEmailPasswordCombination();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (invalidEmailPasswordCombination != null) {
      return invalidEmailPasswordCombination();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return invalidEmailPasswordCombination(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (invalidEmailPasswordCombination != null) {
      return invalidEmailPasswordCombination(this);
    }
    return orElse();
  }
}

abstract class FInvalidEmailPasswordCombination<T> implements AuthFailure<T> {
  const factory FInvalidEmailPasswordCombination() =
      _$FInvalidEmailPasswordCombination<T>;
}

/// @nodoc
abstract class _$InvalidOTPCopyWith<T, $Res> {
  factory _$InvalidOTPCopyWith(
          _InvalidOTP<T> value, $Res Function(_InvalidOTP<T>) then) =
      __$InvalidOTPCopyWithImpl<T, $Res>;
}

/// @nodoc
class __$InvalidOTPCopyWithImpl<T, $Res>
    extends _$AuthFailureCopyWithImpl<T, $Res>
    implements _$InvalidOTPCopyWith<T, $Res> {
  __$InvalidOTPCopyWithImpl(
      _InvalidOTP<T> _value, $Res Function(_InvalidOTP<T>) _then)
      : super(_value, (v) => _then(v as _InvalidOTP<T>));

  @override
  _InvalidOTP<T> get _value => super._value as _InvalidOTP<T>;
}

/// @nodoc
class _$_InvalidOTP<T> implements _InvalidOTP<T> {
  const _$_InvalidOTP();

  @override
  String toString() {
    return 'AuthFailure<$T>.invalidOTP()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _InvalidOTP<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return invalidOTP();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (invalidOTP != null) {
      return invalidOTP();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return invalidOTP(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (invalidOTP != null) {
      return invalidOTP(this);
    }
    return orElse();
  }
}

abstract class _InvalidOTP<T> implements AuthFailure<T> {
  const factory _InvalidOTP() = _$_InvalidOTP<T>;
}

/// @nodoc
abstract class _$InvalidOTPVerIdCopyWith<T, $Res> {
  factory _$InvalidOTPVerIdCopyWith(
          _InvalidOTPVerId<T> value, $Res Function(_InvalidOTPVerId<T>) then) =
      __$InvalidOTPVerIdCopyWithImpl<T, $Res>;
}

/// @nodoc
class __$InvalidOTPVerIdCopyWithImpl<T, $Res>
    extends _$AuthFailureCopyWithImpl<T, $Res>
    implements _$InvalidOTPVerIdCopyWith<T, $Res> {
  __$InvalidOTPVerIdCopyWithImpl(
      _InvalidOTPVerId<T> _value, $Res Function(_InvalidOTPVerId<T>) _then)
      : super(_value, (v) => _then(v as _InvalidOTPVerId<T>));

  @override
  _InvalidOTPVerId<T> get _value => super._value as _InvalidOTPVerId<T>;
}

/// @nodoc
class _$_InvalidOTPVerId<T> implements _InvalidOTPVerId<T> {
  const _$_InvalidOTPVerId();

  @override
  String toString() {
    return 'AuthFailure<$T>.invalidOTPVerId()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _InvalidOTPVerId<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return invalidOTPVerId();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (invalidOTPVerId != null) {
      return invalidOTPVerId();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return invalidOTPVerId(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (invalidOTPVerId != null) {
      return invalidOTPVerId(this);
    }
    return orElse();
  }
}

abstract class _InvalidOTPVerId<T> implements AuthFailure<T> {
  const factory _InvalidOTPVerId() = _$_InvalidOTPVerId<T>;
}

/// @nodoc
abstract class _$InvalidPhoneCopyWith<T, $Res> {
  factory _$InvalidPhoneCopyWith(
          _InvalidPhone<T> value, $Res Function(_InvalidPhone<T>) then) =
      __$InvalidPhoneCopyWithImpl<T, $Res>;
}

/// @nodoc
class __$InvalidPhoneCopyWithImpl<T, $Res>
    extends _$AuthFailureCopyWithImpl<T, $Res>
    implements _$InvalidPhoneCopyWith<T, $Res> {
  __$InvalidPhoneCopyWithImpl(
      _InvalidPhone<T> _value, $Res Function(_InvalidPhone<T>) _then)
      : super(_value, (v) => _then(v as _InvalidPhone<T>));

  @override
  _InvalidPhone<T> get _value => super._value as _InvalidPhone<T>;
}

/// @nodoc
class _$_InvalidPhone<T> implements _InvalidPhone<T> {
  const _$_InvalidPhone();

  @override
  String toString() {
    return 'AuthFailure<$T>.invalidPhone()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _InvalidPhone<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return invalidPhone();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (invalidPhone != null) {
      return invalidPhone();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return invalidPhone(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (invalidPhone != null) {
      return invalidPhone(this);
    }
    return orElse();
  }
}

abstract class _InvalidPhone<T> implements AuthFailure<T> {
  const factory _InvalidPhone() = _$_InvalidPhone<T>;
}

/// @nodoc
abstract class $FNotAnAdminCopyWith<T, $Res> {
  factory $FNotAnAdminCopyWith(
          FNotAnAdmin<T> value, $Res Function(FNotAnAdmin<T>) then) =
      _$FNotAnAdminCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$FNotAnAdminCopyWithImpl<T, $Res>
    extends _$AuthFailureCopyWithImpl<T, $Res>
    implements $FNotAnAdminCopyWith<T, $Res> {
  _$FNotAnAdminCopyWithImpl(
      FNotAnAdmin<T> _value, $Res Function(FNotAnAdmin<T>) _then)
      : super(_value, (v) => _then(v as FNotAnAdmin<T>));

  @override
  FNotAnAdmin<T> get _value => super._value as FNotAnAdmin<T>;
}

/// @nodoc
class _$FNotAnAdmin<T> implements FNotAnAdmin<T> {
  const _$FNotAnAdmin();

  @override
  String toString() {
    return 'AuthFailure<$T>.notAnAdmin()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is FNotAnAdmin<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return notAnAdmin();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (notAnAdmin != null) {
      return notAnAdmin();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return notAnAdmin(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (notAnAdmin != null) {
      return notAnAdmin(this);
    }
    return orElse();
  }
}

abstract class FNotAnAdmin<T> implements AuthFailure<T> {
  const factory FNotAnAdmin() = _$FNotAnAdmin<T>;
}

/// @nodoc
abstract class $FUserNotFoundCopyWith<T, $Res> {
  factory $FUserNotFoundCopyWith(
          FUserNotFound<T> value, $Res Function(FUserNotFound<T>) then) =
      _$FUserNotFoundCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$FUserNotFoundCopyWithImpl<T, $Res>
    extends _$AuthFailureCopyWithImpl<T, $Res>
    implements $FUserNotFoundCopyWith<T, $Res> {
  _$FUserNotFoundCopyWithImpl(
      FUserNotFound<T> _value, $Res Function(FUserNotFound<T>) _then)
      : super(_value, (v) => _then(v as FUserNotFound<T>));

  @override
  FUserNotFound<T> get _value => super._value as FUserNotFound<T>;
}

/// @nodoc
class _$FUserNotFound<T> implements FUserNotFound<T> {
  const _$FUserNotFound();

  @override
  String toString() {
    return 'AuthFailure<$T>.userNotFound()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is FUserNotFound<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return userNotFound();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (userNotFound != null) {
      return userNotFound();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return userNotFound(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (userNotFound != null) {
      return userNotFound(this);
    }
    return orElse();
  }
}

abstract class FUserNotFound<T> implements AuthFailure<T> {
  const factory FUserNotFound() = _$FUserNotFound<T>;
}

/// @nodoc
abstract class $FInvalidEmailCopyWith<T, $Res> {
  factory $FInvalidEmailCopyWith(
          FInvalidEmail<T> value, $Res Function(FInvalidEmail<T>) then) =
      _$FInvalidEmailCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$FInvalidEmailCopyWithImpl<T, $Res>
    extends _$AuthFailureCopyWithImpl<T, $Res>
    implements $FInvalidEmailCopyWith<T, $Res> {
  _$FInvalidEmailCopyWithImpl(
      FInvalidEmail<T> _value, $Res Function(FInvalidEmail<T>) _then)
      : super(_value, (v) => _then(v as FInvalidEmail<T>));

  @override
  FInvalidEmail<T> get _value => super._value as FInvalidEmail<T>;
}

/// @nodoc
class _$FInvalidEmail<T> implements FInvalidEmail<T> {
  const _$FInvalidEmail();

  @override
  String toString() {
    return 'AuthFailure<$T>.invalidEmail()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is FInvalidEmail<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return invalidEmail();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (invalidEmail != null) {
      return invalidEmail();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return invalidEmail(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (invalidEmail != null) {
      return invalidEmail(this);
    }
    return orElse();
  }
}

abstract class FInvalidEmail<T> implements AuthFailure<T> {
  const factory FInvalidEmail() = _$FInvalidEmail<T>;
}

/// @nodoc
abstract class $FInvalidEmailOrPasswordValueCopyWith<T, $Res> {
  factory $FInvalidEmailOrPasswordValueCopyWith(
          FInvalidEmailOrPasswordValue<T> value,
          $Res Function(FInvalidEmailOrPasswordValue<T>) then) =
      _$FInvalidEmailOrPasswordValueCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$FInvalidEmailOrPasswordValueCopyWithImpl<T, $Res>
    extends _$AuthFailureCopyWithImpl<T, $Res>
    implements $FInvalidEmailOrPasswordValueCopyWith<T, $Res> {
  _$FInvalidEmailOrPasswordValueCopyWithImpl(
      FInvalidEmailOrPasswordValue<T> _value,
      $Res Function(FInvalidEmailOrPasswordValue<T>) _then)
      : super(_value, (v) => _then(v as FInvalidEmailOrPasswordValue<T>));

  @override
  FInvalidEmailOrPasswordValue<T> get _value =>
      super._value as FInvalidEmailOrPasswordValue<T>;
}

/// @nodoc
class _$FInvalidEmailOrPasswordValue<T>
    implements FInvalidEmailOrPasswordValue<T> {
  const _$FInvalidEmailOrPasswordValue();

  @override
  String toString() {
    return 'AuthFailure<$T>.invalidEmailOrPasswordValue()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is FInvalidEmailOrPasswordValue<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return invalidEmailOrPasswordValue();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (invalidEmailOrPasswordValue != null) {
      return invalidEmailOrPasswordValue();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return invalidEmailOrPasswordValue(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (invalidEmailOrPasswordValue != null) {
      return invalidEmailOrPasswordValue(this);
    }
    return orElse();
  }
}

abstract class FInvalidEmailOrPasswordValue<T> implements AuthFailure<T> {
  const factory FInvalidEmailOrPasswordValue() =
      _$FInvalidEmailOrPasswordValue<T>;
}

/// @nodoc
abstract class $FemailAlreadyExistCopyWith<T, $Res> {
  factory $FemailAlreadyExistCopyWith(FemailAlreadyExist<T> value,
          $Res Function(FemailAlreadyExist<T>) then) =
      _$FemailAlreadyExistCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$FemailAlreadyExistCopyWithImpl<T, $Res>
    extends _$AuthFailureCopyWithImpl<T, $Res>
    implements $FemailAlreadyExistCopyWith<T, $Res> {
  _$FemailAlreadyExistCopyWithImpl(
      FemailAlreadyExist<T> _value, $Res Function(FemailAlreadyExist<T>) _then)
      : super(_value, (v) => _then(v as FemailAlreadyExist<T>));

  @override
  FemailAlreadyExist<T> get _value => super._value as FemailAlreadyExist<T>;
}

/// @nodoc
class _$FemailAlreadyExist<T> implements FemailAlreadyExist<T> {
  const _$FemailAlreadyExist();

  @override
  String toString() {
    return 'AuthFailure<$T>.emailAlreadyExist()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is FemailAlreadyExist<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return emailAlreadyExist();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (emailAlreadyExist != null) {
      return emailAlreadyExist();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return emailAlreadyExist(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (emailAlreadyExist != null) {
      return emailAlreadyExist(this);
    }
    return orElse();
  }
}

abstract class FemailAlreadyExist<T> implements AuthFailure<T> {
  const factory FemailAlreadyExist() = _$FemailAlreadyExist<T>;
}

/// @nodoc
abstract class $FphoneAlreadyExistCopyWith<T, $Res> {
  factory $FphoneAlreadyExistCopyWith(FphoneAlreadyExist<T> value,
          $Res Function(FphoneAlreadyExist<T>) then) =
      _$FphoneAlreadyExistCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$FphoneAlreadyExistCopyWithImpl<T, $Res>
    extends _$AuthFailureCopyWithImpl<T, $Res>
    implements $FphoneAlreadyExistCopyWith<T, $Res> {
  _$FphoneAlreadyExistCopyWithImpl(
      FphoneAlreadyExist<T> _value, $Res Function(FphoneAlreadyExist<T>) _then)
      : super(_value, (v) => _then(v as FphoneAlreadyExist<T>));

  @override
  FphoneAlreadyExist<T> get _value => super._value as FphoneAlreadyExist<T>;
}

/// @nodoc
class _$FphoneAlreadyExist<T> implements FphoneAlreadyExist<T> {
  const _$FphoneAlreadyExist();

  @override
  String toString() {
    return 'AuthFailure<$T>.phoneAlreadyExist()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is FphoneAlreadyExist<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return phoneAlreadyExist();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (phoneAlreadyExist != null) {
      return phoneAlreadyExist();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return phoneAlreadyExist(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (phoneAlreadyExist != null) {
      return phoneAlreadyExist(this);
    }
    return orElse();
  }
}

abstract class FphoneAlreadyExist<T> implements AuthFailure<T> {
  const factory FphoneAlreadyExist() = _$FphoneAlreadyExist<T>;
}

/// @nodoc
abstract class $FNotACartManagerCopyWith<T, $Res> {
  factory $FNotACartManagerCopyWith(
          FNotACartManager<T> value, $Res Function(FNotACartManager<T>) then) =
      _$FNotACartManagerCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$FNotACartManagerCopyWithImpl<T, $Res>
    extends _$AuthFailureCopyWithImpl<T, $Res>
    implements $FNotACartManagerCopyWith<T, $Res> {
  _$FNotACartManagerCopyWithImpl(
      FNotACartManager<T> _value, $Res Function(FNotACartManager<T>) _then)
      : super(_value, (v) => _then(v as FNotACartManager<T>));

  @override
  FNotACartManager<T> get _value => super._value as FNotACartManager<T>;
}

/// @nodoc
class _$FNotACartManager<T> implements FNotACartManager<T> {
  const _$FNotACartManager();

  @override
  String toString() {
    return 'AuthFailure<$T>.notACartManager()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is FNotACartManager<T>);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult canceledByUser(),
    @required TResult serverError(),
    @required TResult notAllowed(),
    @required TResult accountExistWithDifferentCredential(),
    @required TResult invalidCredential(),
    @required TResult invalidEmailPasswordCombination(),
    @required TResult invalidOTP(),
    @required TResult invalidOTPVerId(),
    @required TResult invalidPhone(),
    @required TResult notAnAdmin(),
    @required TResult userNotFound(),
    @required TResult invalidEmail(),
    @required TResult invalidEmailOrPasswordValue(),
    @required TResult emailAlreadyExist(),
    @required TResult phoneAlreadyExist(),
    @required TResult notACartManager(),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return notACartManager();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult canceledByUser(),
    TResult serverError(),
    TResult notAllowed(),
    TResult accountExistWithDifferentCredential(),
    TResult invalidCredential(),
    TResult invalidEmailPasswordCombination(),
    TResult invalidOTP(),
    TResult invalidOTPVerId(),
    TResult invalidPhone(),
    TResult notAnAdmin(),
    TResult userNotFound(),
    TResult invalidEmail(),
    TResult invalidEmailOrPasswordValue(),
    TResult emailAlreadyExist(),
    TResult phoneAlreadyExist(),
    TResult notACartManager(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (notACartManager != null) {
      return notACartManager();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult canceledByUser(FCanceledByUser<T> value),
    @required TResult serverError(FServerError<T> value),
    @required TResult notAllowed(FNotAllowed<T> value),
    @required
        TResult accountExistWithDifferentCredential(
            _FAccountExistWithDifferentCredential<T> value),
    @required TResult invalidCredential(_InvalidCredential<T> value),
    @required
        TResult invalidEmailPasswordCombination(
            FInvalidEmailPasswordCombination<T> value),
    @required TResult invalidOTP(_InvalidOTP<T> value),
    @required TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    @required TResult invalidPhone(_InvalidPhone<T> value),
    @required TResult notAnAdmin(FNotAnAdmin<T> value),
    @required TResult userNotFound(FUserNotFound<T> value),
    @required TResult invalidEmail(FInvalidEmail<T> value),
    @required
        TResult invalidEmailOrPasswordValue(
            FInvalidEmailOrPasswordValue<T> value),
    @required TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    @required TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    @required TResult notACartManager(FNotACartManager<T> value),
  }) {
    assert(canceledByUser != null);
    assert(serverError != null);
    assert(notAllowed != null);
    assert(accountExistWithDifferentCredential != null);
    assert(invalidCredential != null);
    assert(invalidEmailPasswordCombination != null);
    assert(invalidOTP != null);
    assert(invalidOTPVerId != null);
    assert(invalidPhone != null);
    assert(notAnAdmin != null);
    assert(userNotFound != null);
    assert(invalidEmail != null);
    assert(invalidEmailOrPasswordValue != null);
    assert(emailAlreadyExist != null);
    assert(phoneAlreadyExist != null);
    assert(notACartManager != null);
    return notACartManager(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult canceledByUser(FCanceledByUser<T> value),
    TResult serverError(FServerError<T> value),
    TResult notAllowed(FNotAllowed<T> value),
    TResult accountExistWithDifferentCredential(
        _FAccountExistWithDifferentCredential<T> value),
    TResult invalidCredential(_InvalidCredential<T> value),
    TResult invalidEmailPasswordCombination(
        FInvalidEmailPasswordCombination<T> value),
    TResult invalidOTP(_InvalidOTP<T> value),
    TResult invalidOTPVerId(_InvalidOTPVerId<T> value),
    TResult invalidPhone(_InvalidPhone<T> value),
    TResult notAnAdmin(FNotAnAdmin<T> value),
    TResult userNotFound(FUserNotFound<T> value),
    TResult invalidEmail(FInvalidEmail<T> value),
    TResult invalidEmailOrPasswordValue(FInvalidEmailOrPasswordValue<T> value),
    TResult emailAlreadyExist(FemailAlreadyExist<T> value),
    TResult phoneAlreadyExist(FphoneAlreadyExist<T> value),
    TResult notACartManager(FNotACartManager<T> value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (notACartManager != null) {
      return notACartManager(this);
    }
    return orElse();
  }
}

abstract class FNotACartManager<T> implements AuthFailure<T> {
  const factory FNotACartManager() = _$FNotACartManager<T>;
}
