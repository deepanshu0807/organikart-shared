// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'payment_method.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$PaymentMethodTearOff {
  const _$PaymentMethodTearOff();

// ignore: unused_element
  _StCash cash() {
    return const _StCash();
  }

// ignore: unused_element
  _StOnline online() {
    return const _StOnline();
  }
}

/// @nodoc
// ignore: unused_element
const $PaymentMethod = _$PaymentMethodTearOff();

/// @nodoc
mixin _$PaymentMethod {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult cash(),
    @required TResult online(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult cash(),
    TResult online(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult cash(_StCash value),
    @required TResult online(_StOnline value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult cash(_StCash value),
    TResult online(_StOnline value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $PaymentMethodCopyWith<$Res> {
  factory $PaymentMethodCopyWith(
          PaymentMethod value, $Res Function(PaymentMethod) then) =
      _$PaymentMethodCopyWithImpl<$Res>;
}

/// @nodoc
class _$PaymentMethodCopyWithImpl<$Res>
    implements $PaymentMethodCopyWith<$Res> {
  _$PaymentMethodCopyWithImpl(this._value, this._then);

  final PaymentMethod _value;
  // ignore: unused_field
  final $Res Function(PaymentMethod) _then;
}

/// @nodoc
abstract class _$StCashCopyWith<$Res> {
  factory _$StCashCopyWith(_StCash value, $Res Function(_StCash) then) =
      __$StCashCopyWithImpl<$Res>;
}

/// @nodoc
class __$StCashCopyWithImpl<$Res> extends _$PaymentMethodCopyWithImpl<$Res>
    implements _$StCashCopyWith<$Res> {
  __$StCashCopyWithImpl(_StCash _value, $Res Function(_StCash) _then)
      : super(_value, (v) => _then(v as _StCash));

  @override
  _StCash get _value => super._value as _StCash;
}

/// @nodoc
class _$_StCash implements _StCash {
  const _$_StCash();

  @override
  String toString() {
    return 'PaymentMethod.cash()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _StCash);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult cash(),
    @required TResult online(),
  }) {
    assert(cash != null);
    assert(online != null);
    return cash();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult cash(),
    TResult online(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (cash != null) {
      return cash();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult cash(_StCash value),
    @required TResult online(_StOnline value),
  }) {
    assert(cash != null);
    assert(online != null);
    return cash(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult cash(_StCash value),
    TResult online(_StOnline value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (cash != null) {
      return cash(this);
    }
    return orElse();
  }
}

abstract class _StCash implements PaymentMethod {
  const factory _StCash() = _$_StCash;
}

/// @nodoc
abstract class _$StOnlineCopyWith<$Res> {
  factory _$StOnlineCopyWith(_StOnline value, $Res Function(_StOnline) then) =
      __$StOnlineCopyWithImpl<$Res>;
}

/// @nodoc
class __$StOnlineCopyWithImpl<$Res> extends _$PaymentMethodCopyWithImpl<$Res>
    implements _$StOnlineCopyWith<$Res> {
  __$StOnlineCopyWithImpl(_StOnline _value, $Res Function(_StOnline) _then)
      : super(_value, (v) => _then(v as _StOnline));

  @override
  _StOnline get _value => super._value as _StOnline;
}

/// @nodoc
class _$_StOnline implements _StOnline {
  const _$_StOnline();

  @override
  String toString() {
    return 'PaymentMethod.online()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _StOnline);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult cash(),
    @required TResult online(),
  }) {
    assert(cash != null);
    assert(online != null);
    return online();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult cash(),
    TResult online(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (online != null) {
      return online();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult cash(_StCash value),
    @required TResult online(_StOnline value),
  }) {
    assert(cash != null);
    assert(online != null);
    return online(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult cash(_StCash value),
    TResult online(_StOnline value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (online != null) {
      return online(this);
    }
    return orElse();
  }
}

abstract class _StOnline implements PaymentMethod {
  const factory _StOnline() = _$_StOnline;
}
