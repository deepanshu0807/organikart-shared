import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../organikart_shared_package.dart';
import 'order_state.dart';
import 'payment_method.dart';

part 'orders.freezed.dart';

@freezed
abstract class OrderEntity with _$OrderEntity {
  const factory OrderEntity({
    @required UniqueId id,
    @required OrganikartUser customer,
    @required DateTime selectedTime,
    @required List<Content> items,
    @required OrderState bookingOrderEntitytate,
    @required PaymentMethod paymentMethod,
    @required bool isPaymentDone,
    @required DateTime orderDateTime,
  }) = _OrderEntity;

  factory OrderEntity.initial() => OrderEntity(
        id: UniqueId(),
        customer: OrganikartUser(
          uId: UniqueId(),
          name: Name(""),
          emailAddress: EmailAddress(""),
          phoneNumber: PhoneNumber(""),
          lastSignInDateTime: DateTime.now(),
          role: UserRole.customer(),
        ),
        selectedTime: DateTime.now(),
        items: <Content>[],
        bookingOrderEntitytate: const OrderState.newOrder(),
        paymentMethod: const PaymentMethod.cash(),
        isPaymentDone: false,
        orderDateTime: DateTime.now(),
      );
}

extension OrderEntityX on OrderEntity {
  bool isOrderEntityValid() {
    return this.id.isValid() && customer != null && customer.name.isValid();
  }

  double totalDiscount() {
    double totalPayment = 0.0;
    for (final Content c in this.items) {
      totalPayment +=
          (c.mrp.getOrElse(1) - c.discountedMrp.getOrElse(1)).toDouble();
    }
    return totalPayment;
  }

  double totalActualPrice() {
    double totalPrice = 0.0;
    for (final Content c in this.items) {
      totalPrice += (c.mrp.getOrElse(1)).toDouble();
    }
    return totalPrice;
  }

  double amountToBePaid() {
    return totalActualPrice() - totalDiscount();
  }
}
