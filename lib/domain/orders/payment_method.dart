import 'package:freezed_annotation/freezed_annotation.dart';

part 'payment_method.freezed.dart';

@freezed
abstract class PaymentMethod with _$PaymentMethod {
  const factory PaymentMethod.cash() = _StCash;
  const factory PaymentMethod.online() = _StOnline;
}

extension PaymentMethodX on String {
  PaymentMethod toPaymentMethod() {
    switch (this) {
      case "cash":
        return const PaymentMethod.cash();
        break;
      case "online":
        return const PaymentMethod.online();
        break;
      default:
        return const PaymentMethod.cash();
    }
  }
}

extension PaymentMethodXX on PaymentMethod {
  // String toValueString() {
  //   final str = toString();
  //   final stIndex = toString().indexOf(".");
  //   return str.substring(stIndex + 1);
  // }

  String toValueString() {
    return map(
      cash: (_) => "cash",
      online: (_) => "online",
    );
  }
}
