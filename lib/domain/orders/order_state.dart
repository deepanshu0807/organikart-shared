import 'package:freezed_annotation/freezed_annotation.dart';

part 'order_state.freezed.dart';

@freezed
abstract class OrderState with _$OrderState {
  const factory OrderState.draftOrder() = StDraftOrder;
  const factory OrderState.newOrder() = StNewOrder;
  const factory OrderState.inPacking() = StInPacking;
  const factory OrderState.readyForDelivery() = StReadyForDelivery;
  const factory OrderState.pickedUp() = StPickedUp;
  const factory OrderState.delivered() = StDelivered;
  const factory OrderState.canceled() = StCanceled;
}

extension OrderStateX on String {
  OrderState toOrderState() {
    switch (this) {
      case "Draft Order":
        return const OrderState.draftOrder();
        break;
      case "Order Received":
        return const OrderState.newOrder();
        break;
      case "In Packing":
        return const OrderState.inPacking();
        break;
      case "Ready For Delivery":
        return const OrderState.readyForDelivery();
        break;
      case "pickedUp":
        return const OrderState.pickedUp();
        break;
      case "Delivered":
        return const OrderState.delivered();
        break;
      case "Cancelled":
        return const OrderState.canceled();
        break;
      default:
        return const OrderState.draftOrder();
    }
  }
}

extension OrderStateXX on OrderState {
  // String toValueString() {
  //   final str = toString();
  //   final stIndex = toString().indexOf(".");
  //   return str.substring(stIndex + 1);
  // }

  String toValueString() {
    return map(
      draftOrder: (_) => "Draft Order",
      newOrder: (_) => "Order Received",
      inPacking: (_) => "In Packing",
      readyForDelivery: (_) => "Ready For Delivery",
      pickedUp: (_) => "pickedUp",
      delivered: (_) => "Delivered",
      canceled: (_) => "Cancelled",
    );
  }
}
