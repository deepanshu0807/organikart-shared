// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'orders.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$OrderEntityTearOff {
  const _$OrderEntityTearOff();

// ignore: unused_element
  _OrderEntity call(
      {@required UniqueId id,
      @required OrganikartUser customer,
      @required DateTime selectedTime,
      @required List<Content> items,
      @required OrderState bookingOrderEntitytate,
      @required PaymentMethod paymentMethod,
      @required bool isPaymentDone,
      @required DateTime orderDateTime}) {
    return _OrderEntity(
      id: id,
      customer: customer,
      selectedTime: selectedTime,
      items: items,
      bookingOrderEntitytate: bookingOrderEntitytate,
      paymentMethod: paymentMethod,
      isPaymentDone: isPaymentDone,
      orderDateTime: orderDateTime,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $OrderEntity = _$OrderEntityTearOff();

/// @nodoc
mixin _$OrderEntity {
  UniqueId get id;
  OrganikartUser get customer;
  DateTime get selectedTime;
  List<Content> get items;
  OrderState get bookingOrderEntitytate;
  PaymentMethod get paymentMethod;
  bool get isPaymentDone;
  DateTime get orderDateTime;

  $OrderEntityCopyWith<OrderEntity> get copyWith;
}

/// @nodoc
abstract class $OrderEntityCopyWith<$Res> {
  factory $OrderEntityCopyWith(
          OrderEntity value, $Res Function(OrderEntity) then) =
      _$OrderEntityCopyWithImpl<$Res>;
  $Res call(
      {UniqueId id,
      OrganikartUser customer,
      DateTime selectedTime,
      List<Content> items,
      OrderState bookingOrderEntitytate,
      PaymentMethod paymentMethod,
      bool isPaymentDone,
      DateTime orderDateTime});

  $OrganikartUserCopyWith<$Res> get customer;
  $OrderStateCopyWith<$Res> get bookingOrderEntitytate;
  $PaymentMethodCopyWith<$Res> get paymentMethod;
}

/// @nodoc
class _$OrderEntityCopyWithImpl<$Res> implements $OrderEntityCopyWith<$Res> {
  _$OrderEntityCopyWithImpl(this._value, this._then);

  final OrderEntity _value;
  // ignore: unused_field
  final $Res Function(OrderEntity) _then;

  @override
  $Res call({
    Object id = freezed,
    Object customer = freezed,
    Object selectedTime = freezed,
    Object items = freezed,
    Object bookingOrderEntitytate = freezed,
    Object paymentMethod = freezed,
    Object isPaymentDone = freezed,
    Object orderDateTime = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as UniqueId,
      customer:
          customer == freezed ? _value.customer : customer as OrganikartUser,
      selectedTime: selectedTime == freezed
          ? _value.selectedTime
          : selectedTime as DateTime,
      items: items == freezed ? _value.items : items as List<Content>,
      bookingOrderEntitytate: bookingOrderEntitytate == freezed
          ? _value.bookingOrderEntitytate
          : bookingOrderEntitytate as OrderState,
      paymentMethod: paymentMethod == freezed
          ? _value.paymentMethod
          : paymentMethod as PaymentMethod,
      isPaymentDone: isPaymentDone == freezed
          ? _value.isPaymentDone
          : isPaymentDone as bool,
      orderDateTime: orderDateTime == freezed
          ? _value.orderDateTime
          : orderDateTime as DateTime,
    ));
  }

  @override
  $OrganikartUserCopyWith<$Res> get customer {
    if (_value.customer == null) {
      return null;
    }
    return $OrganikartUserCopyWith<$Res>(_value.customer, (value) {
      return _then(_value.copyWith(customer: value));
    });
  }

  @override
  $OrderStateCopyWith<$Res> get bookingOrderEntitytate {
    if (_value.bookingOrderEntitytate == null) {
      return null;
    }
    return $OrderStateCopyWith<$Res>(_value.bookingOrderEntitytate, (value) {
      return _then(_value.copyWith(bookingOrderEntitytate: value));
    });
  }

  @override
  $PaymentMethodCopyWith<$Res> get paymentMethod {
    if (_value.paymentMethod == null) {
      return null;
    }
    return $PaymentMethodCopyWith<$Res>(_value.paymentMethod, (value) {
      return _then(_value.copyWith(paymentMethod: value));
    });
  }
}

/// @nodoc
abstract class _$OrderEntityCopyWith<$Res>
    implements $OrderEntityCopyWith<$Res> {
  factory _$OrderEntityCopyWith(
          _OrderEntity value, $Res Function(_OrderEntity) then) =
      __$OrderEntityCopyWithImpl<$Res>;
  @override
  $Res call(
      {UniqueId id,
      OrganikartUser customer,
      DateTime selectedTime,
      List<Content> items,
      OrderState bookingOrderEntitytate,
      PaymentMethod paymentMethod,
      bool isPaymentDone,
      DateTime orderDateTime});

  @override
  $OrganikartUserCopyWith<$Res> get customer;
  @override
  $OrderStateCopyWith<$Res> get bookingOrderEntitytate;
  @override
  $PaymentMethodCopyWith<$Res> get paymentMethod;
}

/// @nodoc
class __$OrderEntityCopyWithImpl<$Res> extends _$OrderEntityCopyWithImpl<$Res>
    implements _$OrderEntityCopyWith<$Res> {
  __$OrderEntityCopyWithImpl(
      _OrderEntity _value, $Res Function(_OrderEntity) _then)
      : super(_value, (v) => _then(v as _OrderEntity));

  @override
  _OrderEntity get _value => super._value as _OrderEntity;

  @override
  $Res call({
    Object id = freezed,
    Object customer = freezed,
    Object selectedTime = freezed,
    Object items = freezed,
    Object bookingOrderEntitytate = freezed,
    Object paymentMethod = freezed,
    Object isPaymentDone = freezed,
    Object orderDateTime = freezed,
  }) {
    return _then(_OrderEntity(
      id: id == freezed ? _value.id : id as UniqueId,
      customer:
          customer == freezed ? _value.customer : customer as OrganikartUser,
      selectedTime: selectedTime == freezed
          ? _value.selectedTime
          : selectedTime as DateTime,
      items: items == freezed ? _value.items : items as List<Content>,
      bookingOrderEntitytate: bookingOrderEntitytate == freezed
          ? _value.bookingOrderEntitytate
          : bookingOrderEntitytate as OrderState,
      paymentMethod: paymentMethod == freezed
          ? _value.paymentMethod
          : paymentMethod as PaymentMethod,
      isPaymentDone: isPaymentDone == freezed
          ? _value.isPaymentDone
          : isPaymentDone as bool,
      orderDateTime: orderDateTime == freezed
          ? _value.orderDateTime
          : orderDateTime as DateTime,
    ));
  }
}

/// @nodoc
class _$_OrderEntity implements _OrderEntity {
  const _$_OrderEntity(
      {@required this.id,
      @required this.customer,
      @required this.selectedTime,
      @required this.items,
      @required this.bookingOrderEntitytate,
      @required this.paymentMethod,
      @required this.isPaymentDone,
      @required this.orderDateTime})
      : assert(id != null),
        assert(customer != null),
        assert(selectedTime != null),
        assert(items != null),
        assert(bookingOrderEntitytate != null),
        assert(paymentMethod != null),
        assert(isPaymentDone != null),
        assert(orderDateTime != null);

  @override
  final UniqueId id;
  @override
  final OrganikartUser customer;
  @override
  final DateTime selectedTime;
  @override
  final List<Content> items;
  @override
  final OrderState bookingOrderEntitytate;
  @override
  final PaymentMethod paymentMethod;
  @override
  final bool isPaymentDone;
  @override
  final DateTime orderDateTime;

  @override
  String toString() {
    return 'OrderEntity(id: $id, customer: $customer, selectedTime: $selectedTime, items: $items, bookingOrderEntitytate: $bookingOrderEntitytate, paymentMethod: $paymentMethod, isPaymentDone: $isPaymentDone, orderDateTime: $orderDateTime)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _OrderEntity &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.customer, customer) ||
                const DeepCollectionEquality()
                    .equals(other.customer, customer)) &&
            (identical(other.selectedTime, selectedTime) ||
                const DeepCollectionEquality()
                    .equals(other.selectedTime, selectedTime)) &&
            (identical(other.items, items) ||
                const DeepCollectionEquality().equals(other.items, items)) &&
            (identical(other.bookingOrderEntitytate, bookingOrderEntitytate) ||
                const DeepCollectionEquality().equals(
                    other.bookingOrderEntitytate, bookingOrderEntitytate)) &&
            (identical(other.paymentMethod, paymentMethod) ||
                const DeepCollectionEquality()
                    .equals(other.paymentMethod, paymentMethod)) &&
            (identical(other.isPaymentDone, isPaymentDone) ||
                const DeepCollectionEquality()
                    .equals(other.isPaymentDone, isPaymentDone)) &&
            (identical(other.orderDateTime, orderDateTime) ||
                const DeepCollectionEquality()
                    .equals(other.orderDateTime, orderDateTime)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(customer) ^
      const DeepCollectionEquality().hash(selectedTime) ^
      const DeepCollectionEquality().hash(items) ^
      const DeepCollectionEquality().hash(bookingOrderEntitytate) ^
      const DeepCollectionEquality().hash(paymentMethod) ^
      const DeepCollectionEquality().hash(isPaymentDone) ^
      const DeepCollectionEquality().hash(orderDateTime);

  @override
  _$OrderEntityCopyWith<_OrderEntity> get copyWith =>
      __$OrderEntityCopyWithImpl<_OrderEntity>(this, _$identity);
}

abstract class _OrderEntity implements OrderEntity {
  const factory _OrderEntity(
      {@required UniqueId id,
      @required OrganikartUser customer,
      @required DateTime selectedTime,
      @required List<Content> items,
      @required OrderState bookingOrderEntitytate,
      @required PaymentMethod paymentMethod,
      @required bool isPaymentDone,
      @required DateTime orderDateTime}) = _$_OrderEntity;

  @override
  UniqueId get id;
  @override
  OrganikartUser get customer;
  @override
  DateTime get selectedTime;
  @override
  List<Content> get items;
  @override
  OrderState get bookingOrderEntitytate;
  @override
  PaymentMethod get paymentMethod;
  @override
  bool get isPaymentDone;
  @override
  DateTime get orderDateTime;
  @override
  _$OrderEntityCopyWith<_OrderEntity> get copyWith;
}
