// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'order_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$OrderStateTearOff {
  const _$OrderStateTearOff();

// ignore: unused_element
  StDraftOrder draftOrder() {
    return const StDraftOrder();
  }

// ignore: unused_element
  StNewOrder newOrder() {
    return const StNewOrder();
  }

// ignore: unused_element
  StInPacking inPacking() {
    return const StInPacking();
  }

// ignore: unused_element
  StReadyForDelivery readyForDelivery() {
    return const StReadyForDelivery();
  }

// ignore: unused_element
  StPickedUp pickedUp() {
    return const StPickedUp();
  }

// ignore: unused_element
  StDelivered delivered() {
    return const StDelivered();
  }

// ignore: unused_element
  StCanceled canceled() {
    return const StCanceled();
  }
}

/// @nodoc
// ignore: unused_element
const $OrderState = _$OrderStateTearOff();

/// @nodoc
mixin _$OrderState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult draftOrder(),
    @required TResult newOrder(),
    @required TResult inPacking(),
    @required TResult readyForDelivery(),
    @required TResult pickedUp(),
    @required TResult delivered(),
    @required TResult canceled(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult draftOrder(),
    TResult newOrder(),
    TResult inPacking(),
    TResult readyForDelivery(),
    TResult pickedUp(),
    TResult delivered(),
    TResult canceled(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult draftOrder(StDraftOrder value),
    @required TResult newOrder(StNewOrder value),
    @required TResult inPacking(StInPacking value),
    @required TResult readyForDelivery(StReadyForDelivery value),
    @required TResult pickedUp(StPickedUp value),
    @required TResult delivered(StDelivered value),
    @required TResult canceled(StCanceled value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult draftOrder(StDraftOrder value),
    TResult newOrder(StNewOrder value),
    TResult inPacking(StInPacking value),
    TResult readyForDelivery(StReadyForDelivery value),
    TResult pickedUp(StPickedUp value),
    TResult delivered(StDelivered value),
    TResult canceled(StCanceled value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $OrderStateCopyWith<$Res> {
  factory $OrderStateCopyWith(
          OrderState value, $Res Function(OrderState) then) =
      _$OrderStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$OrderStateCopyWithImpl<$Res> implements $OrderStateCopyWith<$Res> {
  _$OrderStateCopyWithImpl(this._value, this._then);

  final OrderState _value;
  // ignore: unused_field
  final $Res Function(OrderState) _then;
}

/// @nodoc
abstract class $StDraftOrderCopyWith<$Res> {
  factory $StDraftOrderCopyWith(
          StDraftOrder value, $Res Function(StDraftOrder) then) =
      _$StDraftOrderCopyWithImpl<$Res>;
}

/// @nodoc
class _$StDraftOrderCopyWithImpl<$Res> extends _$OrderStateCopyWithImpl<$Res>
    implements $StDraftOrderCopyWith<$Res> {
  _$StDraftOrderCopyWithImpl(
      StDraftOrder _value, $Res Function(StDraftOrder) _then)
      : super(_value, (v) => _then(v as StDraftOrder));

  @override
  StDraftOrder get _value => super._value as StDraftOrder;
}

/// @nodoc
class _$StDraftOrder implements StDraftOrder {
  const _$StDraftOrder();

  @override
  String toString() {
    return 'OrderState.draftOrder()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is StDraftOrder);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult draftOrder(),
    @required TResult newOrder(),
    @required TResult inPacking(),
    @required TResult readyForDelivery(),
    @required TResult pickedUp(),
    @required TResult delivered(),
    @required TResult canceled(),
  }) {
    assert(draftOrder != null);
    assert(newOrder != null);
    assert(inPacking != null);
    assert(readyForDelivery != null);
    assert(pickedUp != null);
    assert(delivered != null);
    assert(canceled != null);
    return draftOrder();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult draftOrder(),
    TResult newOrder(),
    TResult inPacking(),
    TResult readyForDelivery(),
    TResult pickedUp(),
    TResult delivered(),
    TResult canceled(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (draftOrder != null) {
      return draftOrder();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult draftOrder(StDraftOrder value),
    @required TResult newOrder(StNewOrder value),
    @required TResult inPacking(StInPacking value),
    @required TResult readyForDelivery(StReadyForDelivery value),
    @required TResult pickedUp(StPickedUp value),
    @required TResult delivered(StDelivered value),
    @required TResult canceled(StCanceled value),
  }) {
    assert(draftOrder != null);
    assert(newOrder != null);
    assert(inPacking != null);
    assert(readyForDelivery != null);
    assert(pickedUp != null);
    assert(delivered != null);
    assert(canceled != null);
    return draftOrder(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult draftOrder(StDraftOrder value),
    TResult newOrder(StNewOrder value),
    TResult inPacking(StInPacking value),
    TResult readyForDelivery(StReadyForDelivery value),
    TResult pickedUp(StPickedUp value),
    TResult delivered(StDelivered value),
    TResult canceled(StCanceled value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (draftOrder != null) {
      return draftOrder(this);
    }
    return orElse();
  }
}

abstract class StDraftOrder implements OrderState {
  const factory StDraftOrder() = _$StDraftOrder;
}

/// @nodoc
abstract class $StNewOrderCopyWith<$Res> {
  factory $StNewOrderCopyWith(
          StNewOrder value, $Res Function(StNewOrder) then) =
      _$StNewOrderCopyWithImpl<$Res>;
}

/// @nodoc
class _$StNewOrderCopyWithImpl<$Res> extends _$OrderStateCopyWithImpl<$Res>
    implements $StNewOrderCopyWith<$Res> {
  _$StNewOrderCopyWithImpl(StNewOrder _value, $Res Function(StNewOrder) _then)
      : super(_value, (v) => _then(v as StNewOrder));

  @override
  StNewOrder get _value => super._value as StNewOrder;
}

/// @nodoc
class _$StNewOrder implements StNewOrder {
  const _$StNewOrder();

  @override
  String toString() {
    return 'OrderState.newOrder()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is StNewOrder);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult draftOrder(),
    @required TResult newOrder(),
    @required TResult inPacking(),
    @required TResult readyForDelivery(),
    @required TResult pickedUp(),
    @required TResult delivered(),
    @required TResult canceled(),
  }) {
    assert(draftOrder != null);
    assert(newOrder != null);
    assert(inPacking != null);
    assert(readyForDelivery != null);
    assert(pickedUp != null);
    assert(delivered != null);
    assert(canceled != null);
    return newOrder();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult draftOrder(),
    TResult newOrder(),
    TResult inPacking(),
    TResult readyForDelivery(),
    TResult pickedUp(),
    TResult delivered(),
    TResult canceled(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (newOrder != null) {
      return newOrder();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult draftOrder(StDraftOrder value),
    @required TResult newOrder(StNewOrder value),
    @required TResult inPacking(StInPacking value),
    @required TResult readyForDelivery(StReadyForDelivery value),
    @required TResult pickedUp(StPickedUp value),
    @required TResult delivered(StDelivered value),
    @required TResult canceled(StCanceled value),
  }) {
    assert(draftOrder != null);
    assert(newOrder != null);
    assert(inPacking != null);
    assert(readyForDelivery != null);
    assert(pickedUp != null);
    assert(delivered != null);
    assert(canceled != null);
    return newOrder(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult draftOrder(StDraftOrder value),
    TResult newOrder(StNewOrder value),
    TResult inPacking(StInPacking value),
    TResult readyForDelivery(StReadyForDelivery value),
    TResult pickedUp(StPickedUp value),
    TResult delivered(StDelivered value),
    TResult canceled(StCanceled value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (newOrder != null) {
      return newOrder(this);
    }
    return orElse();
  }
}

abstract class StNewOrder implements OrderState {
  const factory StNewOrder() = _$StNewOrder;
}

/// @nodoc
abstract class $StInPackingCopyWith<$Res> {
  factory $StInPackingCopyWith(
          StInPacking value, $Res Function(StInPacking) then) =
      _$StInPackingCopyWithImpl<$Res>;
}

/// @nodoc
class _$StInPackingCopyWithImpl<$Res> extends _$OrderStateCopyWithImpl<$Res>
    implements $StInPackingCopyWith<$Res> {
  _$StInPackingCopyWithImpl(
      StInPacking _value, $Res Function(StInPacking) _then)
      : super(_value, (v) => _then(v as StInPacking));

  @override
  StInPacking get _value => super._value as StInPacking;
}

/// @nodoc
class _$StInPacking implements StInPacking {
  const _$StInPacking();

  @override
  String toString() {
    return 'OrderState.inPacking()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is StInPacking);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult draftOrder(),
    @required TResult newOrder(),
    @required TResult inPacking(),
    @required TResult readyForDelivery(),
    @required TResult pickedUp(),
    @required TResult delivered(),
    @required TResult canceled(),
  }) {
    assert(draftOrder != null);
    assert(newOrder != null);
    assert(inPacking != null);
    assert(readyForDelivery != null);
    assert(pickedUp != null);
    assert(delivered != null);
    assert(canceled != null);
    return inPacking();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult draftOrder(),
    TResult newOrder(),
    TResult inPacking(),
    TResult readyForDelivery(),
    TResult pickedUp(),
    TResult delivered(),
    TResult canceled(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (inPacking != null) {
      return inPacking();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult draftOrder(StDraftOrder value),
    @required TResult newOrder(StNewOrder value),
    @required TResult inPacking(StInPacking value),
    @required TResult readyForDelivery(StReadyForDelivery value),
    @required TResult pickedUp(StPickedUp value),
    @required TResult delivered(StDelivered value),
    @required TResult canceled(StCanceled value),
  }) {
    assert(draftOrder != null);
    assert(newOrder != null);
    assert(inPacking != null);
    assert(readyForDelivery != null);
    assert(pickedUp != null);
    assert(delivered != null);
    assert(canceled != null);
    return inPacking(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult draftOrder(StDraftOrder value),
    TResult newOrder(StNewOrder value),
    TResult inPacking(StInPacking value),
    TResult readyForDelivery(StReadyForDelivery value),
    TResult pickedUp(StPickedUp value),
    TResult delivered(StDelivered value),
    TResult canceled(StCanceled value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (inPacking != null) {
      return inPacking(this);
    }
    return orElse();
  }
}

abstract class StInPacking implements OrderState {
  const factory StInPacking() = _$StInPacking;
}

/// @nodoc
abstract class $StReadyForDeliveryCopyWith<$Res> {
  factory $StReadyForDeliveryCopyWith(
          StReadyForDelivery value, $Res Function(StReadyForDelivery) then) =
      _$StReadyForDeliveryCopyWithImpl<$Res>;
}

/// @nodoc
class _$StReadyForDeliveryCopyWithImpl<$Res>
    extends _$OrderStateCopyWithImpl<$Res>
    implements $StReadyForDeliveryCopyWith<$Res> {
  _$StReadyForDeliveryCopyWithImpl(
      StReadyForDelivery _value, $Res Function(StReadyForDelivery) _then)
      : super(_value, (v) => _then(v as StReadyForDelivery));

  @override
  StReadyForDelivery get _value => super._value as StReadyForDelivery;
}

/// @nodoc
class _$StReadyForDelivery implements StReadyForDelivery {
  const _$StReadyForDelivery();

  @override
  String toString() {
    return 'OrderState.readyForDelivery()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is StReadyForDelivery);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult draftOrder(),
    @required TResult newOrder(),
    @required TResult inPacking(),
    @required TResult readyForDelivery(),
    @required TResult pickedUp(),
    @required TResult delivered(),
    @required TResult canceled(),
  }) {
    assert(draftOrder != null);
    assert(newOrder != null);
    assert(inPacking != null);
    assert(readyForDelivery != null);
    assert(pickedUp != null);
    assert(delivered != null);
    assert(canceled != null);
    return readyForDelivery();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult draftOrder(),
    TResult newOrder(),
    TResult inPacking(),
    TResult readyForDelivery(),
    TResult pickedUp(),
    TResult delivered(),
    TResult canceled(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (readyForDelivery != null) {
      return readyForDelivery();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult draftOrder(StDraftOrder value),
    @required TResult newOrder(StNewOrder value),
    @required TResult inPacking(StInPacking value),
    @required TResult readyForDelivery(StReadyForDelivery value),
    @required TResult pickedUp(StPickedUp value),
    @required TResult delivered(StDelivered value),
    @required TResult canceled(StCanceled value),
  }) {
    assert(draftOrder != null);
    assert(newOrder != null);
    assert(inPacking != null);
    assert(readyForDelivery != null);
    assert(pickedUp != null);
    assert(delivered != null);
    assert(canceled != null);
    return readyForDelivery(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult draftOrder(StDraftOrder value),
    TResult newOrder(StNewOrder value),
    TResult inPacking(StInPacking value),
    TResult readyForDelivery(StReadyForDelivery value),
    TResult pickedUp(StPickedUp value),
    TResult delivered(StDelivered value),
    TResult canceled(StCanceled value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (readyForDelivery != null) {
      return readyForDelivery(this);
    }
    return orElse();
  }
}

abstract class StReadyForDelivery implements OrderState {
  const factory StReadyForDelivery() = _$StReadyForDelivery;
}

/// @nodoc
abstract class $StPickedUpCopyWith<$Res> {
  factory $StPickedUpCopyWith(
          StPickedUp value, $Res Function(StPickedUp) then) =
      _$StPickedUpCopyWithImpl<$Res>;
}

/// @nodoc
class _$StPickedUpCopyWithImpl<$Res> extends _$OrderStateCopyWithImpl<$Res>
    implements $StPickedUpCopyWith<$Res> {
  _$StPickedUpCopyWithImpl(StPickedUp _value, $Res Function(StPickedUp) _then)
      : super(_value, (v) => _then(v as StPickedUp));

  @override
  StPickedUp get _value => super._value as StPickedUp;
}

/// @nodoc
class _$StPickedUp implements StPickedUp {
  const _$StPickedUp();

  @override
  String toString() {
    return 'OrderState.pickedUp()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is StPickedUp);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult draftOrder(),
    @required TResult newOrder(),
    @required TResult inPacking(),
    @required TResult readyForDelivery(),
    @required TResult pickedUp(),
    @required TResult delivered(),
    @required TResult canceled(),
  }) {
    assert(draftOrder != null);
    assert(newOrder != null);
    assert(inPacking != null);
    assert(readyForDelivery != null);
    assert(pickedUp != null);
    assert(delivered != null);
    assert(canceled != null);
    return pickedUp();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult draftOrder(),
    TResult newOrder(),
    TResult inPacking(),
    TResult readyForDelivery(),
    TResult pickedUp(),
    TResult delivered(),
    TResult canceled(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (pickedUp != null) {
      return pickedUp();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult draftOrder(StDraftOrder value),
    @required TResult newOrder(StNewOrder value),
    @required TResult inPacking(StInPacking value),
    @required TResult readyForDelivery(StReadyForDelivery value),
    @required TResult pickedUp(StPickedUp value),
    @required TResult delivered(StDelivered value),
    @required TResult canceled(StCanceled value),
  }) {
    assert(draftOrder != null);
    assert(newOrder != null);
    assert(inPacking != null);
    assert(readyForDelivery != null);
    assert(pickedUp != null);
    assert(delivered != null);
    assert(canceled != null);
    return pickedUp(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult draftOrder(StDraftOrder value),
    TResult newOrder(StNewOrder value),
    TResult inPacking(StInPacking value),
    TResult readyForDelivery(StReadyForDelivery value),
    TResult pickedUp(StPickedUp value),
    TResult delivered(StDelivered value),
    TResult canceled(StCanceled value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (pickedUp != null) {
      return pickedUp(this);
    }
    return orElse();
  }
}

abstract class StPickedUp implements OrderState {
  const factory StPickedUp() = _$StPickedUp;
}

/// @nodoc
abstract class $StDeliveredCopyWith<$Res> {
  factory $StDeliveredCopyWith(
          StDelivered value, $Res Function(StDelivered) then) =
      _$StDeliveredCopyWithImpl<$Res>;
}

/// @nodoc
class _$StDeliveredCopyWithImpl<$Res> extends _$OrderStateCopyWithImpl<$Res>
    implements $StDeliveredCopyWith<$Res> {
  _$StDeliveredCopyWithImpl(
      StDelivered _value, $Res Function(StDelivered) _then)
      : super(_value, (v) => _then(v as StDelivered));

  @override
  StDelivered get _value => super._value as StDelivered;
}

/// @nodoc
class _$StDelivered implements StDelivered {
  const _$StDelivered();

  @override
  String toString() {
    return 'OrderState.delivered()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is StDelivered);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult draftOrder(),
    @required TResult newOrder(),
    @required TResult inPacking(),
    @required TResult readyForDelivery(),
    @required TResult pickedUp(),
    @required TResult delivered(),
    @required TResult canceled(),
  }) {
    assert(draftOrder != null);
    assert(newOrder != null);
    assert(inPacking != null);
    assert(readyForDelivery != null);
    assert(pickedUp != null);
    assert(delivered != null);
    assert(canceled != null);
    return delivered();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult draftOrder(),
    TResult newOrder(),
    TResult inPacking(),
    TResult readyForDelivery(),
    TResult pickedUp(),
    TResult delivered(),
    TResult canceled(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (delivered != null) {
      return delivered();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult draftOrder(StDraftOrder value),
    @required TResult newOrder(StNewOrder value),
    @required TResult inPacking(StInPacking value),
    @required TResult readyForDelivery(StReadyForDelivery value),
    @required TResult pickedUp(StPickedUp value),
    @required TResult delivered(StDelivered value),
    @required TResult canceled(StCanceled value),
  }) {
    assert(draftOrder != null);
    assert(newOrder != null);
    assert(inPacking != null);
    assert(readyForDelivery != null);
    assert(pickedUp != null);
    assert(delivered != null);
    assert(canceled != null);
    return delivered(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult draftOrder(StDraftOrder value),
    TResult newOrder(StNewOrder value),
    TResult inPacking(StInPacking value),
    TResult readyForDelivery(StReadyForDelivery value),
    TResult pickedUp(StPickedUp value),
    TResult delivered(StDelivered value),
    TResult canceled(StCanceled value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (delivered != null) {
      return delivered(this);
    }
    return orElse();
  }
}

abstract class StDelivered implements OrderState {
  const factory StDelivered() = _$StDelivered;
}

/// @nodoc
abstract class $StCanceledCopyWith<$Res> {
  factory $StCanceledCopyWith(
          StCanceled value, $Res Function(StCanceled) then) =
      _$StCanceledCopyWithImpl<$Res>;
}

/// @nodoc
class _$StCanceledCopyWithImpl<$Res> extends _$OrderStateCopyWithImpl<$Res>
    implements $StCanceledCopyWith<$Res> {
  _$StCanceledCopyWithImpl(StCanceled _value, $Res Function(StCanceled) _then)
      : super(_value, (v) => _then(v as StCanceled));

  @override
  StCanceled get _value => super._value as StCanceled;
}

/// @nodoc
class _$StCanceled implements StCanceled {
  const _$StCanceled();

  @override
  String toString() {
    return 'OrderState.canceled()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is StCanceled);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult draftOrder(),
    @required TResult newOrder(),
    @required TResult inPacking(),
    @required TResult readyForDelivery(),
    @required TResult pickedUp(),
    @required TResult delivered(),
    @required TResult canceled(),
  }) {
    assert(draftOrder != null);
    assert(newOrder != null);
    assert(inPacking != null);
    assert(readyForDelivery != null);
    assert(pickedUp != null);
    assert(delivered != null);
    assert(canceled != null);
    return canceled();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult draftOrder(),
    TResult newOrder(),
    TResult inPacking(),
    TResult readyForDelivery(),
    TResult pickedUp(),
    TResult delivered(),
    TResult canceled(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (canceled != null) {
      return canceled();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult draftOrder(StDraftOrder value),
    @required TResult newOrder(StNewOrder value),
    @required TResult inPacking(StInPacking value),
    @required TResult readyForDelivery(StReadyForDelivery value),
    @required TResult pickedUp(StPickedUp value),
    @required TResult delivered(StDelivered value),
    @required TResult canceled(StCanceled value),
  }) {
    assert(draftOrder != null);
    assert(newOrder != null);
    assert(inPacking != null);
    assert(readyForDelivery != null);
    assert(pickedUp != null);
    assert(delivered != null);
    assert(canceled != null);
    return canceled(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult draftOrder(StDraftOrder value),
    TResult newOrder(StNewOrder value),
    TResult inPacking(StInPacking value),
    TResult readyForDelivery(StReadyForDelivery value),
    TResult pickedUp(StPickedUp value),
    TResult delivered(StDelivered value),
    TResult canceled(StCanceled value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (canceled != null) {
      return canceled(this);
    }
    return orElse();
  }
}

abstract class StCanceled implements OrderState {
  const factory StCanceled() = _$StCanceled;
}
