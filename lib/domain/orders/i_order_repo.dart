import 'package:dartz/dartz.dart';

import '../../organikart_shared_package.dart';
import 'orders.dart';

abstract class IOrdersRepo {
  Stream<Either<InfraFailure, List<OrderEntity>>> getOrders();

  Future<Either<InfraFailure, Unit>> createOrder(OrderEntity order);
  Future<Either<InfraFailure, Unit>> update(OrderEntity order);
  Future<Either<InfraFailure, Unit>> delete(OrderEntity order);

  Stream<Either<InfraFailure, List<OrderEntity>>> getAllOrdersOfUser(
      OrganikartUser user);
}
