import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:organikart_shared/domain/content/content.dart';
import 'package:organikart_shared/domain/orders/orders.dart';
import 'package:organikart_shared/domain/orders/order_state.dart';
import 'package:organikart_shared/infrastructure/auth/organikart_user_dtos.dart';
import 'package:organikart_shared/infrastructure/content/content_dto.dart';
import '../../domain/core/unique_id.dart';
import '../../domain/core/value_objects.dart';
import '../../domain/orders/payment_method.dart';

part 'order_dto.freezed.dart';
part 'order_dto.g.dart';

@freezed
abstract class OrderDto with _$OrderDto {
  factory OrderDto({
    @required String id,
    @required @OrganikartUserDtosConverter() OrganikartUserDtos customer,
    @required int selectedDateTime,
    @required @ContentDtoConverter() List<ContentDto> items,
    @required String bookingOrderState,
    @required String paymentMethod,
    @required bool isPaymentDone,
    @required int orderDateTime,
  }) = _OrderDto;

  factory OrderDto.fromDomain(OrderEntity p) {
    return OrderDto(
      id: p.id.getOrCrash(),
      customer: OrganikartUserDtos.fromDomain(p.customer),
      selectedDateTime:
          DateTime.now().add(const Duration(hours: 4)).millisecondsSinceEpoch,
      paymentMethod: p.paymentMethod.toValueString(),
      items: p.items.map((e) => ContentDto.fromDomain(e)).toList() ?? [],
      bookingOrderState: p.bookingOrderEntitytate.toValueString(),
      isPaymentDone: p.isPaymentDone,
      orderDateTime: p.orderDateTime.millisecondsSinceEpoch,
    );
  }

  factory OrderDto.fromJson(Map<String, dynamic> json) =>
      _$OrderDtoFromJson(json);
}

class OrderDtoConverter
    implements JsonConverter<OrderDto, Map<String, dynamic>> {
  const OrderDtoConverter();

  @override
  OrderDto fromJson(Map<String, dynamic> json) {
    return OrderDto.fromJson(json);
  }

  @override
  Map<String, dynamic> toJson(OrderDto fieldValue) => fieldValue.toJson();
}

extension OrderDtoX on OrderDto {
  OrderEntity toDomain() {
    return OrderEntity(
      id: UniqueId.fromUniqueString(this.id),
      customer: customer.toDomain(),
      selectedTime: DateTime.fromMillisecondsSinceEpoch(selectedDateTime),
      paymentMethod: paymentMethod.toPaymentMethod(),
      bookingOrderEntitytate: bookingOrderState.toOrderState(),
      items: items?.map((e) => e.toDomain())?.toList() ?? [],
      isPaymentDone: isPaymentDone,
      orderDateTime: DateTime.fromMillisecondsSinceEpoch(orderDateTime),
    );
  }
}
