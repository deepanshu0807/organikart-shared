import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:organikart_shared/domain/auth/user.dart';
import 'package:organikart_shared/domain/core/infra_failure.dart';
import 'package:organikart_shared/domain/orders/i_order_repo.dart';
import 'package:organikart_shared/domain/orders/orders.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter/foundation.dart';
import '../core/firebase_extensions.dart';
import 'order_dto.dart';

class OrderRepo implements IOrdersRepo {
  final FirebaseFirestore _firestore;

  OrderRepo(
    this._firestore,
  );

  @override
  Future<Either<InfraFailure, Unit>> createOrder(OrderEntity order) async {
    try {
      final c = await _firestore.orders();

      final cDto = OrderDto.fromDomain(order);

      final jsonX = cDto.toJson();
      jsonX["writeCount"] = FieldValue.increment(1);
      await c.doc(cDto.id).set(jsonX, SetOptions(merge: true));
      return right(unit);
    } catch (e, s) {
      debugPrint("ERR:$e");
      return left(const InfraFailure.serverError());
    }
  }

  @override
  Future<Either<InfraFailure, Unit>> delete(OrderEntity order) {
    throw UnimplementedError();
  }

  @override
  Stream<Either<InfraFailure, List<OrderEntity>>> getAllOrdersOfUser(
      OrganikartUser user) async* {
    yield* _firestore
        .collectionGroup("ORDERS")
        .where("customer.id", isEqualTo: user.uId.getOrElse("dflt"))
        .snapshots()
        .map((snapshot) =>
            right<InfraFailure, List<OrderEntity>>(snapshot.docs.map((doc) {
              print(doc.data());
              return OrderDto.fromJson(doc.data()).toDomain();
            }).toList()))
        .onErrorReturnWith((e) {
      debugPrint("Unexpected Error $e");
      return left(const InfraFailure.serverError());
    });
  }

  @override
  Stream<Either<InfraFailure, List<OrderEntity>>> getOrders() async* {
    // throw UnimplementedError();
    yield* _firestore
        .collectionGroup("ORDERS")
        .snapshots()
        .map((snapshot) =>
            right<InfraFailure, List<OrderEntity>>(snapshot.docs.map((doc) {
              print(doc.data());
              return OrderDto.fromJson(doc.data()).toDomain();
            }).toList()))
        .onErrorReturnWith((e) {
      debugPrint("Unexpected Error $e");

      return left(const InfraFailure.serverError());
    });
  }

  @override
  Future<Either<InfraFailure, Unit>> update(OrderEntity order) {
    // TODO: implement update
    throw UnimplementedError();
  }
}
