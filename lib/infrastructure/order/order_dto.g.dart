// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_OrderDto _$_$_OrderDtoFromJson(Map<String, dynamic> json) {
  return _$_OrderDto(
    id: json['id'] as String,
    customer: const OrganikartUserDtosConverter()
        .fromJson(json['customer'] as Map<String, dynamic>),
    selectedDateTime: json['selectedDateTime'] as int,
    items: (json['items'] as List)
        ?.map((e) =>
            const ContentDtoConverter().fromJson(e as Map<String, dynamic>))
        ?.toList(),
    bookingOrderState: json['bookingOrderState'] as String,
    paymentMethod: json['paymentMethod'] as String,
    isPaymentDone: json['isPaymentDone'] as bool,
    orderDateTime: json['orderDateTime'] as int,
  );
}

Map<String, dynamic> _$_$_OrderDtoToJson(_$_OrderDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'customer': const OrganikartUserDtosConverter().toJson(instance.customer),
      'selectedDateTime': instance.selectedDateTime,
      'items':
          instance.items?.map(const ContentDtoConverter().toJson)?.toList(),
      'bookingOrderState': instance.bookingOrderState,
      'paymentMethod': instance.paymentMethod,
      'isPaymentDone': instance.isPaymentDone,
      'orderDateTime': instance.orderDateTime,
    };
