// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'order_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
OrderDto _$OrderDtoFromJson(Map<String, dynamic> json) {
  return _OrderDto.fromJson(json);
}

/// @nodoc
class _$OrderDtoTearOff {
  const _$OrderDtoTearOff();

// ignore: unused_element
  _OrderDto call(
      {@required String id,
      @required @OrganikartUserDtosConverter() OrganikartUserDtos customer,
      @required int selectedDateTime,
      @required @ContentDtoConverter() List<ContentDto> items,
      @required String bookingOrderState,
      @required String paymentMethod,
      @required bool isPaymentDone,
      @required int orderDateTime}) {
    return _OrderDto(
      id: id,
      customer: customer,
      selectedDateTime: selectedDateTime,
      items: items,
      bookingOrderState: bookingOrderState,
      paymentMethod: paymentMethod,
      isPaymentDone: isPaymentDone,
      orderDateTime: orderDateTime,
    );
  }

// ignore: unused_element
  OrderDto fromJson(Map<String, Object> json) {
    return OrderDto.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $OrderDto = _$OrderDtoTearOff();

/// @nodoc
mixin _$OrderDto {
  String get id;
  @OrganikartUserDtosConverter()
  OrganikartUserDtos get customer;
  int get selectedDateTime;
  @ContentDtoConverter()
  List<ContentDto> get items;
  String get bookingOrderState;
  String get paymentMethod;
  bool get isPaymentDone;
  int get orderDateTime;

  Map<String, dynamic> toJson();
  $OrderDtoCopyWith<OrderDto> get copyWith;
}

/// @nodoc
abstract class $OrderDtoCopyWith<$Res> {
  factory $OrderDtoCopyWith(OrderDto value, $Res Function(OrderDto) then) =
      _$OrderDtoCopyWithImpl<$Res>;
  $Res call(
      {String id,
      @OrganikartUserDtosConverter() OrganikartUserDtos customer,
      int selectedDateTime,
      @ContentDtoConverter() List<ContentDto> items,
      String bookingOrderState,
      String paymentMethod,
      bool isPaymentDone,
      int orderDateTime});

  $OrganikartUserDtosCopyWith<$Res> get customer;
}

/// @nodoc
class _$OrderDtoCopyWithImpl<$Res> implements $OrderDtoCopyWith<$Res> {
  _$OrderDtoCopyWithImpl(this._value, this._then);

  final OrderDto _value;
  // ignore: unused_field
  final $Res Function(OrderDto) _then;

  @override
  $Res call({
    Object id = freezed,
    Object customer = freezed,
    Object selectedDateTime = freezed,
    Object items = freezed,
    Object bookingOrderState = freezed,
    Object paymentMethod = freezed,
    Object isPaymentDone = freezed,
    Object orderDateTime = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      customer: customer == freezed
          ? _value.customer
          : customer as OrganikartUserDtos,
      selectedDateTime: selectedDateTime == freezed
          ? _value.selectedDateTime
          : selectedDateTime as int,
      items: items == freezed ? _value.items : items as List<ContentDto>,
      bookingOrderState: bookingOrderState == freezed
          ? _value.bookingOrderState
          : bookingOrderState as String,
      paymentMethod: paymentMethod == freezed
          ? _value.paymentMethod
          : paymentMethod as String,
      isPaymentDone: isPaymentDone == freezed
          ? _value.isPaymentDone
          : isPaymentDone as bool,
      orderDateTime: orderDateTime == freezed
          ? _value.orderDateTime
          : orderDateTime as int,
    ));
  }

  @override
  $OrganikartUserDtosCopyWith<$Res> get customer {
    if (_value.customer == null) {
      return null;
    }
    return $OrganikartUserDtosCopyWith<$Res>(_value.customer, (value) {
      return _then(_value.copyWith(customer: value));
    });
  }
}

/// @nodoc
abstract class _$OrderDtoCopyWith<$Res> implements $OrderDtoCopyWith<$Res> {
  factory _$OrderDtoCopyWith(_OrderDto value, $Res Function(_OrderDto) then) =
      __$OrderDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      @OrganikartUserDtosConverter() OrganikartUserDtos customer,
      int selectedDateTime,
      @ContentDtoConverter() List<ContentDto> items,
      String bookingOrderState,
      String paymentMethod,
      bool isPaymentDone,
      int orderDateTime});

  @override
  $OrganikartUserDtosCopyWith<$Res> get customer;
}

/// @nodoc
class __$OrderDtoCopyWithImpl<$Res> extends _$OrderDtoCopyWithImpl<$Res>
    implements _$OrderDtoCopyWith<$Res> {
  __$OrderDtoCopyWithImpl(_OrderDto _value, $Res Function(_OrderDto) _then)
      : super(_value, (v) => _then(v as _OrderDto));

  @override
  _OrderDto get _value => super._value as _OrderDto;

  @override
  $Res call({
    Object id = freezed,
    Object customer = freezed,
    Object selectedDateTime = freezed,
    Object items = freezed,
    Object bookingOrderState = freezed,
    Object paymentMethod = freezed,
    Object isPaymentDone = freezed,
    Object orderDateTime = freezed,
  }) {
    return _then(_OrderDto(
      id: id == freezed ? _value.id : id as String,
      customer: customer == freezed
          ? _value.customer
          : customer as OrganikartUserDtos,
      selectedDateTime: selectedDateTime == freezed
          ? _value.selectedDateTime
          : selectedDateTime as int,
      items: items == freezed ? _value.items : items as List<ContentDto>,
      bookingOrderState: bookingOrderState == freezed
          ? _value.bookingOrderState
          : bookingOrderState as String,
      paymentMethod: paymentMethod == freezed
          ? _value.paymentMethod
          : paymentMethod as String,
      isPaymentDone: isPaymentDone == freezed
          ? _value.isPaymentDone
          : isPaymentDone as bool,
      orderDateTime: orderDateTime == freezed
          ? _value.orderDateTime
          : orderDateTime as int,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_OrderDto with DiagnosticableTreeMixin implements _OrderDto {
  _$_OrderDto(
      {@required this.id,
      @required @OrganikartUserDtosConverter() this.customer,
      @required this.selectedDateTime,
      @required @ContentDtoConverter() this.items,
      @required this.bookingOrderState,
      @required this.paymentMethod,
      @required this.isPaymentDone,
      @required this.orderDateTime})
      : assert(id != null),
        assert(customer != null),
        assert(selectedDateTime != null),
        assert(items != null),
        assert(bookingOrderState != null),
        assert(paymentMethod != null),
        assert(isPaymentDone != null),
        assert(orderDateTime != null);

  factory _$_OrderDto.fromJson(Map<String, dynamic> json) =>
      _$_$_OrderDtoFromJson(json);

  @override
  final String id;
  @override
  @OrganikartUserDtosConverter()
  final OrganikartUserDtos customer;
  @override
  final int selectedDateTime;
  @override
  @ContentDtoConverter()
  final List<ContentDto> items;
  @override
  final String bookingOrderState;
  @override
  final String paymentMethod;
  @override
  final bool isPaymentDone;
  @override
  final int orderDateTime;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'OrderDto(id: $id, customer: $customer, selectedDateTime: $selectedDateTime, items: $items, bookingOrderState: $bookingOrderState, paymentMethod: $paymentMethod, isPaymentDone: $isPaymentDone, orderDateTime: $orderDateTime)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'OrderDto'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('customer', customer))
      ..add(DiagnosticsProperty('selectedDateTime', selectedDateTime))
      ..add(DiagnosticsProperty('items', items))
      ..add(DiagnosticsProperty('bookingOrderState', bookingOrderState))
      ..add(DiagnosticsProperty('paymentMethod', paymentMethod))
      ..add(DiagnosticsProperty('isPaymentDone', isPaymentDone))
      ..add(DiagnosticsProperty('orderDateTime', orderDateTime));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _OrderDto &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.customer, customer) ||
                const DeepCollectionEquality()
                    .equals(other.customer, customer)) &&
            (identical(other.selectedDateTime, selectedDateTime) ||
                const DeepCollectionEquality()
                    .equals(other.selectedDateTime, selectedDateTime)) &&
            (identical(other.items, items) ||
                const DeepCollectionEquality().equals(other.items, items)) &&
            (identical(other.bookingOrderState, bookingOrderState) ||
                const DeepCollectionEquality()
                    .equals(other.bookingOrderState, bookingOrderState)) &&
            (identical(other.paymentMethod, paymentMethod) ||
                const DeepCollectionEquality()
                    .equals(other.paymentMethod, paymentMethod)) &&
            (identical(other.isPaymentDone, isPaymentDone) ||
                const DeepCollectionEquality()
                    .equals(other.isPaymentDone, isPaymentDone)) &&
            (identical(other.orderDateTime, orderDateTime) ||
                const DeepCollectionEquality()
                    .equals(other.orderDateTime, orderDateTime)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(customer) ^
      const DeepCollectionEquality().hash(selectedDateTime) ^
      const DeepCollectionEquality().hash(items) ^
      const DeepCollectionEquality().hash(bookingOrderState) ^
      const DeepCollectionEquality().hash(paymentMethod) ^
      const DeepCollectionEquality().hash(isPaymentDone) ^
      const DeepCollectionEquality().hash(orderDateTime);

  @override
  _$OrderDtoCopyWith<_OrderDto> get copyWith =>
      __$OrderDtoCopyWithImpl<_OrderDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_OrderDtoToJson(this);
  }
}

abstract class _OrderDto implements OrderDto {
  factory _OrderDto(
      {@required String id,
      @required @OrganikartUserDtosConverter() OrganikartUserDtos customer,
      @required int selectedDateTime,
      @required @ContentDtoConverter() List<ContentDto> items,
      @required String bookingOrderState,
      @required String paymentMethod,
      @required bool isPaymentDone,
      @required int orderDateTime}) = _$_OrderDto;

  factory _OrderDto.fromJson(Map<String, dynamic> json) = _$_OrderDto.fromJson;

  @override
  String get id;
  @override
  @OrganikartUserDtosConverter()
  OrganikartUserDtos get customer;
  @override
  int get selectedDateTime;
  @override
  @ContentDtoConverter()
  List<ContentDto> get items;
  @override
  String get bookingOrderState;
  @override
  String get paymentMethod;
  @override
  bool get isPaymentDone;
  @override
  int get orderDateTime;
  @override
  _$OrderDtoCopyWith<_OrderDto> get copyWith;
}
