// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'enquiry_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
EnquiryDto _$EnquiryDtoFromJson(Map<String, dynamic> json) {
  return _EnquiryDto.fromJson(json);
}

/// @nodoc
class _$EnquiryDtoTearOff {
  const _$EnquiryDtoTearOff();

// ignore: unused_element
  _EnquiryDto call(
      {@required String id,
      @required String emailAddress,
      @required String message,
      @required String name,
      @required String phoneNumber}) {
    return _EnquiryDto(
      id: id,
      emailAddress: emailAddress,
      message: message,
      name: name,
      phoneNumber: phoneNumber,
    );
  }

// ignore: unused_element
  EnquiryDto fromJson(Map<String, Object> json) {
    return EnquiryDto.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $EnquiryDto = _$EnquiryDtoTearOff();

/// @nodoc
mixin _$EnquiryDto {
  String get id;
  String get emailAddress;
  String get message;
  String get name;
  String get phoneNumber;

  Map<String, dynamic> toJson();
  $EnquiryDtoCopyWith<EnquiryDto> get copyWith;
}

/// @nodoc
abstract class $EnquiryDtoCopyWith<$Res> {
  factory $EnquiryDtoCopyWith(
          EnquiryDto value, $Res Function(EnquiryDto) then) =
      _$EnquiryDtoCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String emailAddress,
      String message,
      String name,
      String phoneNumber});
}

/// @nodoc
class _$EnquiryDtoCopyWithImpl<$Res> implements $EnquiryDtoCopyWith<$Res> {
  _$EnquiryDtoCopyWithImpl(this._value, this._then);

  final EnquiryDto _value;
  // ignore: unused_field
  final $Res Function(EnquiryDto) _then;

  @override
  $Res call({
    Object id = freezed,
    Object emailAddress = freezed,
    Object message = freezed,
    Object name = freezed,
    Object phoneNumber = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      emailAddress: emailAddress == freezed
          ? _value.emailAddress
          : emailAddress as String,
      message: message == freezed ? _value.message : message as String,
      name: name == freezed ? _value.name : name as String,
      phoneNumber:
          phoneNumber == freezed ? _value.phoneNumber : phoneNumber as String,
    ));
  }
}

/// @nodoc
abstract class _$EnquiryDtoCopyWith<$Res> implements $EnquiryDtoCopyWith<$Res> {
  factory _$EnquiryDtoCopyWith(
          _EnquiryDto value, $Res Function(_EnquiryDto) then) =
      __$EnquiryDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String emailAddress,
      String message,
      String name,
      String phoneNumber});
}

/// @nodoc
class __$EnquiryDtoCopyWithImpl<$Res> extends _$EnquiryDtoCopyWithImpl<$Res>
    implements _$EnquiryDtoCopyWith<$Res> {
  __$EnquiryDtoCopyWithImpl(
      _EnquiryDto _value, $Res Function(_EnquiryDto) _then)
      : super(_value, (v) => _then(v as _EnquiryDto));

  @override
  _EnquiryDto get _value => super._value as _EnquiryDto;

  @override
  $Res call({
    Object id = freezed,
    Object emailAddress = freezed,
    Object message = freezed,
    Object name = freezed,
    Object phoneNumber = freezed,
  }) {
    return _then(_EnquiryDto(
      id: id == freezed ? _value.id : id as String,
      emailAddress: emailAddress == freezed
          ? _value.emailAddress
          : emailAddress as String,
      message: message == freezed ? _value.message : message as String,
      name: name == freezed ? _value.name : name as String,
      phoneNumber:
          phoneNumber == freezed ? _value.phoneNumber : phoneNumber as String,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_EnquiryDto implements _EnquiryDto {
  _$_EnquiryDto(
      {@required this.id,
      @required this.emailAddress,
      @required this.message,
      @required this.name,
      @required this.phoneNumber})
      : assert(id != null),
        assert(emailAddress != null),
        assert(message != null),
        assert(name != null),
        assert(phoneNumber != null);

  factory _$_EnquiryDto.fromJson(Map<String, dynamic> json) =>
      _$_$_EnquiryDtoFromJson(json);

  @override
  final String id;
  @override
  final String emailAddress;
  @override
  final String message;
  @override
  final String name;
  @override
  final String phoneNumber;

  @override
  String toString() {
    return 'EnquiryDto(id: $id, emailAddress: $emailAddress, message: $message, name: $name, phoneNumber: $phoneNumber)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EnquiryDto &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.emailAddress, emailAddress) ||
                const DeepCollectionEquality()
                    .equals(other.emailAddress, emailAddress)) &&
            (identical(other.message, message) ||
                const DeepCollectionEquality()
                    .equals(other.message, message)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.phoneNumber, phoneNumber) ||
                const DeepCollectionEquality()
                    .equals(other.phoneNumber, phoneNumber)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(emailAddress) ^
      const DeepCollectionEquality().hash(message) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(phoneNumber);

  @override
  _$EnquiryDtoCopyWith<_EnquiryDto> get copyWith =>
      __$EnquiryDtoCopyWithImpl<_EnquiryDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_EnquiryDtoToJson(this);
  }
}

abstract class _EnquiryDto implements EnquiryDto {
  factory _EnquiryDto(
      {@required String id,
      @required String emailAddress,
      @required String message,
      @required String name,
      @required String phoneNumber}) = _$_EnquiryDto;

  factory _EnquiryDto.fromJson(Map<String, dynamic> json) =
      _$_EnquiryDto.fromJson;

  @override
  String get id;
  @override
  String get emailAddress;
  @override
  String get message;
  @override
  String get name;
  @override
  String get phoneNumber;
  @override
  _$EnquiryDtoCopyWith<_EnquiryDto> get copyWith;
}
