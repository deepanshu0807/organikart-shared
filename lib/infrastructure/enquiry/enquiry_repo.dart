import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:organikart_shared/infrastructure/enquiry/enquiry_dto.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

class EnquiryRepo implements IEnquiryRepo {
  final FirebaseFirestore _firestore;

  EnquiryRepo(
    this._firestore,
  );
  @override
  Future<Either<InfraFailure, Unit>> create(Enquiry enquiry) async {
    try {
      final cRef = await _firestore.enquiry();
      final cDto = EnquiryDto.fromDomain(enquiry);

      final jsonX = cDto.toJson();

      jsonX["writeCount"] = FieldValue.increment(1);
      await cRef.doc(cDto.id).set(jsonX, SetOptions(merge: true));
      return right(unit);
    } catch (e) {
      // These error codes and messages aren't in the documentation AFAIK, experiment in the debugger to find out about them.
      debugPrint("ERR:$e");
      return left(const InfraFailure.serverError());
    }
  }

  @override
  Future<Either<InfraFailure, Unit>> delete(Enquiry enquiry) async {
    try {
      final cRef = await _firestore.enquiry();
      final cDto = EnquiryDto.fromDomain(enquiry);
      await cRef.doc(cDto.id).delete();
      return right(unit);
    } catch (e) {
      debugPrint("ERR:: $e");
      return left(const InfraFailure.serverError());
    }
  }

  @override
  Stream<Either<InfraFailure, List<Enquiry>>> getEnquiry() async* {
    final c = await _firestore.enquiry();
    yield* c
        .snapshots()
        .map(
          (snapshot) => right<InfraFailure, List<Enquiry>>(snapshot.docs
              .map((doc) => EnquiryDto.fromJson(doc.data()).toDomain())
              .toList()),
        )
        .onErrorReturnWith((e) {
      debugPrint("Unexpected Error $e");
      return left(const InfraFailure.serverError());
    });
  }
}
