// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'enquiry_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_EnquiryDto _$_$_EnquiryDtoFromJson(Map<String, dynamic> json) {
  return _$_EnquiryDto(
    id: json['id'] as String,
    emailAddress: json['emailAddress'] as String,
    message: json['message'] as String,
    name: json['name'] as String,
    phoneNumber: json['phoneNumber'] as String,
  );
}

Map<String, dynamic> _$_$_EnquiryDtoToJson(_$_EnquiryDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'emailAddress': instance.emailAddress,
      'message': instance.message,
      'name': instance.name,
      'phoneNumber': instance.phoneNumber,
    };
