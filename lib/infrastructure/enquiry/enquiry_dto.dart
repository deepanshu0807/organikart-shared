import 'package:organikart_shared/organikart_shared_package.dart';

part 'enquiry_dto.freezed.dart';
part 'enquiry_dto.g.dart';

@freezed
abstract class EnquiryDto with _$EnquiryDto {
  factory EnquiryDto(
      {@required String id,
      @required String emailAddress,
      @required String message,
      @required String name,
      @required String phoneNumber}) = _EnquiryDto;

  factory EnquiryDto.fromDomain(Enquiry b) {
    return EnquiryDto(
        id: b.id.getOrCrash(),
        emailAddress: b.emailAddress.getOrCrash(),
        name: b.name.getOrCrash(),
        phoneNumber: b.phoneNumber.getOrCrash(),
        message: b.message);
  }

  factory EnquiryDto.fromJson(Map<String, dynamic> json) =>
      _$EnquiryDtoFromJson(json);

  // factory EnquiryDto.fromFirestore(DocumentSnapshot doc) {
  //   return EnquiryDto.fromJson(doc.data()).copyWith(id: doc.id);
  // }
}

class EnquiryDtoConverter
    implements JsonConverter<EnquiryDto, Map<String, dynamic>> {
  const EnquiryDtoConverter();

  @override
  EnquiryDto fromJson(Map<String, dynamic> json) {
    return json == null ? null : EnquiryDto.fromJson(json);
  }

  @override
  Map<String, dynamic> toJson(EnquiryDto fieldValue) => fieldValue?.toJson();
}

extension EnquiryDtoX on EnquiryDto {
  Enquiry toDomain() {
    return Enquiry(
      id: UniqueId.fromUniqueString(id),
      emailAddress: EmailAddress(emailAddress),
      name: Name(name),
      phoneNumber: PhoneNumber(phoneNumber),
      message: message,
    );
  }
}
