// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'saved_product_dtos.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SavedProductsDto _$_$_SavedProductsDtoFromJson(Map<String, dynamic> json) {
  return _$_SavedProductsDto(
    allProducts: (json['allProducts'] as List)
        ?.map((e) =>
            const ContentDtoConverter().fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$_$_SavedProductsDtoToJson(
        _$_SavedProductsDto instance) =>
    <String, dynamic>{
      'allProducts': instance.allProducts
          ?.map(const ContentDtoConverter().toJson)
          ?.toList(),
    };
