import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:dartz/dartz.dart';
import 'package:organikart_shared/domain/features/i_customer_repo.dart';
import 'package:organikart_shared/domain/features/saved_products.dart';
import 'package:organikart_shared/infrastructure/content/content_dto.dart';
import 'package:organikart_shared/infrastructure/features/saved_product_dtos.dart';

import '../../organikart_shared_package.dart';
import '../core/firebase_extensions.dart';

class CustomerRepo implements ICustomerRepo {
  final FirebaseFirestore _firestore;

  CustomerRepo(this._firestore);
  @override
  Future<Either<InfraFailure, Unit>> create(OrganikartUser user) async {
    try {
      final cRef = await _firestore.customers();
      final cDto = OrganikartUserDtos.fromDomain(user);

      final jsonX = cDto.toJson();
      jsonX.remove("picUrl");
      jsonX["writeCount"] = FieldValue.increment(1);
      await cRef.doc(cDto.id).set(jsonX, SetOptions(merge: true));
      return right(unit);
    } catch (e, s) {
      // These error codes and messages aren't in the documentation AFAIK, experiment in the debugger to find out about them.
      debugPrint("ERR:$e\n\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");

      return left(const InfraFailure.serverError());
    }
  }

  @override
  Future<Either<InfraFailure, Unit>> delete(OrganikartUser user) async {
    try {
      final cRef = await _firestore.customers();
      final cDto = OrganikartUserDtos.fromDomain(user);
      await cRef.doc(cDto.name.toUpperCase()).delete();
      return right(unit);
    } catch (e, s) {
      debugPrint("ERR:: $e");
      return left(const InfraFailure.serverError());
    }
  }

  @override
  Stream<Either<InfraFailure, OrganikartUser>> getStoreUser(
      OrganikartUser user) async* {
    final c = await _firestore.customers();
    yield* c
        .doc(user.uId.getOrElse("dflt"))
        .snapshots()
        .map((doc) => right<InfraFailure, OrganikartUser>(
            OrganikartUserDtos.fromJson(doc.data()).toDomain()))
        .onErrorReturnWith((e) {
      debugPrint("Unexpected Error $e");
      return left(const InfraFailure.serverError());
    });
  }

  @override
  Stream<Either<InfraFailure, List<OrganikartUser>>> getAllUsers() async* {
    final c = await _firestore.customers();
    yield* c
        .snapshots()
        .map(
          (snapshot) => right<InfraFailure, List<OrganikartUser>>(snapshot.docs
              .map((doc) => OrganikartUserDtos.fromJson(doc.data()).toDomain())
              .toList()),
        )
        .onErrorReturnWith((e) {
      debugPrint("Unexpected Error $e");
      return left(const InfraFailure.serverError());
    });
  }

  @override
  Future<Either<InfraFailure, Unit>> update(OrganikartUser user) {
    // TODO: implement update
    throw UnimplementedError();
  }

  @override
  Future<Either<InfraFailure, Unit>> saveOrRemoveProductToMySavedList(
    OrganikartUser user,
    Content product,
  ) async {
    try {
      final cRef = await _firestore.savedProductOfCustomer(user);

      final saveProductsJson = (await cRef.get()).data();
      var newProductList = <ContentDto>[];

      if (saveProductsJson != null &&
          saveProductsJson.keys.contains("allProducts")) {
        final SavedProductsDto savedProducts =
            SavedProductsDto.fromJson(saveProductsJson);
        final prod = savedProducts.allProducts.firstWhere(
            (element) => element.id == product.id.getOrElse("dflt"),
            orElse: () => null);

        if (prod == null) {
          newProductList = [
            ...savedProducts.allProducts,
            ContentDto.fromDomain(product),
          ];
        } else {
          savedProducts.allProducts.remove(ContentDto.fromDomain(product));
          newProductList = savedProducts.allProducts;
        }
      } else {
        newProductList = [ContentDto.fromDomain(product)];
      }

      final cDto = SavedProductsDto(allProducts: newProductList);

      final jsonX = cDto.toJson();
      jsonX["writeCount"] = FieldValue.increment(1);
      await cRef.set(jsonX, SetOptions(merge: true));
      return right(unit);
    } catch (e, s) {
      // These error codes and messages aren't in the documentation AFAIK, experiment in the debugger to find out about them.
      debugPrint("ERR:$e\n\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");

      return left(const InfraFailure.serverError());
    }
  }

  @override
  Stream<Either<InfraFailure, SavedProducts>> getMySavedProducts(
      OrganikartUser user) async* {
    final c = await _firestore.savedProductOfCustomer(user);
    yield* c.snapshots().map((doc) {
      // print(doc.data());
      if (doc.data() != null) {
        return right<InfraFailure, SavedProducts>(
            SavedProductsDto.fromJson(doc.data()).toDomain());
      } else {
        return left<InfraFailure, SavedProducts>(const InfraFailure.notFound());
      }
    }).onErrorReturnWith((e) {
      debugPrint("Unexpected Error $e");
      return left(const InfraFailure.serverError());
    });
  }
}
