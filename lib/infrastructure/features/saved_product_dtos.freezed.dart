// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'saved_product_dtos.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
SavedProductsDto _$SavedProductsDtoFromJson(Map<String, dynamic> json) {
  return _SavedProductsDto.fromJson(json);
}

/// @nodoc
class _$SavedProductsDtoTearOff {
  const _$SavedProductsDtoTearOff();

// ignore: unused_element
  _SavedProductsDto call(
      {@ContentDtoConverter() List<ContentDto> allProducts}) {
    return _SavedProductsDto(
      allProducts: allProducts,
    );
  }

// ignore: unused_element
  SavedProductsDto fromJson(Map<String, Object> json) {
    return SavedProductsDto.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $SavedProductsDto = _$SavedProductsDtoTearOff();

/// @nodoc
mixin _$SavedProductsDto {
  @ContentDtoConverter()
  List<ContentDto> get allProducts;

  Map<String, dynamic> toJson();
  $SavedProductsDtoCopyWith<SavedProductsDto> get copyWith;
}

/// @nodoc
abstract class $SavedProductsDtoCopyWith<$Res> {
  factory $SavedProductsDtoCopyWith(
          SavedProductsDto value, $Res Function(SavedProductsDto) then) =
      _$SavedProductsDtoCopyWithImpl<$Res>;
  $Res call({@ContentDtoConverter() List<ContentDto> allProducts});
}

/// @nodoc
class _$SavedProductsDtoCopyWithImpl<$Res>
    implements $SavedProductsDtoCopyWith<$Res> {
  _$SavedProductsDtoCopyWithImpl(this._value, this._then);

  final SavedProductsDto _value;
  // ignore: unused_field
  final $Res Function(SavedProductsDto) _then;

  @override
  $Res call({
    Object allProducts = freezed,
  }) {
    return _then(_value.copyWith(
      allProducts: allProducts == freezed
          ? _value.allProducts
          : allProducts as List<ContentDto>,
    ));
  }
}

/// @nodoc
abstract class _$SavedProductsDtoCopyWith<$Res>
    implements $SavedProductsDtoCopyWith<$Res> {
  factory _$SavedProductsDtoCopyWith(
          _SavedProductsDto value, $Res Function(_SavedProductsDto) then) =
      __$SavedProductsDtoCopyWithImpl<$Res>;
  @override
  $Res call({@ContentDtoConverter() List<ContentDto> allProducts});
}

/// @nodoc
class __$SavedProductsDtoCopyWithImpl<$Res>
    extends _$SavedProductsDtoCopyWithImpl<$Res>
    implements _$SavedProductsDtoCopyWith<$Res> {
  __$SavedProductsDtoCopyWithImpl(
      _SavedProductsDto _value, $Res Function(_SavedProductsDto) _then)
      : super(_value, (v) => _then(v as _SavedProductsDto));

  @override
  _SavedProductsDto get _value => super._value as _SavedProductsDto;

  @override
  $Res call({
    Object allProducts = freezed,
  }) {
    return _then(_SavedProductsDto(
      allProducts: allProducts == freezed
          ? _value.allProducts
          : allProducts as List<ContentDto>,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_SavedProductsDto implements _SavedProductsDto {
  _$_SavedProductsDto({@ContentDtoConverter() this.allProducts});

  factory _$_SavedProductsDto.fromJson(Map<String, dynamic> json) =>
      _$_$_SavedProductsDtoFromJson(json);

  @override
  @ContentDtoConverter()
  final List<ContentDto> allProducts;

  @override
  String toString() {
    return 'SavedProductsDto(allProducts: $allProducts)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SavedProductsDto &&
            (identical(other.allProducts, allProducts) ||
                const DeepCollectionEquality()
                    .equals(other.allProducts, allProducts)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(allProducts);

  @override
  _$SavedProductsDtoCopyWith<_SavedProductsDto> get copyWith =>
      __$SavedProductsDtoCopyWithImpl<_SavedProductsDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_SavedProductsDtoToJson(this);
  }
}

abstract class _SavedProductsDto implements SavedProductsDto {
  factory _SavedProductsDto(
          {@ContentDtoConverter() List<ContentDto> allProducts}) =
      _$_SavedProductsDto;

  factory _SavedProductsDto.fromJson(Map<String, dynamic> json) =
      _$_SavedProductsDto.fromJson;

  @override
  @ContentDtoConverter()
  List<ContentDto> get allProducts;
  @override
  _$SavedProductsDtoCopyWith<_SavedProductsDto> get copyWith;
}
