import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:organikart_shared/domain/features/saved_products.dart';
import 'package:organikart_shared/infrastructure/content/content_dto.dart';
part 'saved_product_dtos.freezed.dart';
part 'saved_product_dtos.g.dart';

@freezed
abstract class SavedProductsDto with _$SavedProductsDto {
  factory SavedProductsDto({
    @ContentDtoConverter() List<ContentDto> allProducts,
  }) = _SavedProductsDto;

  factory SavedProductsDto.fromDomain(SavedProducts p) {
    return SavedProductsDto(
      allProducts: p.allProducts.map((e) => ContentDto.fromDomain(e)).toList(),
    );
  }

  factory SavedProductsDto.fromJson(Map<String, dynamic> json) =>
      _$SavedProductsDtoFromJson(json);
}

class SavedProductsDtoConverter
    implements JsonConverter<SavedProductsDto, Map<String, dynamic>> {
  const SavedProductsDtoConverter();

  @override
  SavedProductsDto fromJson(Map<String, dynamic> json) {
    return SavedProductsDto.fromJson(json);
  }

  @override
  Map<String, dynamic> toJson(SavedProductsDto fieldValue) =>
      fieldValue.toJson();
}

extension SavedProductsDtoX on SavedProductsDto {
  SavedProducts toDomain() {
    return SavedProducts(
      allProducts: allProducts.map((e) => e.toDomain()).toList(),
    );
  }
}
