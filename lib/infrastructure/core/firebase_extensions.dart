import 'package:firebase_auth/firebase_auth.dart';
import 'package:organikart_shared/organikart_shared_package.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

extension FirebaseUserExt on User {
  OrganikartUser toDomain(Map<dynamic, dynamic> claims) {
    return OrganikartUser(
      uId: UniqueId.fromUniqueString(uid),
      name: Name(displayName),
      emailAddress: email != null && email.isNotEmpty
          ? EmailAddress(email)
          : EmailAddress(""),
      // isAdmin: claims["isAdmin"] == true,
      role: const UserRole
          .superAdmin(), // (claims["role"] as String).toUserRole(),
      phoneNumber: PhoneNumber(phoneNumber),
      picUrl: photoURL,
      lastSignInDateTime: metadata.lastSignInTime,
    );
  }
}

extension FirestoreX on FirebaseFirestore {
  Future<CollectionReference> customers() async {
    return FirebaseFirestore.instance.collection('CUSTOMERS');
  }

  Future<CollectionReference> categories() async {
    return FirebaseFirestore.instance.collection('CATEGORIES');
  }

  Future<CollectionReference> content() async {
    return FirebaseFirestore.instance.collection('PRODUCTS');
  }

  Future<CollectionReference> enquiry() async {
    return FirebaseFirestore.instance.collection('ENQUIRY');
  }

  Future<DocumentReference> savedProductOfCustomer(OrganikartUser user) async {
    return collection('CUSTOMERS')
        .doc(user.uId.getOrElse(""))
        .collection("SAVED_PRODUCTS")
        .doc("ALL_PRODUCTS");
  }

  Future<CollectionReference> orders() async {
    return FirebaseFirestore.instance.collection("ORDERS");
  }
}
