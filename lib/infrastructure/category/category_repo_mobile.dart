import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:organikart_shared/domain/category/category.dart';
import 'package:organikart_shared/domain/category/i_category_repo.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

import 'category_dtos.dart';

class CategoryRepo implements ICategoryRepo {
  final FirebaseFirestore _firestore;

  final FirebaseStorage _firebaseStorage;

  CategoryRepo(this._firestore, this._firebaseStorage);

  @override
  Future<Either<InfraFailure, Unit>> create(Category category) async {
    try {
      final cRef = await _firestore.categories();
      final cDto = CategoryDto.fromDomain(category);

      final jsonX = cDto.toJson();
      print("JsonX \n $jsonX");
      jsonX["writeCount"] = FieldValue.increment(1);
      await cRef.doc(cDto.id).set(jsonX, SetOptions(merge: true));
      return right(unit);
    } catch (e) {
      // These error codes and messages aren't in the documentation AFAIK, experiment in the debugger to find out about them.
      debugPrint("ERR:$e");
      return left(const InfraFailure.serverError());
    }
  }

  @override
  Future<Either<InfraFailure, Unit>> delete(Category category) async {
    try {
      final cRef = await _firestore.categories();
      final cDto = CategoryDto.fromDomain(category);
      await cRef.doc(cDto.id).delete();
      return right(unit);
    } catch (e) {
      debugPrint("ERR:: $e");

      return left(const InfraFailure.serverError());
    }
  }

  @override
  Stream<Either<InfraFailure, List<Category>>> getAllCategory() async* {
    final c = await _firestore.categories();
    yield* c
        .snapshots()
        .map(
          (snapshot) => right<InfraFailure, List<Category>>(snapshot.docs
              .map((doc) => CategoryDto.fromJson(doc.data()).toDomain())
              .toList()),
        )
        .onErrorReturnWith((e) {
      debugPrint("Unexpected Error $e");

      return left(const InfraFailure.serverError());
    });
  }

  @override
  Future<Either<InfraFailure, UploadResult>> uploadTitleImage(
      Category category) {
    // TODO: implement uploadTitleImage
    throw UnimplementedError();
  }
}
