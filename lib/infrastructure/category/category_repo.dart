import 'package:dartz/dartz.dart';
import 'package:organikart_shared/domain/category/category.dart';
import 'package:organikart_shared/domain/category/i_category_repo.dart';
import 'package:firebase/firebase.dart' as fb;
import 'package:organikart_shared/organikart_shared_package.dart';

class CategoryRepo implements ICategoryRepo {
  final FirebaseFirestore _firestore;

  final fb.Storage _firebaseStorage;

  CategoryRepo(this._firestore, this._firebaseStorage);

  @override
  Future<Either<InfraFailure, Unit>> create(Category category) {
    // TODO: implement create
    throw UnimplementedError();
  }

  @override
  Future<Either<InfraFailure, Unit>> delete(Category category) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Stream<Either<InfraFailure, List<Category>>> getAllCategory() {
    // TODO: implement getAllCategory
    throw UnimplementedError();
  }

  @override
  Future<Either<InfraFailure, UploadResult>> uploadTitleImage(
      Category category) {
    // TODO: implement uploadTitleImage
    throw UnimplementedError();
  }
}
