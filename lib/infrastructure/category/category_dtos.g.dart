// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_dtos.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CategoryDto _$_$_CategoryDtoFromJson(Map<String, dynamic> json) {
  return _$_CategoryDto(
    id: json['id'] as String,
    title: json['title'] as String,
    titlePicUrl: json['titlePicUrl'] as String,
    shortDescription: json['shortDescription'] as String,
  );
}

Map<String, dynamic> _$_$_CategoryDtoToJson(_$_CategoryDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'titlePicUrl': instance.titlePicUrl,
      'shortDescription': instance.shortDescription,
    };
