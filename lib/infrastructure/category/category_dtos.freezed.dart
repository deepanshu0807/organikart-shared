// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'category_dtos.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
CategoryDto _$CategoryDtoFromJson(Map<String, dynamic> json) {
  return _CategoryDto.fromJson(json);
}

/// @nodoc
class _$CategoryDtoTearOff {
  const _$CategoryDtoTearOff();

// ignore: unused_element
  _CategoryDto call(
      {@required String id,
      @required String title,
      @required String titlePicUrl,
      String shortDescription}) {
    return _CategoryDto(
      id: id,
      title: title,
      titlePicUrl: titlePicUrl,
      shortDescription: shortDescription,
    );
  }

// ignore: unused_element
  CategoryDto fromJson(Map<String, Object> json) {
    return CategoryDto.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $CategoryDto = _$CategoryDtoTearOff();

/// @nodoc
mixin _$CategoryDto {
  String get id;
  String get title;
  String get titlePicUrl;
  String get shortDescription;

  Map<String, dynamic> toJson();
  $CategoryDtoCopyWith<CategoryDto> get copyWith;
}

/// @nodoc
abstract class $CategoryDtoCopyWith<$Res> {
  factory $CategoryDtoCopyWith(
          CategoryDto value, $Res Function(CategoryDto) then) =
      _$CategoryDtoCopyWithImpl<$Res>;
  $Res call(
      {String id, String title, String titlePicUrl, String shortDescription});
}

/// @nodoc
class _$CategoryDtoCopyWithImpl<$Res> implements $CategoryDtoCopyWith<$Res> {
  _$CategoryDtoCopyWithImpl(this._value, this._then);

  final CategoryDto _value;
  // ignore: unused_field
  final $Res Function(CategoryDto) _then;

  @override
  $Res call({
    Object id = freezed,
    Object title = freezed,
    Object titlePicUrl = freezed,
    Object shortDescription = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      title: title == freezed ? _value.title : title as String,
      titlePicUrl:
          titlePicUrl == freezed ? _value.titlePicUrl : titlePicUrl as String,
      shortDescription: shortDescription == freezed
          ? _value.shortDescription
          : shortDescription as String,
    ));
  }
}

/// @nodoc
abstract class _$CategoryDtoCopyWith<$Res>
    implements $CategoryDtoCopyWith<$Res> {
  factory _$CategoryDtoCopyWith(
          _CategoryDto value, $Res Function(_CategoryDto) then) =
      __$CategoryDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id, String title, String titlePicUrl, String shortDescription});
}

/// @nodoc
class __$CategoryDtoCopyWithImpl<$Res> extends _$CategoryDtoCopyWithImpl<$Res>
    implements _$CategoryDtoCopyWith<$Res> {
  __$CategoryDtoCopyWithImpl(
      _CategoryDto _value, $Res Function(_CategoryDto) _then)
      : super(_value, (v) => _then(v as _CategoryDto));

  @override
  _CategoryDto get _value => super._value as _CategoryDto;

  @override
  $Res call({
    Object id = freezed,
    Object title = freezed,
    Object titlePicUrl = freezed,
    Object shortDescription = freezed,
  }) {
    return _then(_CategoryDto(
      id: id == freezed ? _value.id : id as String,
      title: title == freezed ? _value.title : title as String,
      titlePicUrl:
          titlePicUrl == freezed ? _value.titlePicUrl : titlePicUrl as String,
      shortDescription: shortDescription == freezed
          ? _value.shortDescription
          : shortDescription as String,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_CategoryDto implements _CategoryDto {
  _$_CategoryDto(
      {@required this.id,
      @required this.title,
      @required this.titlePicUrl,
      this.shortDescription})
      : assert(id != null),
        assert(title != null),
        assert(titlePicUrl != null);

  factory _$_CategoryDto.fromJson(Map<String, dynamic> json) =>
      _$_$_CategoryDtoFromJson(json);

  @override
  final String id;
  @override
  final String title;
  @override
  final String titlePicUrl;
  @override
  final String shortDescription;

  @override
  String toString() {
    return 'CategoryDto(id: $id, title: $title, titlePicUrl: $titlePicUrl, shortDescription: $shortDescription)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CategoryDto &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.titlePicUrl, titlePicUrl) ||
                const DeepCollectionEquality()
                    .equals(other.titlePicUrl, titlePicUrl)) &&
            (identical(other.shortDescription, shortDescription) ||
                const DeepCollectionEquality()
                    .equals(other.shortDescription, shortDescription)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(titlePicUrl) ^
      const DeepCollectionEquality().hash(shortDescription);

  @override
  _$CategoryDtoCopyWith<_CategoryDto> get copyWith =>
      __$CategoryDtoCopyWithImpl<_CategoryDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_CategoryDtoToJson(this);
  }
}

abstract class _CategoryDto implements CategoryDto {
  factory _CategoryDto(
      {@required String id,
      @required String title,
      @required String titlePicUrl,
      String shortDescription}) = _$_CategoryDto;

  factory _CategoryDto.fromJson(Map<String, dynamic> json) =
      _$_CategoryDto.fromJson;

  @override
  String get id;
  @override
  String get title;
  @override
  String get titlePicUrl;
  @override
  String get shortDescription;
  @override
  _$CategoryDtoCopyWith<_CategoryDto> get copyWith;
}
