import 'package:organikart_shared/domain/category/category.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

part 'category_dtos.freezed.dart';
part 'category_dtos.g.dart';

@freezed
abstract class CategoryDto with _$CategoryDto {
  factory CategoryDto({
    @required String id,
    @required String title,
    @required String titlePicUrl,
    String shortDescription,
  }) = _CategoryDto;

  factory CategoryDto.fromDomain(Category b) {
    return CategoryDto(
      id: b.id.getOrCrash(),
      title: b.title,
      titlePicUrl: b.titlePicUrl,
      shortDescription: b.shortDescription,
    );
  }

  factory CategoryDto.fromJson(Map<String, dynamic> json) =>
      _$CategoryDtoFromJson(json);
}

class CategoryDtoConverter
    implements JsonConverter<CategoryDto, Map<String, dynamic>> {
  const CategoryDtoConverter();

  @override
  CategoryDto fromJson(Map<String, dynamic> json) {
    return CategoryDto.fromJson(json);
  }

  @override
  Map<String, dynamic> toJson(CategoryDto fieldValue) => fieldValue.toJson();
}

extension CategoryDtoX on CategoryDto {
  Category toDomain() {
    return Category(
      id: UniqueId.fromUniqueString(id),
      title: title,
      titlePicUrl: titlePicUrl,
      shortDescription: shortDescription,
    );
  }
}
