import 'package:organikart_shared/organikart_shared_package.dart';
import 'package:organikart_shared/infrastructure/category/category_dtos.dart';

part 'content_dto.freezed.dart';
part 'content_dto.g.dart';

@freezed
abstract class ContentDto with _$ContentDto {
  factory ContentDto(
      {@required String id,
      @required String name,
      @required String picUrl,
      @required int mrp,
      @required int discountPercentage,
      @required int discountMrp,
      @required String description,
      @required String qtyUnit,
      @required @CategoryDtoConverter() CategoryDto category}) = _ContentDto;

  factory ContentDto.fromDomain(Content b) {
    return ContentDto(
        id: b.id.getOrCrash(),
        picUrl: b.picUrl,
        name: b.name,
        description: b.description,
        discountPercentage: b.discountPercentage.getOrCrash(),
        mrp: b.mrp.getOrCrash(),
        discountMrp: b.discountedMrp.getOrCrash(),
        qtyUnit: b.qtyUnit.toValueString(),
        category: CategoryDto.fromDomain(b.category));
  }

  factory ContentDto.fromJson(Map<String, dynamic> json) =>
      _$ContentDtoFromJson(json);
}

class ContentDtoConverter
    implements JsonConverter<ContentDto, Map<String, dynamic>> {
  const ContentDtoConverter();

  @override
  ContentDto fromJson(Map<String, dynamic> json) {
    return ContentDto.fromJson(json);
  }

  @override
  Map<String, dynamic> toJson(ContentDto fieldValue) => fieldValue.toJson();
}

extension ContentDtoX on ContentDto {
  Content toDomain() {
    return Content(
        id: UniqueId.fromUniqueString(id),
        name: name,
        picUrl: picUrl,
        mrp: Price(mrp.toString()),
        discountedMrp: Price(discountMrp.toString()),
        discountPercentage: Percentage(discountPercentage.toString()),
        description: description,
        qtyUnit: qtyUnit.toQtyUnit(),
        category: category.toDomain());
  }
}
