// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'content_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
ContentDto _$ContentDtoFromJson(Map<String, dynamic> json) {
  return _ContentDto.fromJson(json);
}

/// @nodoc
class _$ContentDtoTearOff {
  const _$ContentDtoTearOff();

// ignore: unused_element
  _ContentDto call(
      {@required String id,
      @required String name,
      @required String picUrl,
      @required int mrp,
      @required int discountPercentage,
      @required int discountMrp,
      @required String description,
      @required String qtyUnit,
      @required @CategoryDtoConverter() CategoryDto category}) {
    return _ContentDto(
      id: id,
      name: name,
      picUrl: picUrl,
      mrp: mrp,
      discountPercentage: discountPercentage,
      discountMrp: discountMrp,
      description: description,
      qtyUnit: qtyUnit,
      category: category,
    );
  }

// ignore: unused_element
  ContentDto fromJson(Map<String, Object> json) {
    return ContentDto.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $ContentDto = _$ContentDtoTearOff();

/// @nodoc
mixin _$ContentDto {
  String get id;
  String get name;
  String get picUrl;
  int get mrp;
  int get discountPercentage;
  int get discountMrp;
  String get description;
  String get qtyUnit;
  @CategoryDtoConverter()
  CategoryDto get category;

  Map<String, dynamic> toJson();
  $ContentDtoCopyWith<ContentDto> get copyWith;
}

/// @nodoc
abstract class $ContentDtoCopyWith<$Res> {
  factory $ContentDtoCopyWith(
          ContentDto value, $Res Function(ContentDto) then) =
      _$ContentDtoCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String name,
      String picUrl,
      int mrp,
      int discountPercentage,
      int discountMrp,
      String description,
      String qtyUnit,
      @CategoryDtoConverter() CategoryDto category});

  $CategoryDtoCopyWith<$Res> get category;
}

/// @nodoc
class _$ContentDtoCopyWithImpl<$Res> implements $ContentDtoCopyWith<$Res> {
  _$ContentDtoCopyWithImpl(this._value, this._then);

  final ContentDto _value;
  // ignore: unused_field
  final $Res Function(ContentDto) _then;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
    Object picUrl = freezed,
    Object mrp = freezed,
    Object discountPercentage = freezed,
    Object discountMrp = freezed,
    Object description = freezed,
    Object qtyUnit = freezed,
    Object category = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed ? _value.id : id as String,
      name: name == freezed ? _value.name : name as String,
      picUrl: picUrl == freezed ? _value.picUrl : picUrl as String,
      mrp: mrp == freezed ? _value.mrp : mrp as int,
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage as int,
      discountMrp:
          discountMrp == freezed ? _value.discountMrp : discountMrp as int,
      description:
          description == freezed ? _value.description : description as String,
      qtyUnit: qtyUnit == freezed ? _value.qtyUnit : qtyUnit as String,
      category: category == freezed ? _value.category : category as CategoryDto,
    ));
  }

  @override
  $CategoryDtoCopyWith<$Res> get category {
    if (_value.category == null) {
      return null;
    }
    return $CategoryDtoCopyWith<$Res>(_value.category, (value) {
      return _then(_value.copyWith(category: value));
    });
  }
}

/// @nodoc
abstract class _$ContentDtoCopyWith<$Res> implements $ContentDtoCopyWith<$Res> {
  factory _$ContentDtoCopyWith(
          _ContentDto value, $Res Function(_ContentDto) then) =
      __$ContentDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String name,
      String picUrl,
      int mrp,
      int discountPercentage,
      int discountMrp,
      String description,
      String qtyUnit,
      @CategoryDtoConverter() CategoryDto category});

  @override
  $CategoryDtoCopyWith<$Res> get category;
}

/// @nodoc
class __$ContentDtoCopyWithImpl<$Res> extends _$ContentDtoCopyWithImpl<$Res>
    implements _$ContentDtoCopyWith<$Res> {
  __$ContentDtoCopyWithImpl(
      _ContentDto _value, $Res Function(_ContentDto) _then)
      : super(_value, (v) => _then(v as _ContentDto));

  @override
  _ContentDto get _value => super._value as _ContentDto;

  @override
  $Res call({
    Object id = freezed,
    Object name = freezed,
    Object picUrl = freezed,
    Object mrp = freezed,
    Object discountPercentage = freezed,
    Object discountMrp = freezed,
    Object description = freezed,
    Object qtyUnit = freezed,
    Object category = freezed,
  }) {
    return _then(_ContentDto(
      id: id == freezed ? _value.id : id as String,
      name: name == freezed ? _value.name : name as String,
      picUrl: picUrl == freezed ? _value.picUrl : picUrl as String,
      mrp: mrp == freezed ? _value.mrp : mrp as int,
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage as int,
      discountMrp:
          discountMrp == freezed ? _value.discountMrp : discountMrp as int,
      description:
          description == freezed ? _value.description : description as String,
      qtyUnit: qtyUnit == freezed ? _value.qtyUnit : qtyUnit as String,
      category: category == freezed ? _value.category : category as CategoryDto,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_ContentDto implements _ContentDto {
  _$_ContentDto(
      {@required this.id,
      @required this.name,
      @required this.picUrl,
      @required this.mrp,
      @required this.discountPercentage,
      @required this.discountMrp,
      @required this.description,
      @required this.qtyUnit,
      @required @CategoryDtoConverter() this.category})
      : assert(id != null),
        assert(name != null),
        assert(picUrl != null),
        assert(mrp != null),
        assert(discountPercentage != null),
        assert(discountMrp != null),
        assert(description != null),
        assert(qtyUnit != null),
        assert(category != null);

  factory _$_ContentDto.fromJson(Map<String, dynamic> json) =>
      _$_$_ContentDtoFromJson(json);

  @override
  final String id;
  @override
  final String name;
  @override
  final String picUrl;
  @override
  final int mrp;
  @override
  final int discountPercentage;
  @override
  final int discountMrp;
  @override
  final String description;
  @override
  final String qtyUnit;
  @override
  @CategoryDtoConverter()
  final CategoryDto category;

  @override
  String toString() {
    return 'ContentDto(id: $id, name: $name, picUrl: $picUrl, mrp: $mrp, discountPercentage: $discountPercentage, discountMrp: $discountMrp, description: $description, qtyUnit: $qtyUnit, category: $category)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ContentDto &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.picUrl, picUrl) ||
                const DeepCollectionEquality().equals(other.picUrl, picUrl)) &&
            (identical(other.mrp, mrp) ||
                const DeepCollectionEquality().equals(other.mrp, mrp)) &&
            (identical(other.discountPercentage, discountPercentage) ||
                const DeepCollectionEquality()
                    .equals(other.discountPercentage, discountPercentage)) &&
            (identical(other.discountMrp, discountMrp) ||
                const DeepCollectionEquality()
                    .equals(other.discountMrp, discountMrp)) &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.qtyUnit, qtyUnit) ||
                const DeepCollectionEquality()
                    .equals(other.qtyUnit, qtyUnit)) &&
            (identical(other.category, category) ||
                const DeepCollectionEquality()
                    .equals(other.category, category)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(picUrl) ^
      const DeepCollectionEquality().hash(mrp) ^
      const DeepCollectionEquality().hash(discountPercentage) ^
      const DeepCollectionEquality().hash(discountMrp) ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(qtyUnit) ^
      const DeepCollectionEquality().hash(category);

  @override
  _$ContentDtoCopyWith<_ContentDto> get copyWith =>
      __$ContentDtoCopyWithImpl<_ContentDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ContentDtoToJson(this);
  }
}

abstract class _ContentDto implements ContentDto {
  factory _ContentDto(
      {@required String id,
      @required String name,
      @required String picUrl,
      @required int mrp,
      @required int discountPercentage,
      @required int discountMrp,
      @required String description,
      @required String qtyUnit,
      @required @CategoryDtoConverter() CategoryDto category}) = _$_ContentDto;

  factory _ContentDto.fromJson(Map<String, dynamic> json) =
      _$_ContentDto.fromJson;

  @override
  String get id;
  @override
  String get name;
  @override
  String get picUrl;
  @override
  int get mrp;
  @override
  int get discountPercentage;
  @override
  int get discountMrp;
  @override
  String get description;
  @override
  String get qtyUnit;
  @override
  @CategoryDtoConverter()
  CategoryDto get category;
  @override
  _$ContentDtoCopyWith<_ContentDto> get copyWith;
}
