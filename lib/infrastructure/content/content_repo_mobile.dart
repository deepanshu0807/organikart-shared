import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:organikart_shared/infrastructure/content/content_dto.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

class ContentRepo implements IContentRepo {
  final FirebaseFirestore _firestore;

  final FirebaseStorage _firebaseStorage;

  ContentRepo(this._firestore, this._firebaseStorage);
  @override
  Future<Either<InfraFailure, Unit>> create(Content content) async {
    try {
      final cRef = await _firestore.content();
      final cDto = ContentDto.fromDomain(content);

      final jsonX = cDto.toJson();
      print("JsonX \n $jsonX");
      jsonX["writeCount"] = FieldValue.increment(1);
      await cRef.doc(cDto.id).set(jsonX, SetOptions(merge: true));
      return right(unit);
    } catch (e) {
      // These error codes and messages aren't in the documentation AFAIK, experiment in the debugger to find out about them.
      debugPrint("ERR:$e");
      return left(const InfraFailure.serverError());
    }
  }

  @override
  Future<Either<InfraFailure, Unit>> delete(Content content) async {
    try {
      final cRef = await _firestore.content();
      final cDto = ContentDto.fromDomain(content);
      await cRef.doc(cDto.id).delete();
      return right(unit);
    } catch (e) {
      debugPrint("ERR:: $e");

      return left(const InfraFailure.serverError());
    }
  }

  @override
  Stream<Either<InfraFailure, List<Content>>> getAllContent() async* {
    final c = await _firestore.content();
    yield* c
        .snapshots()
        .map(
          (snapshot) => right<InfraFailure, List<Content>>(snapshot.docs
              .map((doc) => ContentDto.fromJson(doc.data()).toDomain())
              .toList()),
        )
        .onErrorReturnWith((e) {
      debugPrint("Unexpected Error $e");

      return left(const InfraFailure.serverError());
    });
  }

  @override
  Future<Either<InfraFailure, UploadResult>> uploadMainContentImage(
      Content content) {
    // TODO: implement uploadMainContentImage
    throw UnimplementedError();
  }
}
