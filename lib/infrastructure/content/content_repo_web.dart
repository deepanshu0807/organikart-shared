import 'dart:html';

import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:image_picker_web/image_picker_web.dart';
import 'package:firebase/firebase.dart' as fb;
import 'package:organikart_shared/infrastructure/content/content_dto.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

class ContentRepo implements IContentRepo {
  final FirebaseFirestore _firestore;

  final fb.Storage _firebaseStorage;

  ContentRepo(this._firestore, this._firebaseStorage);
  @override
  Future<Either<InfraFailure, Unit>> create(Content content) async {
    try {
      final cRef = await _firestore.content();
      final cDto = ContentDto.fromDomain(content);

      final jsonX = cDto.toJson();
      print("JsonX \n $jsonX");
      jsonX["writeCount"] = FieldValue.increment(1);
      await cRef.doc(cDto.id).set(jsonX, SetOptions(merge: true));
      return right(unit);
    } catch (e) {
      // These error codes and messages aren't in the documentation AFAIK, experiment in the debugger to find out about them.
      debugPrint("ERR:$e");
      return left(const InfraFailure.serverError());
    }
  }

  @override
  Future<Either<InfraFailure, Unit>> delete(Content content) async {
    try {
      final cRef = await _firestore.content();
      final cDto = ContentDto.fromDomain(content);
      await cRef.doc(cDto.id).delete();
      return right(unit);
    } catch (e) {
      debugPrint("ERR:: $e");

      return left(const InfraFailure.serverError());
    }
  }

  @override
  Stream<Either<InfraFailure, List<Content>>> getAllContent() async* {
    final c = await _firestore.content();
    yield* c
        .snapshots()
        .map(
          (snapshot) => right<InfraFailure, List<Content>>(snapshot.docs
              .map((doc) => ContentDto.fromJson(doc.data()).toDomain())
              .toList()),
        )
        .onErrorReturnWith((e) {
      debugPrint("Unexpected Error $e");

      return left(const InfraFailure.serverError());
    });
  }

  @override
  Future<Either<InfraFailure, UploadResult>> uploadMainContentImage(
      Content content) async {
    try {
      final File file =
          await ImagePickerWeb.getImage(outputType: ImageType.file) as File;

      final imageFileName = content.id.getOrElse("Wrong");
      final storageref = _firebaseStorage
          .ref("content")
          .child(content.id.getOrCrash())
          .child("main_content_image")
          .child(imageFileName);
      final fb.UploadTask uploadTask = storageref.put(file);

      final fb.UploadTaskSnapshot taskSnapshot = await uploadTask.future;

      final downloadUrl = await taskSnapshot.ref.getDownloadURL();

      return right(
          UploadResult(name: imageFileName, picUrl: downloadUrl.toString()));
    } catch (e) {
      debugPrint("ERR unexpected::${e.code}");

      return left(const InfraFailure.imageUploadError());
    }
  }
}
