import 'package:dartz/dartz.dart';
import 'package:firebase/firebase.dart' as fb;
import 'package:organikart_shared/organikart_shared_package.dart';

class ContentRepo implements IContentRepo {
  final FirebaseFirestore _firestore;

  final fb.Storage _firebaseStorage;

  ContentRepo(this._firestore, this._firebaseStorage);

  @override
  Future<Either<InfraFailure, Unit>> create(Content content) {
    // TODO: implement create
    throw UnimplementedError();
  }

  @override
  Future<Either<InfraFailure, Unit>> delete(Content content) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Stream<Either<InfraFailure, List<Content>>> getAllContent() {
    // TODO: implement getAllContent
    throw UnimplementedError();
  }

  @override
  Future<Either<InfraFailure, UploadResult>> uploadMainContentImage(
      Content content) {
    // TODO: implement uploadMainContentImage
    throw UnimplementedError();
  }
}
