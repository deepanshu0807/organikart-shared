// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'content_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ContentDto _$_$_ContentDtoFromJson(Map<String, dynamic> json) {
  return _$_ContentDto(
    id: json['id'] as String,
    name: json['name'] as String,
    picUrl: json['picUrl'] as String,
    mrp: json['mrp'] as int,
    discountPercentage: json['discountPercentage'] as int,
    discountMrp: json['discountMrp'] as int,
    description: json['description'] as String,
    qtyUnit: json['qtyUnit'] as String,
    category: const CategoryDtoConverter()
        .fromJson(json['category'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$_$_ContentDtoToJson(_$_ContentDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'picUrl': instance.picUrl,
      'mrp': instance.mrp,
      'discountPercentage': instance.discountPercentage,
      'discountMrp': instance.discountMrp,
      'description': instance.description,
      'qtyUnit': instance.qtyUnit,
      'category': const CategoryDtoConverter().toJson(instance.category),
    };
