import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

part 'organikart_user_dtos.freezed.dart';
part 'organikart_user_dtos.g.dart';

@freezed
abstract class OrganikartUserDtos with _$OrganikartUserDtos {
  factory OrganikartUserDtos(
      {@required String id,
      @required String email,
      @required String name,
      @required String phoneNumber,
      @required String role,
      @required int lastSignInTime,
      String picUrl}) = _OrganikartUserDtos;

  factory OrganikartUserDtos.fromDomain(OrganikartUser user) {
    return OrganikartUserDtos(
      // id: p.id.getOrCrash(),
      id: user.uId.getOrCrash(),
      email: user.emailAddress.getOrElse("NA"),
      phoneNumber: user.phoneNumber.getOrElse(""),
      role: user.role.toValueString(),
      lastSignInTime: user.lastSignInDateTime.millisecondsSinceEpoch,
      name: user.name.getOrCrash(),
      picUrl: user.picUrl,
      // locations:
      //     p.locations.map((e) => OrganikartLocationDtos.fromDomain(e)).toList(),
      // locPoint: GeoFirePoint(loc.lat, loc.lng),
    );
  }

  factory OrganikartUserDtos.fromJson(Map<String, dynamic> json) =>
      _$OrganikartUserDtosFromJson(json);
}

class OrganikartUserDtosConverter
    implements JsonConverter<OrganikartUserDtos, Map<String, dynamic>> {
  const OrganikartUserDtosConverter();

  @override
  OrganikartUserDtos fromJson(Map<String, dynamic> json) {
    return OrganikartUserDtos.fromJson(json);
  }

  @override
  Map<String, dynamic> toJson(OrganikartUserDtos fieldValue) =>
      fieldValue.toJson();
}

extension OrganikartUserDtosX on OrganikartUserDtos {
  OrganikartUser toDomain() {
    return OrganikartUser(
      uId: UniqueId.fromUniqueString(this.id),
      emailAddress: EmailAddress(email),
      phoneNumber: PhoneNumber(phoneNumber),
      role: role.toUserRole(),
      lastSignInDateTime:
          DateTime.fromMillisecondsSinceEpoch(lastSignInTime ?? 0),
      name: Name(name),
      picUrl: picUrl,
    );
  }
}
