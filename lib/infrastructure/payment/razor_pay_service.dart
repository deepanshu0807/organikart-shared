import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:organikart_shared/domain/orders/orders.dart';
import 'package:organikart_shared/domain/orders/payment_method.dart';
import 'package:organikart_shared/domain/payment/i_payment_repo.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

const testKey = {
  "keyId": "rzp_test_iqdVEyiV6zKZ28",
  "keySecret": "jRW6f9NUd5joIeY9jP2nVRpD",
};

class RazorPayService implements IPaymentRepo {
  final _razorpay = Razorpay();

  @override
  Future<Unit> onlinePaymentForOrder({
    OrderEntity order,
    // @required VrishamLocation location,
    Function(OrderEntity order, PaymentMethod method) onPaymentSuccess,
    Function(OrderEntity order, PaymentMethod method) onPaymentFailed,
  }) async {
    final Map<String, Object> options = {
      'key': testKey["keyId"],
      'amount': (double.parse(order.amountToBePaid().toStringAsFixed(2)) * 100)
          .toInt(), //in the smallest currency sub-unit.
      'name': '${order?.items?.first?.name ?? "No item"} etc..',
      // 'order_id': cartBooking.id.getOrElse("Na"),
      'description': 'Placing order on organikart',
      'prefill': {
        'contact': order.customer.phoneNumber.getOrElse("Na"),
        'email': order.customer.emailAddress.getOrElse("nomail@gmail.com"),
        'name': order.customer.name.getOrElse("Na"),
      },
      'theme': {
        'color': "#99BF00",
      }
    };

    _razorpay.open(options);

    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, (PaymentSuccessResponse r) {
      debugPrint("Razorpay.EVENT_PAYMENT_SUCCESS ${r.toString()}");
      onPaymentSuccess(order, const PaymentMethod.online());
    });

    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, (PaymentFailureResponse r) {
      debugPrint("Razorpay.EVENT_PAYMENT_ERROR ${r.message}");
      onPaymentFailed(order, const PaymentMethod.online());
    });

    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, (r) {
      // // debugPrint("Razorpay.EVENT_EXTERNAL_WALLET ${r.toString()}");
    });

    // // debugPrint("\nOptions before open razorpay: $options");

    return unit;
  }
}
