import 'package:flutter/material.dart';

class AppColors {
  static const Color _primaryColor = Color(0xff99BF00);
  static const Color _secondaryColor = Color(0xffE1E66B);
  static const Color _black = Colors.black;
  static const Color _white = Colors.white;
  static Color getSecondaryColor() {
    return _secondaryColor;
  }

  static Color getPrimaryColor() {
    return _primaryColor;
  }

  static Color getBlack() {
    return _black;
  }

  static Color getWhite() {
    return _white;
  }
}
