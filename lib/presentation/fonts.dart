import 'dart:ui';

import 'package:google_fonts/google_fonts.dart';

import 'colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

TextStyle regular = TextStyle(
  fontSize: 14,
  fontWeight: FontWeight.w400,
  color: Colors.grey[500],
);

//=====================================

TextStyle text12 = GoogleFonts.lato(
  fontSize: 12.sp,
  color: AppColors.getPrimaryColor(),
  fontWeight: FontWeight.w400,
);

TextStyle text16 = GoogleFonts.lato(
  fontSize: 16.sp,
  color: AppColors.getPrimaryColor(),
  fontWeight: FontWeight.w400,
);

TextStyle text20 = GoogleFonts.lato(
  fontSize: 20.sp,
  color: AppColors.getPrimaryColor(),
  fontWeight: FontWeight.w300,
);

TextStyle text25 = GoogleFonts.lato(
  fontSize: 25.sp,
  color: AppColors.getPrimaryColor(),
  fontWeight: FontWeight.w400,
);

TextStyle text30 = GoogleFonts.lato(
  fontSize: 30.sp,
  color: AppColors.getPrimaryColor(),
  fontWeight: FontWeight.w400,
);

TextStyle text35 = GoogleFonts.lato(
  fontSize: 35.sp,
  color: AppColors.getPrimaryColor(),
  fontWeight: FontWeight.w400,
);

TextStyle text60 = GoogleFonts.lato(
  fontSize: 60.sp,
  color: AppColors.getPrimaryColor(),
  fontWeight: FontWeight.w400,
);

TextStyle text80 = GoogleFonts.lato(
  fontSize: 80.sp,
  color: AppColors.getPrimaryColor(),
  fontWeight: FontWeight.w400,
);

TextStyle text100 = GoogleFonts.lato(
  fontSize: 100.sp,
  color: AppColors.getPrimaryColor(),
  fontWeight: FontWeight.w400,
);

TextStyle text300 = GoogleFonts.lato(
  fontSize: 300.sp,
  color: AppColors.getPrimaryColor(),
  fontWeight: FontWeight.w400,
);
